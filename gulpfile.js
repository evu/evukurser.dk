/*jshint node:true,strict:true,undef:true,unused:true*/
'use strict';


/***** Setup *****/

// Configuration of base folders.
var docDir = 'doc';
var appDir = 'app';
var serverDir = 'server';

// Configuration of file patterns.
var paths = {
	project: {
		readmeFile: 'README.md',
		gulpFile: 'gulpfile.js',
		docDir: docDir,
		ramlFile: serverDir + '/api/index.raml'
	},
	client: {
		// Destination folders.
		destDir: 'build',
		docDir: docDir + '/' + appDir,
		// Source folders and files.
		srcDir: appDir,
		styleMain: appDir + '/**/index.less',
		styleFiles: appDir + '/**/*.{less,css}',
		jsMain: appDir + '/app.js',
		jsFiles: appDir + '/**/*.js',
		imgFiles: [ appDir + '/**/*.{png,jpeg,jpg}', 'lib/jquery-ui/**/*.{png,jpeg,jpg}' ],
		svgFiles: appDir + '/**/*.svg',
		htmlFiles: appDir + '/**/*.html',
		staticFiles: [ appDir + '/**/*.ico', 'lib/semantic/minified/{fonts,images}/*', appDir + '/**/*.csv' ],
		jshintrc: appDir + '/.jshintrc'
	},
	server: {
		// Destination folders.
		docDir: docDir + '/' + serverDir,
		// Source folders and files.
		srcDir: serverDir,
		jsFiles: serverDir + '/**/*.js',
		jshintrc: serverDir + '/.jshintrc'
	}
};

// Create list of destination folders to clean.
paths.project.cleanDirs = [ paths.client.destDir, paths.project.docDir ];


/***** Tasks *****/

var changed = require('gulp-changed');
var cssPrefixer = require('gulp-autoprefixer');
var minifyHTML = require('gulp-minify-html');
// var livereload = require('gulp-livereload');
var browserify = require('gulp-browserify');
var raml2html = require('gulp-raml2html');
var imagemin = require('gulp-imagemin');
var changed = require('gulp-changed');
var plumber = require('gulp-plumber');
var replace = require('gulp-replace');
var flatten = require('flatten');
var rename = require('gulp-rename');
var cssmin = require('gulp-cssmin');
var svgmin = require('gulp-svgmin');
// var uglify = require('gulp-uglify');
var jshint = require('gulp-jshint');
var clean = require('gulp-clean');
var gutil = require('gulp-util');
var less = require('gulp-less');
var gulp = require('gulp');


// Determine whether running in production or not:
// If NODE_ENV=production or invoking gulp with --production argument.
var isProduction = process.env.NODE_ENV === 'production' || !!gutil.env.production;

// Cleans the destination build folder: gulp clean
gulp.task('clean', function() {
	gulp.src(flatten([ paths.project.cleanDirs ]), { read: false })
		.pipe(clean());
});

// Compile LESS stylesheets to CSS.
gulp.task('style', function() {
	gulp.src(paths.client.styleMain)
		.pipe(plumber())
		.pipe(less())
		.pipe(cssPrefixer('last 2 versions', 'ie 9'))
		.pipe(cssmin({ keepSpecialComments: false }))
		.pipe(replace('undefined', '')) // quick fix for https://github.com/jakubpawlowicz/clean-css/issues/335
		.pipe(gulp.dest(paths.client.destDir));
});

// Check client-side JavaScript code quality using JSHint.
gulp.task('lint-js-client', function() {
	gulp.src(flatten([ paths.client.jsFiles ]))
		.pipe(jshint(paths.client.jshintrc))
		.pipe(jshint.reporter('jshint-stylish'));
});

// Check server-side JavaScript code quality using JSHint.
gulp.task('lint-js-server', function() {
	gulp.src(flatten([ paths.server.jsFiles, paths.project.gulpFile ]))
		.pipe(jshint(paths.server.jshintrc))
		.pipe(jshint.reporter('jshint-stylish'));
});

// Compile and minify JS files using Browserify/CommonJS.
// In production everything is minified to a single bundle.
// Otherwise (in development) the JS is output with source maps.
gulp.task('js', function(){
	gulp.src(paths.client.jsMain)
		.pipe(plumber())
		.pipe(browserify({
			// Do not parse require statements in certain modules for faster builds.
			noParse: [
				'lib/angular/angular.js',
				'lib/angular-route/angular-route.js',
				'lib/momentjs/moment-with-locales.js',
				'lib/semantic/packaged/javascript/semantic.js',
				'lib/jquery-ui/jquery-ui.min.js',
				'jquery-commonjs'
			],
			insertGlobals : !isProduction,
			ignore: isProduction ? ['./dev'] : [],
			debug : !isProduction
		}))
		// .pipe(isProduction ? uglify() : gutil.noop()) // Code fails when minified
		.pipe(gulp.dest(paths.client.destDir));
});

// Copy images.
gulp.task('img', function(){
	gulp.src(paths.client.imgFiles)
		.pipe(gulp.dest(paths.client.destDir));
});

// Minify images (in source). Task must be manually run by developer
gulp.task('pngmin', function(){
	gulp.src(paths.client.imgFiles)
		.pipe(changed(paths.client.destDir))
		.pipe(imagemin({ optimizationLevel: 5 }))
		.pipe(gulp.dest(paths.client.srcDir));
});
gulp.task('jpgmin' , ['pngmin']);
gulp.task('jpegmin', ['pngmin']);

// Copy SVG files.
gulp.task('svg', function(){
	gulp.src(paths.client.svgFiles)
		.pipe(gulp.dest(paths.client.destDir));
});

// Minify SVG files (in source). Task must be manually run by developer
gulp.task('svgmin', function(){
	gulp.src(paths.client.svgFiles)
		.pipe(changed(paths.client.destDir))
		.pipe(svgmin())
		.pipe(gulp.dest(paths.client.srcDir));
});

// Minify HTML files.
gulp.task('html', function(){
	gulp.src(paths.client.htmlFiles)
		.pipe(minifyHTML({
			empty: true,
			quotes: true
		}))
		.pipe(gulp.dest(paths.client.destDir));
});

// Copy static files.
gulp.task('copy', function(){
	gulp.src(paths.client.staticFiles)
		.pipe(gulp.dest(paths.client.destDir));
});

// Generate API documentation for developers.
gulp.task('apidoc', function() {
	gulp.src(paths.project.ramlFile)
		.pipe(rename('api.raml'))
		.pipe(raml2html())
		.pipe(gulp.dest(paths.client.destDir));
});

// Watch for file changes and build accordingly.
gulp.task('watch', function() {
	gulp.watch(paths.client.styleFiles, ['style']);
	gulp.watch(paths.client.jsFiles, ['lint-js-client', 'js']);
	gulp.watch(paths.server.jsFiles, ['lint-js-server']);
	gulp.watch(paths.client.imgFiles, ['img']);
	gulp.watch(paths.client.svgFiles, ['svg']);
	gulp.watch(paths.client.htmlFiles, ['html']);
	gulp.watch(paths.client.staticFiles, ['copy']);
	gulp.watch(paths.project.ramlFile, ['apidoc']);

	// Start LiveReload server and emit change events everytime CSS is updated.
	// var liveReloadServer = livereload();
	// gulp.watch(paths.client.destDir + '/**/*.{css,js}').on('change', function(file) {
	// 	liveReloadServer.changed(file.path);
	// });
});

// Lint everything.
gulp.task('lint', ['lint-js-client', 'lint-js-server']);

// Minify images. Task must be run manually by developer.
gulp.task('imgmin'  , ['pngmin', 'svgmin']);
gulp.task('imagemin', ['pngmin', 'svgmin']);

// Default tasks.
// In NPM it is setup with: gulp clean && clean build --production
// When developing you can continously build files using just: gulp
gulp.task('build', ['style', 'lint', 'js', 'img', 'svg', 'html', 'copy', 'apidoc']);
gulp.task('default', ['build', 'watch']);
