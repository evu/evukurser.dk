/** @module server/mongodb */
'use strict';

var spawn = require('child_process').spawn;
var path = require('path');

function runMongoInBackground() {
  var mongo = spawn(path.resolve(__dirname, 'scripts', 'run-mongodb.sh'), {
    cwd: path.resolve(__dirname, '..'),
    stdio: 'inherit'
  });

  mongo.on('close', function(code, signal) {
    console.log(
      'MongoDB exited with code %s%s. Restarting in a few seconds!',
      code,
      signal ? ' (killed by signal ' + signal + ')' : ''
    );
    setTimeout(runMongoInBackground, 1000);
  });
}

if (process.env.NODE_ENV !== 'production') {
  runMongoInBackground();
}
