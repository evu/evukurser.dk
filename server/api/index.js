/** @module server/api */
'use strict';

exports.cpr = require('./cpr');
exports.csv = require('./csv');
exports.oauth2 = require('./oauth2');
exports.options = require('./options');
exports.contact = require('./contact');
exports.course = require('./course');
exports.courses = require('./courses');
exports.courseTypes = require('./courseTypes');
exports.courseRating = require('./courseRating');
exports.completedClasses = require('./completedClasses');
exports.news = require('./news');
exports.user = require('./user');
exports.users = require('./users');
exports.apilog = require('./apilog');
exports.wishlist = require('./wishlist');
exports.wsStatus = require('./wsStatus');
