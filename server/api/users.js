/** @module server/api/users */
'use strict';

var scrypt = require('scrypt');
var email = require('../email');
var uuid = require('../uuid');
var user = require('./user');
var db = require('../db');

function createUser(clientInfo, options, callback) {
	var error;

	options.name = ('' + [options.name]).replace(/\s+/g, ' ').trim();
	options.email = ('' + [options.email]).trim().toLowerCase();
	options.password = '' + [options.password];

	if (!options.name) {
		error = new Error('Navn er påkrævet.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (!options.email) {
		error = new Error('E-mail-adresse er påkrævet.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (!options.password) {
		error = new Error('Adgangskode er påkrævet.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (!/^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,20}$/.test(options.email)) {
		error = new Error('Den angivne e-mail-adresse er ugyldig.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (options.password.length < 6) {
		error = new Error('Adgangskode er for kort (mindst 6 bogstaver, tal og/eller tegn).');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	scrypt.passwordHash(options.password, {N:15,r:8,p:1}, function(error, hashedPassword) {
		if (error) {
			console.error('Could not create password hash for new user:', error);
			error = new Error('Der opstod en fejl ved oprettelse af brugeren.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		var now = new Date();

		db.users.save({
			name: options.name,
			password: hashedPassword,
			passwordResetTime: null,
			passwordResetCodes: [],
			picture: null,
			email: options.email,
			emailVerifiedTime: null,
			emailVerifyCodes: [
				{ code: uuid(), time: now }
			],
			created: {
				time: now,
				client: clientInfo
			},
			notifications: {
				certificateExpiration: false,
				registeredCourses: false
			},
			roles: ['user']
		}, function(error, doc) {
			if (error) {
				if (error.name === 'MongoError' && error.code === 11000) {
					error = new Error('Brugeren med den angivne e-mail-adresse eksisterer allerede.');
					error.code = 'conflict';
					error.status = 409;
					return callback(error);
				}

				console.error('Could not create new user in database:', error);
				error = new Error('Der opstod en fejl ved oprettelse af brugeren.');
				error.code = 'server_error';
				error.status = 500;
				return callback(error);
			}

			email.signup.send(doc.email, doc.name, doc.emailVerifyCodes[0].code);

			callback(null, user.format(doc));
		});
	});
}

function getUsers(options, callback) {
	var offset = Math.max(0, options.offset | 0);
	var limit = Math.max(0, Math.min(1000, options.limit | 0 || 50));

	db.users.find({}).sort({ name: 1 }).limit(limit).skip(offset, function(error, docs) {
		if (error) {
			console.error('Could not fetch list of users:', error);
			error = new Error('Kunne ikke hente listen af brugere.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		callback(null, {
			pagination: {
				offset: offset,
				limit: limit
			},
			items: docs.map(function(doc) {
				return user.format(doc, { listing: true });
			})
		});
	});
}

exports.create = createUser;
exports.get = getUsers;
