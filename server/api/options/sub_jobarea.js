/** @module server/api/options/jobarea */
'use strict';

var db = require('../../db');

function getSubJobAreas(params, callback) {
	var query = {};

	if (params.school) {
		query['classes.school.id'] = '' + params.school;
	}

	if (params.region) {
		query['classes.school.geo.properties.address.region'] = '' + params.region;
	}

	var queryType = '' + [params.type];
	if (!/^(electrical|plumbing|chimney)$/.test(queryType)) {
		queryType = 'all';
	}

	if (params.jobarea) {
		query['overwrites.jobareas.' + queryType + '.main.id'] = '' + params.jobarea;
	}

	// if (Object.keys(query).length > 0) {
	// 	query['classesInfo.participants.remaining'] = {$not:{$eq:0}};
	// }

	db.courses.distinct('overwrites.jobareas.' + queryType + '.sub', query, function(error, result) {
		if (error) {
			console.error(error);
			console.error(error);
			error = new Error('Kunne ikke hente listen af under-jobområder fra databasen.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		var values = result
			.filter(function(subJobArea) { return subJobArea; })
			.map(function(subJobArea) {
				return { id: subJobArea.id, name: subJobArea.text };
			});

		callback(null, {
			count: values.length,
			items: values.sort(function(a, b) {
				return a.name.localeCompare(b.name);
			})
		});
	});
}

exports.get = getSubJobAreas;
