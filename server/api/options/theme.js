/** @module server/api/options/theme */
'use strict';

var db = require('../../db');

function getThemes(params, callback) {
	var query = {};

	if (params.school) {
		query['classes.school.id'] = '' + params.school;
	}

	if (params.region) {
		query['classes.school.geo.properties.address.region'] = '' + params.region;
	}

	if (Object.keys(query).length > 0) {
		query['classesInfo.participants.remaining'] = {$not:{$eq:0}};
	}

	var queryType = '' + [params.type];
	if (!/^(electrical|plumbing|chimney)$/.test(queryType)) {
		queryType = 'all';
	}

	db.courses.distinct('overwrites.themes.' + queryType + '.main', query, function(error, result) {
		if (error) {
			console.error(error);
			console.error(error);
			error = new Error('Kunne ikke hente listen af temapakker fra databasen.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		var values = result
			.filter(function(theme) { return theme; })
			.map(function(theme) {
				return { id: theme.id, name: theme.text };
			});

		callback(null, {
			count: values.length,
			items: values.sort(function(a, b) {
				return a.name.localeCompare(b.name);
			})
		});
	});
}

exports.get = getThemes;
