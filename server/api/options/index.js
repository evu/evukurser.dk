/** @module server/api/options */
'use strict';

var common = require('common');

function getOptions(params, callback) {
	common.step([

		function(next) {
			exports.sub_jobarea.get(params, next.parallel());
			exports.jobarea.get(params, next.parallel());
			exports.sub_theme.get(params, next.parallel());
			exports.theme.get(params, next.parallel());
			exports.region.get(params, next.parallel());
			exports.school.get(params, next.parallel());
			exports.sort.get(params, next.parallel());
			exports.type.get(params, next.parallel());
		},

		function(options) {
			callback(null, {
				sub_jobarea: options[0],
				jobarea: options[1],
				sub_theme: options[2],
				theme: options[3],
				region: options[4],
				school: options[5],
				sort: options[6],
				type: options[7]
			});
		}

	], function(error) {
		console.error(error);
		error = new Error('Kunne ikke hente listen af muligheder.');
		error.code = 'server_error';
		error.status = 500;
		callback(error);
	});
}

exports.sub_jobarea = require('./sub_jobarea');
exports.jobarea = require('./jobarea');
exports.sub_theme = require('./sub_theme');
exports.theme = require('./theme');
exports.region = require('./region');
exports.school = require('./school');
exports.sort = require('./sort');
exports.type = require('./type');
exports.get = getOptions;
