/** @module server/api/options/jobarea */
'use strict';

var db = require('../../db');

function getJobAreas(params, callback) {
	var query = {};

	if (params.school) {
		query['classes.school.id'] = '' + params.school;
	}

	if (params.region) {
		query['classes.school.geo.properties.address.region'] = '' + params.region;
	}

	if (Object.keys(query).length > 0) {
		//query['classesInfo.participants.remaining'] = {$not:{$eq:0}};
	}

	var queryType = '' + [params.type];
	if (!/^(electrical|plumbing|chimney)$/.test(queryType)) {
		queryType = 'all';
	}

	db.courses.distinct('overwrites.jobareas.' + queryType + '.main', query, function(error, result) {
		if (error) {
			console.error(error);
			console.error(error);
			error = new Error('Kunne ikke hente listen af jobområder fra databasen.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		var values = result
			.filter(function(jobarea) { return jobarea; })
			.map(function(jobarea) {
				return { id: jobarea.id, name: jobarea.text };
			});

		callback(null, {
			count: values.length,
			items: values.sort(function(a, b) {
				return a.name.localeCompare(b.name);
			})
		});
	});
}

exports.get = getJobAreas;
