/** @module server/api/options/package */
'use strict';

function getSortings(params, callback) {
	callback(null, {
		count: 2,
		items: [
			{ id: 'name', name: 'Kursusnavn' },
			{ id: 'date', name: 'Hold start' }
		]
	});
}

exports.get = getSortings;
