/** @module server/api/options/school */
'use strict';

var db = require('../../db');
var schoolManager = require('../../schoolManager');
var util = require('util');

function getSchools(params, callback) {
	var query = {};

	var region = '' + [params.region];

	if (region) {
		query['classes.school.geo.properties.address.region'] = region;
		// query['classesInfo.participants.remaining'] = {$not:{$eq:0}};
	}

	var jobarea = '' [params.jobarea];


	if (jobarea) {
		query['overwrites.jobareas.' + queryType + '.main.id'] = '' + jobarea;
	}

	var queryType = '' + [params.type];
	if (!/^(electrical|plumbing|chimney)$/.test(queryType)) {
		queryType = 'all';
	}

	
	var courseType = '' + [params.type];

	if (courseType) {
		query['overwrites.types'] = courseType;
	} 

	query['classes.0'] = { $exists: true };
	query['overwrites.types'] = { '$exists': true };
	query['src.id'] = { '$exists': true };

	schoolManager.getSchools(courseType, function(error, mainSchools) {
		var mainSchoolIDs = [];
		var subSchoolIDs = [];

		Object.keys(mainSchools).map(function(id) {
			if (mainSchools[id] && mainSchools[id].id) {
				mainSchoolIDs.push(mainSchools[id].id);
			}
		});

		db.courses.distinct('classes.school', query, function(err, result) {
			if (error || err) {
				console.error(error);
				error = new Error('Kunne ikke hente listen af skoler fra databasen.');
				error.code = 'server_error';
				error.status = 500;
				return callback(error);
			}

			result.forEach(function(school) {
				if (region) {
					if (!(
						school &&
						school.geo &&
						school.geo.properties &&
						school.geo.properties.address &&
						school.geo.properties.address.region === region)) {

						return;
				}
			}

			if (school && school.id && mainSchoolIDs.indexOf(school.id) !== -1) {
					subSchoolIDs.push(school.id);
				}
			});

			mainSchools = Object.keys(mainSchools).map(function(id) {
				return { id: mainSchools[id].id, name: mainSchools[id].schoolName };
			});

			mainSchools = mainSchools.filter(function(school) { return (subSchoolIDs.indexOf(school.id) !== -1); });

			callback(error, {
				count: mainSchools.length,
				items: mainSchools
			});
		});
	});
}

exports.get = getSchools;