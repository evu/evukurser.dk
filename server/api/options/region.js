/** @module server/api/options/region */
'use strict';

var db = require('../../db');

function getRegions(params, callback) {
	db.runCommand({ distinct: 'courses', key: 'classes.school.geo.properties.address.region' }, function(error, result) {
		if (error) {
			console.error(error);
			error = new Error('Kunne ikke hente listen af regioner fra databasen.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		callback(null, {
			count: result.values.length,
			items: result.values.sort().map(function(text) {
				return { id: text, name: text };
			})
		});
	});
}

exports.get = getRegions;
