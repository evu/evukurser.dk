/** @module server/api/options/type */
'use strict';

var courseTypes = require('../../../app/enums/courseTypes');

var typesOptions = {
	count: 4,
	items: Object.keys(courseTypes).map(function(id) {
		return { id: id, name: courseTypes[id].name };
	})
};

function getTypes(params, callback) {
	callback(null, typesOptions);
}

exports.get = getTypes;
