/** @module server/api/options/sub_theme */
'use strict';

var db = require('../../db');

function getSubThemes(params, callback) {
	var query = {};

	if (params.school) {
		query['classes.school.id'] = '' + params.school;
	}

	if (params.region) {
		query['classes.school.geo.properties.address.region'] = '' + params.region;
	}

	var queryType = '' + [params.type];
	if (!/^(electrical|plumbing|chimney)$/.test(queryType)) {
		queryType = 'all';
	}

	if (params.theme) {
		query['overwrites.themes.' + queryType + '.main.id'] = '' + params.theme;
	}

	// if (Object.keys(query).length > 0) {
	// 	query['classesInfo.participants.remaining'] = {$not:{$eq:0}};
	// }

	db.courses.distinct('overwrites.themes.' + queryType + '.sub', query, function(error, result) {
		if (error) {
			console.error(error);
			console.error(error);
			error = new Error('Kunne ikke hente listen af under-temapakker fra databasen.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		var values = result
			.filter(function(subTheme) { return subTheme; })
			.map(function(subTheme) {
				return { id: subTheme.id, name: subTheme.text };
			});

		callback(null, {
			count: values.length,
			items: values.sort(function(a, b) {
				return a.name.localeCompare(b.name);
			})
		});
	});
}

exports.get = getSubThemes;
