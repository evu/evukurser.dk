/** @module server/api/wsStatus */
'use strict';

var common = require('common');
var util = require('util');
var db = require('../db');

function getStatus(callback) {
	common.step([

		function(next) {
			db.courses.count(next.parallel());
			db.courses.count({'classesInfo.updated':{$exists:false}}, next.parallel());
			db.courses.find({'src.updated':{$exists:true}},{'src.updated':1}).sort({'src.updated':1}).limit(1, next.parallel());
			db.courses.find({'src.updated':{$exists:true}},{'src.updated':1}).sort({'src.updated':-1}).limit(1, next.parallel());
			db.courses.find({'classesInfo.updated':{$exists:true}},{'classesInfo.updated':1}).sort({'classesInfo.updated':1}).limit(1, next.parallel());
			db.courses.find({'classesInfo.updated':{$exists:true}},{'classesInfo.updated':1}).sort({'classesInfo.updated':-1}).limit(1, next.parallel());
		},

		function(status) {
			var coursesCount = status[0] | 0;
			var classesNotUpdatedCount = status[1];
			var leastRecentlyUpdatedCourse = (((status[2] || [])[0] || {}).src || {}).updated;
			var mostRecentlyUpdatedCourse = (((status[3] || [])[0] || {}).src || {}).updated;
			var leastRecentlyUpdatedClass = (((status[4] || [])[0] || {}).classesInfo || {}).updated;
			var mostRecentlyUpdatedClass = (((status[5] || [])[0] || {}).classesInfo || {}).updated;

			var now = new Date();
			var updateAge, updateDuration;

			var errors = [];
			var isNotRunning = false;
			if (coursesCount <= 0) {
				errors.push('Der er ikke indlæst kurser i databasen.');
			}
			if (classesNotUpdatedCount !== 0) {
				errors.push('Der er ' + classesNotUpdatedCount + ' kurser, hvor holdene ikke er opdateret.');
			}
			if (!(leastRecentlyUpdatedCourse instanceof Date && mostRecentlyUpdatedCourse instanceof Date)) {
				errors.push('Kurserne er ikke opdateret.');
				isNotRunning = true;
			} else {
				updateAge = (now - leastRecentlyUpdatedCourse) * 0.001 | 0;
				updateDuration = (mostRecentlyUpdatedCourse - leastRecentlyUpdatedCourse) * 0.001 | 0;

				if (updateAge > 25 * 60 * 60) {
					errors.push('Kurserne er ikke blevet opdateret i over et døgn (sidst var ' + leastRecentlyUpdatedCourse + ').');
					isNotRunning = true;
				}
				if (updateDuration > 30 * 60) {
					errors.push('Det tager over en halv time at opdatere kurserne (' + (updateDuration / 60).toFixed(1) + ' minutter).');
				}
			}
			if (!(leastRecentlyUpdatedClass instanceof Date && mostRecentlyUpdatedClass instanceof Date)) {
				errors.push('Holdene er ikke opdateret.');
				isNotRunning = true;
			} else {
				updateAge = (now - leastRecentlyUpdatedClass) * 0.001 | 0;

				if (updateAge > 25 * 60 * 60) {
					errors.push('Holdene er ikke blevet opdateret i over et døgn (sidst var ' + leastRecentlyUpdatedClass + ').');
					isNotRunning = true;
				}
			}
			if (isNotRunning) {
				errors.push('Muligvis kører data-opsamleren ikke.');
			}

			status = {
				courses: {
					count: coursesCount,
					least_recently_updated: leastRecentlyUpdatedCourse,
					most_recently_updated: mostRecentlyUpdatedCourse
				},
				classes: {
					missing: classesNotUpdatedCount,
					least_recently_updated: leastRecentlyUpdatedClass,
					most_recently_updated: mostRecentlyUpdatedClass
				}
			};

			if (errors.length > 0) {
				console.log('Webservice status is bad: ' + errors.join(' ') + '\nStatus data: ' + util.inspect(status, { colors: true, depth: 3 }));
				var error = new Error('Status på data-opsamleren: ' + errors.join(' ') + ' Rå data: ' + JSON.stringify(status));
				error.status = 500;
				error.code = 'webservice_failure';
				return callback(error);
			}

			callback(null, status);
		}

	], function(error) {
		console.log('Could not get WS status:', error.stack || error);
		error = new Error('Kunne ikke indlæse status for data-opsamleren.');
		callback(error);
	});
}

exports.get = getStatus;
