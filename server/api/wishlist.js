/** @module server/api/wishlist */
'use strict';

var course = require('./course');
var db = require('../db');

function formatCourse(doc, wishlistTimestamp) {
	var c = course.format(false, doc);
	delete c.duration;
	delete c.admission;
	delete c.jobareas;
	delete c.theme;
	delete c.apprenticeships;
	delete c.featured;
	delete c.certificate;
	delete c.classes;
	delete c.classesCount;
	c.wishlist = {
		added: wishlistTimestamp || null
	};
	return c;
}

function listWishlist(userId, query, callback) {
	db.users.findOne({ _id: db.decodeId(userId) }, { wishlist: 1 }, function(error, doc) {
		if (error || !doc) {
			console.error('Could not find user when listing wish list:', error);
			error = new Error('Brugeren blev ikke fundet.');
			error.code = 'not_found';
			error.status = 404;
			return callback(error);
		}

		var wishlist = doc.wishlist || {};
		var courseIds = Object.keys(wishlist);

		db.courses.find({
			_id: { $in: courseIds }
		}, {
			'src.title': 1,
			'src.description': 1,
			'overwrites.rating': 1,
			'overwrites.types': 1
		}, function(error, docs) {
			if (error || !docs) {
				console.error('Could not lookup courses when listing wish list:', error);
				error = new Error('Der opstod en fejl med at vise ønskelisten. Prøv venligst igen.');
				error.code = 'server_error';
				error.status = 500;
				return callback(error);
			}

			var wishlistCourses = docs.map(function(doc) {
				return formatCourse(doc, wishlist['' + doc._id]);
			}).sort(function(a, b) {
				return b.wishlist.added - a.wishlist.added;
			});

			callback(null, {
				count: wishlistCourses.length,
				items: wishlistCourses
			});
		});
	});
}

function getWishlistItem(userId, courseId, callback) {
	if (!courseId) {
		error = new Error('Mangler kursus-ID.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	var idMatch = ('' + courseId).match(/^(s?\d{1,8})([evs]?)$/);
	var error;

	if (!idMatch) {
		error = new Error('Ugyldig kursus-ID.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}
	courseId = idMatch[1];

	var fields = {};
	fields['wishlist.' + courseId] = 1;

	db.users.findOne({ _id: db.decodeId(userId) }, fields, function(error, doc) {
		if (error || !doc) {
			console.error('Could not find user when getting wish list item:', error);
			error = new Error('Brugeren blev ikke fundet.');
			error.code = 'not_found';
			error.status = 404;
			return callback(error);
		}

		var wishlist = doc.wishlist || {};
		var wishlistTimestamp = wishlist[courseId];
		if (!wishlistTimestamp) {
			error = new Error('Det angivne kursus er ikke på ønskelisten.');
			error.code = 'not_found';
			error.status = 404;
			return callback(error);
		}

		db.courses.findOne({ _id: courseId }, function(error, doc) {
			if (error || !doc) {
				console.error('Could not lookup course when getting wish list item:', error);
				error = new Error('Der opstod en fejl med at indlæse ønskelisten. Prøv venligst igen.');
				error.code = 'server_error';
				error.status = 500;
				return callback(error);
			}

			callback(null, formatCourse(doc, wishlistTimestamp));
		});
	});
}

function addToWishlist(userId, courseId, callback) {
	if (!courseId) {
		error = new Error('Mangler feltet "id" for at angive kursus-ID.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	var idMatch = ('' + courseId).match(/^(s?\d{1,8})([evs]?)$/);
	var error;

	if (!idMatch) {
		error = new Error('Ugyldig kursus-ID.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}
	courseId = idMatch[1];

	var query = {
		query: { _id: db.decodeId(userId) },
		update: {
			$set: {}
		}
	};
	query.update.$set['wishlist.' + courseId] = {
		'date': new Date(),
		'hasNotifiedUser': false
	};

	db.users.findAndModify(query, function(error, doc) {
		if (error || !doc) {
			console.error('Could not find user when adding to wish list:', error);
			error = new Error('Brugeren blev ikke fundet.');
			error.code = 'not_found';
			error.status = 404;
			return callback(error);
		}

		callback(null, { status: 'ok' });
	});
}

function removeFromWishlist(userId, courseId, callback) {
	if (!courseId) {
		error = new Error('Mangler kursus-ID.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	var idMatch = ('' + courseId).match(/^(s?\d{1,8})([evs]?)$/);
	var error;

	if (!idMatch) {
		error = new Error('Ugyldig kursus-ID.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}
	courseId = idMatch[1];

	var query = {
		query: { _id: db.decodeId(userId) },
		update: {
			$unset: {}
		}
	};
	query.update.$unset['wishlist.' + courseId] = '';

	db.users.findAndModify(query, function(error, doc) {
		if (error || !doc) {
			console.error('Could not find user when removing from wish list:', error);
			error = new Error('Brugeren blev ikke fundet.');
			error.code = 'not_found';
			error.status = 404;
			return callback(error);
		}

		callback(null, { status: 'ok' });
	});
}

exports.list = listWishlist;
exports.getItem = getWishlistItem;
exports.addItem = addToWishlist;
exports.removeItem = removeFromWishlist;
