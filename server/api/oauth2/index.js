/** @module server/api/oauth2 */
'use strict';

exports.provider = require('./provider');
exports.token = require('./token');
