/** @module server/api/oauth2/token */
'use strict';

var provider = require('./provider');

function authenticateClient(client, options, callback) {
	var authIsOptional = (options.grant_type === 'refresh_token');
	var error;

	if (client.headers.authorization) {
		error = new Error('HTTP Authorization understøttes ikke på nuværende tidspunkt.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (authIsOptional && !options.client_id) {
		return callback(null);
	}

	if (options.client_id === 'evukurser.dk') {
		if (process.env.NODE_ENV === 'production' && !(
				(
					client.headers.host === 'evukurser.dk'
				) && (
					client.headers.origin === undefined ||
					client.headers.origin === 'https://evukurser.dk'
				) && (
					client.headers.referer === undefined ||
					/^https:\/\/evukurser\.dk\//.test('' + client.headers.referer)
				)
			)) {

			error = new Error('Ukendt klient.');
			error.code = 'invalid_client';
			error.status = 400;
			callback(error);
		}
		if (options.client_secret) {
			error = new Error('Offentlige klienter må ikke medsende hemmelig nøgle.');
			error.code = 'invalid_request';
			error.status = 400;
			callback(error);
		}

		client.id = options.client_id;
		callback(null);
	} else {
		error = new Error('Ukendt klient.');
		error.code = 'invalid_client';
		error.status = 400;
		callback(error);
	}
}

function createPasswordToken(client, options, callback) {
	var username = ('' + [options.username]).trim().toLowerCase();
	var password = '' + [options.password];

	provider.authenticate({ email: username }, password, function(error, user) {
		if (error) return callback(error);

		provider.token(client, user, user.roles.sort().join(' '), null, function(error, auth) {
			if (error) return callback(error);

			callback(null, auth);
		});
	});
}

function createRefreshToken(client, options, callback) {
	var refreshToken = '' + [options.refresh_token];
	var error;

	if (!/^[A-Za-z0-9_\-]{22}$/.test(refreshToken)) {
		error = new Error('Ugyldig refresh_token.');
		error.code = 'invalid_grant';
		error.status = 400;
		return callback(error);
	}

	provider.refresh(client, refreshToken, callback);
}

function createToken(client, options, callback) {
	options.grant_type = '' + [options.grant_type];
	options.client_id = '' + [options.client_id];

	authenticateClient(client, options, function(error) {
		if (error) return callback(error);

		if (options.grant_type === 'password') {
			return createPasswordToken(client, options, callback);
		} else if (options.grant_type === 'refresh_token') {
			return createRefreshToken(client, options, callback);
		} else {
			error = new Error('Ukendt eller ikke implementeret grant type.');
			error.code = 'unsupported_grant_type';
			error.status = 400;
			callback(error);
		}
	});
}

exports.create = createToken;
