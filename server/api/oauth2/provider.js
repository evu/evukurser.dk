/** @module server/api/oauth2/provider */
'use strict';

var common = require('common');
var scrypt = require('scrypt');
var uuid = require('../../uuid');
var user = require('../user');
var db = require('../../db');

var refreshTokenMaxTime = 31 * 24 * 60 * 60; // 31 days
var accessTokenMaxTime = 3600; // 1 hour

function authenticateUser(query, password, callback) {
	var result = common.future();

	setTimeout(function() {
		result.get(callback);
	}, 300);

	db.users.findOne(query, function(error, doc) {
		if (error || !(doc && doc.password)) {
			console.error('Could not find user when authentication:', error);
			error = new Error('Ugyldig brugernavn og/eller adgangskode.');
			error.code = 'invalid_grant';
			error.status = 400;
			return result.put(error);
		}

		if (!doc.emailVerifiedTime) {
			console.error('User did not yet verify the e-mail and can not be authenticated:', error);
			error = new Error('E-mail-adressen er endnu ikke verificeret.');
			error.code = 'not_verified';
			error.status = 403;
			return result.put(error);
		}

		scrypt.verifyHash(('' + doc.password) || 'fail', ('' + password) || ' ', function(error, passwordMatches) {
			if (error || !passwordMatches) {
				console.error('Could not verify password for user:', error);
				error = new Error('Ugyldig brugernavn og/eller adgangskode.');
				error.code = 'invalid_grant';
				error.status = 400;
				return result.put(error);
			}

			result.put(null, user.format(doc));
		});
	});
}

function createToken(client, user, scope, oldRefreshToken, callback) {
	var now = new Date();
	var userInfo = { id: db.decodeId(user.id), name: user.name, email: user.email };

	var errorMessage = oldRefreshToken ?
		'Der opstod en fejl med at opdatere loginforløb.' :
		'Der opstod en fejl ved login.';

	db.refreshToken.save({
		_id: uuid(),
		time: now,
		client: client,
		user: userInfo,
		scope: scope,
		previousToken: oldRefreshToken || null
	}, function(error, refreshToken) {
		if (error || !refreshToken) {
			console.error('Could not create refresh token:', error);
			error = new Error(errorMessage);
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		if (oldRefreshToken) {
			var query = {
				query: {
					_id: oldRefreshToken,
					'deprecated.time': { $exists: true }
				},
				update: {
					$set: {
						'deprecated.nextToken': refreshToken._id
					}
				}
			};
			db.refreshToken.findAndModify(query, function(error) {
				if (error) {
					console.error('Could not update old refresh token to point at new refresh token:', error);
				}
			});
		}

		db.accessToken.save({
			_id: uuid(),
			time: now,
			client: client,
			user: refreshToken.user,
			scope: refreshToken.scope,
			refreshToken: refreshToken._id
		}, function(error, accessToken) {
			if (error) {
				console.error('Could not create access token:', error);
				error = new Error(errorMessage);
				error.code = 'server_error';
				error.status = 500;
				return callback(error);
			}

			callback(null, {
				access_token: accessToken._id,
				token_type: 'bearer',
				expires_in: accessTokenMaxTime,
				refresh_token: refreshToken._id,
				scope: refreshToken.scope,
				user: user
			});
		});
	});
}

function refreshToken(client, currentRefreshToken, callback) {
	var now = new Date();
	var expiredTime = new Date(now - refreshTokenMaxTime * 1000);

	var query = {
		query: {
			_id: currentRefreshToken,
			'client.id': client.id,
			time: {
				$gte: expiredTime,
				$lte: now
			},
			deprecated: { $exists: false }
		},
		update: {
			$set: {
				deprecated: {
					time: now
				}
			}
		},
		new: true
	};

	db.refreshToken.findAndModify(query, function(error, refreshToken) {
		if (error || !refreshToken || !refreshToken.deprecated) {
			console.error('Could not find refresh token:', error);
			error = new Error('Der opstod en fejl med at opdatere loginforløb.');
			error.code = 'invalid_grant';
			error.status = 400;
			return callback(error);
		}

		db.users.findOne({ _id: refreshToken.user.id }, function(error, doc) {
			if (error || !doc) {
				console.error('Could not find user when refreshing token:', error);
				error = new Error('Brugeren blev ikke fundet.');
				error.code = 'server_error';
				error.status = 500;
				return callback(error);
			}

			createToken(client, user.format(doc), refreshToken.scope, currentRefreshToken, callback);
		});
	});
}

function lookupToken(accessToken, callback) {
	accessToken = '' + accessToken;
	var error;

	if (!/^[A-Z0-9_\-]{22}$/i.test(accessToken)) {
		error = new Error('Ugyldig token.');
		error.code = 'invalid_grant';
		error.status = 401;
		return callback(error);
	}

	var now = new Date();
	var expiredTime = new Date(now - (accessTokenMaxTime + 5) * 1000);

	var query = {
		_id: accessToken,
		time: {
			$gte: expiredTime,
			$lte: now
		},
		deprecated: { $exists: false }
	};

	db.accessToken.findOne(query, function(error, accessToken) {
		if (error || !accessToken) {
			console.error('Could not find access token:', error);
			error = new Error('Login påkrævet.');
			error.code = 'invalid_grant';
			error.status = 401;
			return callback(error);
		}

		var expiresIn = Math.max(0, accessTokenMaxTime - Math.round(0.001 * (now - accessToken.time)));
		var scope = {};

		accessToken.scope.split(' ').forEach(function(scopeLevel) {
			scope[scopeLevel] = true;
		});

		callback(null, {
			token: accessToken._id,
			expires: expiresIn,
			scope: scope,
			client: accessToken.client,
			user: {
				id: db.encodeId(accessToken.user.id),
				name: accessToken.user.name,
				email: accessToken.user.email
			}
		});
	});
}

exports.authenticate = authenticateUser;
exports.token = createToken;
exports.refresh = refreshToken;
exports.lookup = lookupToken;
