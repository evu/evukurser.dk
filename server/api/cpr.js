/** @module server/api/cpr */
'use strict';

var config = require('../config');
var crypto = require('crypto');
var email = require('../email');
var nemid = require('../nemid');
var unic = require('../unic');
var uuid = require('../uuid');
var db = require('../db');

var maxDaysInMonth = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

var cprValidateTimeout = 10 * 60; // 10 minutes

var cprKey1 = ('' + [config.ws.cprEncryptionKey]) || 'dev';
delete config.ws.cprEncryptionKey; // do not leak key in all modules

var cprKey2 = crypto.createHmac('sha256', 'R8JdRt1wONFB/ooSVb4pFhuS1ynTe309Co5Kzz6kPWY').update(cprKey1).digest('base64');

function encryptCPR(cpr) {
	cpr = '' + [cpr];

	try {
		var birthday = cpr.slice(0, 6);
		var salt = uuid();

		var cipher = crypto.createCipher('aes256', salt + '.' + birthday);
		var level1 = Buffer.concat([ cipher.update(cpr.slice(6), 'utf8'), cipher.final() ]).toString('base64');
		level1 = salt + '.' + level1;

		cipher = crypto.createCipher('aes256', cprKey1);
		var level2 = Buffer.concat([ cipher.update(level1, 'utf8'), cipher.final() ]).toString('base64');

		cipher = crypto.createCipher('aes256', cprKey2);
		var level3 = Buffer.concat([ cipher.update(birthday + '.' + level2, 'utf8'), cipher.final() ]).toString('base64');

		return level3;
	} catch (error) {
		return '';
	}
}

function decryptCPR(encryptedCPR) {
	encryptedCPR = '' + [encryptedCPR];

	try {
		var level3 = new Buffer(encryptedCPR, 'base64');

		var decipher = crypto.createDecipher('aes256', cprKey2);
		var level2b = Buffer.concat([ decipher.update(level3), decipher.final() ]).toString('utf8');

		var birthday = level2b.slice(0, 6);
		var level2 = new Buffer(level2b.slice(7), 'base64');

		decipher = crypto.createDecipher('aes256', cprKey1);
		var level1 = Buffer.concat([ decipher.update(level2), decipher.final() ]).toString('utf8');

		var items = level1.split('.');
		var salt = items[0];
		level1 = new Buffer('' + items[1], 'base64');

		decipher = crypto.createDecipher('aes256', salt + '.' + birthday);
		var code = Buffer.concat([ decipher.update(level1), decipher.final() ]).toString('utf8');

		return birthday + code;
	} catch (error) {
		return '';
	}
}

function decryptCPRDate(encryptedCPR) {
	encryptedCPR = '' + [encryptedCPR];

	try {
		var level3 = new Buffer(encryptedCPR, 'base64');

		var decipher = crypto.createDecipher('aes256', cprKey2);
		var level2b = Buffer.concat([ decipher.update(level3), decipher.final() ]).toString('utf8');

		var birthday = level2b.slice(0, 6);
		return birthday + '****';
	} catch (error) {
		return '';
	}
}

function createCPR(auth, userId, options, callback) {
	var cpr = ('' + [options.cpr]).replace(/\s+/g, '');
	var error;

	if (!/^[A-Za-z0-9_\-]{16}$/.test(userId)) {
		error = new Error('Ugyldig bruger-ID.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (!cpr) {
		error = new Error('Et CPR-nummer er påkrævet.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	var cprMatch = cpr.match(/^(\d{6})-?(\d{4})$/);
	if (!cprMatch) {
		error = new Error('Det angivne CPR-nummer er ikke gyldigt.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}
	cpr = cprMatch[1] + cprMatch[2];

	var day = cpr.slice(0, 2) | 0;
	var month = cpr.slice(2, 4) | 0;

	if (!(month >= 1 && month <= 12 && day >= 1 && day <= maxDaysInMonth[month - 1])) {
		error = new Error('Det angivne CPR-nummer er ikke gyldigt.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	var dbUserId = db.decodeId(userId);

	db.users.findOne({ _id: dbUserId }, function(error, user) {
		if (error) {
			error = new Error('Der opstod en fejl med at finde brugeren.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		if (!user) {
			error = new Error('Brugeren blev ikke fundet.');
			error.code = 'not_found';
			error.status = 404;
			return callback(error);
		}

		if (user.cpr && user.cpr.validated) {
			error = new Error('Der er allerede et valideret CPR-nummer. Hvis du vil ændre CPR-nummeret skal du slette det først.');
			error.code = 'conflict';
			error.status = 409;
			return callback(error);
		}

		nemid.loginParams(function(error, params) {
			if (error || !(params.applet && params.applet.origin)) {
				console.log('Could not load login params from NemID server:', error.stack || error);
				error = new Error('Der opstod en fejl. Prøv igen om lidt.');
				error.code = 'server_error';
				error.status = 500;
				return callback(error);
			}

			if (process.env.NODE_ENV !== 'production' && /127\.0\.0/.test(params.applet.origin)) {
				params.applet.origin = 'http://' + auth.client.headers.host;
			}

			var token = uuid();

			var query = {
				query: {
					_id: db.decodeId(userId),
				},
				update: {
					$set: {
						cpr: {
							data: encryptCPR(cpr),
							validated: null,
							token: token,
							created: {
								auth: auth,
								time: new Date()
							},
							nemid: params
						}
					}
				}
			};

			db.users.findAndModify(query, function(error, user) {
				if (error || !user) {
					error = new Error('Der opstod en fejl med at finde brugeren.');
					error.code = 'server_error';
					error.status = 500;
					return callback(error);
				}

				callback(null, {
					nemid: {
						parameters: params.parameters,
						applet: params.applet
					},
					postBack: {
						token: token,
						expires_in: cprValidateTimeout
					}
				});
			});
		});
	});
}

function validateCPR(auth, userId, cprToken, options, callback) {
	var response = '' + [options.response];
	var error, query;

	if (!/^[A-Za-z0-9_\-]{16}$/.test(userId)) {
		error = new Error('Ugyldig bruger-ID.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (!/^[A-Za-z0-9_\-]{22}$/.test(cprToken)) {
		error = new Error('Ugyldig CPR-token.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (!response) {
		error = new Error('Et svar fra NemID-login er påkrævet.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	var dbUserId = db.decodeId(userId);

	// TODO: Just for testing.
	if (process.env.NODE_ENV !== 'production' && response === 'test') {
		query = {
			query: {
				_id: dbUserId,
				'cpr.token': cprToken,
				'cpr.validated': null,
				'cpr.created.time': {
					$lt: new Date(),
					$gt: new Date(Date.now() - cprValidateTimeout * 1000)
				}
			},
			update: {
				$set: {
					'cpr.validated': {
						nemid: response,
						auth: auth,
						time: new Date()
					}
				},
				$unset: {
					'cpr.token': '',
					'cpr.nemid.session': ''
				}
			}
		};

		db.users.findAndModify(query, function(error, user) {
			if (error) {
				error = new Error('Der opstod en fejl med at finde brugeren.');
				error.code = 'server_error';
				error.status = 500;
				return callback(error);
			}

			if (!user) {
				error = new Error('CPR-nummeret er allerede valideret eller der opstod en fejl med at validere CPR-nummeret.');
				error.code = 'not_found';
				error.status = 404;
				return callback(error);
			}

			callback(null, { status: 'ok' });
		});
		return;
	}

	db.users.findOne({ _id: dbUserId }, function(error, user) {
		if (error || !user) {
			error = new Error('Der opstod en fejl med at finde brugeren.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		if (!(user.cpr && user.cpr.data && user.cpr.created && user.cpr.created.time && user.cpr.created.time instanceof Date)) {
			error = new Error('Du skal først bede om CPR-validering.');
			error.code = 'invalid_request';
			error.status = 400;
			return callback(error);
		}

		if (!(user.cpr.nemid && user.cpr.nemid.session)) {
			console.log('Missing `nemid` and/or `nemid.session` property on non-validated CPR object.');
			error = new Error('Der opstod en fejl. Prøv venligst igen.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		if (user.cpr.validated) {
			error = new Error('Brugeren er allerede CPR-valideret.');
			error.code = 'invalid_request';
			error.status = 400;
			return callback(error);
		}

		if (user.cpr.token !== cprToken) {
			error = new Error('Systemfejl: Ugyldigt token angivet.');
			error.code = 'invalid_request';
			error.status = 400;
			return callback(error);
		}

		var time = user.cpr.created.time.getTime();
		var now = Date.now();

		if (time < now - cprValidateTimeout * 1000 || time > now) {
			error = new Error('Du har ikke logget ind med NemID hurtigt nok efter at have indtastet CPR-nummer. Prøv igen.');
			error.code = 'invalid_request';
			error.status = 400;
			return callback(error);
		}

		nemid.login(user.cpr.nemid.session, response, function(error, login) {
			if (error) {
				console.log('Could not process NemID login:', error.stack || error);
				error = new Error('Der opstod en fejl. Prøv venligst igen.');
				error.code = 'server_error';
				error.status = 500;
				nemid.logout(user.cpr.nemid.session);
				return callback(error);
			}

			if (!(login.status === 'logged_in' && login.pid)) {
				console.log('NemID login failed:', login);
				var msg = 'Der opstod en fejl med at logge ind med NemID.';
				var reason = '' + [login.reason];
				if (/\bnull\b/.test(reason)) reason = '';
				error = new Error(msg + (reason ? ' Årsag: ' + reason : ''));
				error.code = 'forbidden';
				error.status = 403;
				nemid.logout(user.cpr.nemid.session);
				email.cprFailed.send(user.email, user.name);
				return callback(error);
			}

			nemid.cprMatch(login.session, decryptCPR(user.cpr.data), login.pid, function(error, match) {
				nemid.logout(login.session);

				if (error) {
					console.log('Could not perform CPR/PID match:', error.stack || error);
					error = new Error('Der opstod en fejl. Prøv venligst igen.');
					error.code = 'server_error';
					error.status = 500;
					return callback(error);
				}

				if (match.status !== 'matches') {
					console.log('CPR/PID match failed:', match);
					error = new Error('Det angivne CPR-nummer stemmer ikke med dit NemID-login.');
					error.status = 403;
					error.code = 'forbidden';
					email.cprFailed.send(user.email, user.name);
					return callback(error);
				}

				query = {
					query: {
						_id: dbUserId,
						'cpr.token': cprToken,
						'cpr.validated': null,
						'cpr.created.time': {
							$lt: new Date(),
							$gt: new Date(Date.now() - cprValidateTimeout * 1000)
						}
					},
					update: {
						$set: {
							'cpr.validated': {
								nemid: response,
								auth: auth,
								time: new Date()
							}
						},
						$unset: {
							'cpr.token': '',
							'cpr.nemid.session': ''
						}
					},
					new: true
				};

				db.users.findAndModify(query, function(error, user) {
					if (error || !user) {
						console.log('Failed to update CPR validation for user:', error.stack || error);
						error = new Error('Der opstod en fejl med at CPR-validere brugeren.');
						error.code = 'server_error';
						error.status = 500;
						return callback(error);
					}

					email.cprAdded.send(user.email, user.name);

					callback(null, { status: 'ok' });
				});
			});
		});
	});
}

function removeCPR(auth, userId, callback) {
	var error;

	if (!/^[A-Za-z0-9_\-]{16}$/.test(userId)) {
		error = new Error('Ugyldig bruger-ID.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	var query = {
		query: {
			_id: db.decodeId(userId)
		},
		update: {
			$set: {
				'cpr.validated': null,
				'cpr.removed': {
					auth: auth,
					time: new Date()
				}
			},
			$unset: {
				'cpr.data': '',
				'cpr.token': '',
				'cpr.nemid': '',
				'cpr.completedClasses': ''
			}
		},
		new: true
	};

	db.users.findAndModify(query, function(error, user) {
		if (error) {
			error = new Error('Der opstod en fejl med at finde brugeren.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		if (!user) {
			error = new Error('Brugeren blev ikke fundet.');
			error.code = 'not_found';
			error.status = 404;
			return callback(error);
		}

		email.cprRemoved.send(user.email, user.name);

		callback(null, { status: 'ok' });
	});
}

function updateInTest(auth, userId, options, callback) {
	var error;

	if (process.env.NODE_ENV === 'production') {
		error = new Error('Resourcen blev ikke fundet.');
		error.code = 'not_found';
		error.status = 404;
		return callback(error);
	}

	var cpr = ('' + [options.cpr]).replace(/\s+/g, '');

	if (!/^[A-Za-z0-9_\-]{16}$/.test(userId)) {
		error = new Error('Ugyldig bruger-ID.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (!cpr) {
		error = new Error('Et CPR-nummer er påkrævet.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	var cprMatch = cpr.match(/^(\d{6})-?(\d{4})$/);
	if (!cprMatch) {
		error = new Error('Det angivne CPR-nummer er ikke gyldigt.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}
	cpr = cprMatch[1] + cprMatch[2];

	var day = cpr.slice(0, 2) | 0;
	var month = cpr.slice(2, 4) | 0;

	if (!(month >= 1 && month <= 12 && day >= 1 && day <= maxDaysInMonth[month - 1])) {
		error = new Error('Det angivne CPR-nummer er ikke gyldigt.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	var dbUserId = db.decodeId(userId);

	db.users.findOne({ _id: dbUserId }, function(error, user) {
		if (error) {
			error = new Error('Der opstod en fejl med at finde brugeren.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		if (!user) {
			error = new Error('Brugeren blev ikke fundet.');
			error.code = 'not_found';
			error.status = 404;
			return callback(error);
		}

		var query = {
			query: {
				_id: dbUserId
			},
			update: {
				$set: {
					'cpr.data': encryptCPR(cpr),
					'cpr.validated': {
						auth: auth,
						time: new Date()
					},
					'cpr.created': {
						auth: auth,
						time: new Date()
					}
				},
				$unset: {
					'cpr.token': '',
					'cpr.nemid.session': ''
				}
			}
		};

		db.users.findAndModify(query, function(error, user) {
			if (error || !user) {
				error = new Error('Der opstod en fejl med at finde brugeren.');
				error.code = 'server_error';
				error.status = 500;
				return callback(error);
			}

			callback(null, { status: 'ok' });
		});
	});
}

function completedClasses(encryptedCPR, callback) {
	unic.completedClasses(decryptCPR(encryptedCPR), callback);
}

exports.decryptCPRDate = decryptCPRDate;
exports.create = createCPR;
exports.validate = validateCPR;
exports.remove = removeCPR;
exports.updateInTest = updateInTest;
exports.completedClasses = completedClasses;
