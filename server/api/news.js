/** @module server/api/news/create */
'use strict';

var dataurl = require('../dataurl');
var common = require('common');
var db = require('../db');

function formatNews(doc, options) {
	var simpleRepresentation = !!(options && options.simple);
	doc = doc || {};

	var news = {};

	news.id = db.encodeId(doc._id);

	news.title = '' + [doc.title];

	news.article = {
		markdown: '' + [doc.article && doc.article.markdown]
	};

	if (doc.image) {
		news.image = doc.image.normal;
	}

	if (!simpleRepresentation) {
		news.image = {
			normal: '' + [doc.image && doc.image.normal]
		};
	}

	news.author = {
		name: '' + [doc.author && doc.author.name],
		email: '' + [doc.author && doc.author.email]
	};

	news.created = {
		time: (doc.created && doc.created.time) || null
	};

	if (doc.updated) {
		news.updated = {
			time: doc.updated.time || null
		};
	}

	return news;
}

function createNews(clientInfo, options, callback) {
	var error;

	options.title = ('' + options.title).replace(/\s+/g, ' ').trim();
	options['article-markdown'] = ('' + [options['article-markdown']]).trim();
	options.image = ('' + [options.image]).trim() || null;
	options.name = ('' + [options.name]).replace(/\s+/g, ' ').trim();
	options.email = ('' + [options.email]).trim();

	if (!options.title) {
		error = new Error('En titel er påkrævet.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (!options['article-markdown']) {
		error = new Error('En artikel er påkrævet.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (!options.name) {
		error = new Error('Et navn er påkrævet.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (!options.email) {
		error = new Error('En e-mail-adresse er påkrævet.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (!/^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,20}$/.test(options.email)) {
		error = new Error('Den angivne e-mail-adresse er ugyldig.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (options.image) {
		options.image = dataurl.image(options.image);
		if (!options.image) {
			error = new Error('Det angivne billede er i et ugyldigt format.');
			error.code = 'invalid_request';
			error.status = 400;
			return callback(error);
		}
	}

	db.news.save({
		title: options.title,
		article: {
			markdown: options['article-markdown']
		},
		image: {
			normal: options.image,
		},
		author: {
			name: options.name,
			email: options.email
		},
		created: {
			client: clientInfo,
			time: new Date()
		}
	}, function(error, doc) {
		if (error || !doc) {
			console.error('Could not create news object:', error);
			error = new Error('Der opstod en fejl ved oprettelse af nyheden.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		callback(null, formatNews(doc));
	});
}

function updateNews(clientInfo, id, options, callback) {
	var error;
	var updateSet = {};

	if ('title' in options) {
		updateSet.title = ('' + options.title).replace(/\s+/g, ' ').trim();
		if (!updateSet.title) {
			error = new Error('Titel er påkrævet.');
			error.code = 'invalid_request';
			error.status = 400;
			return callback(error);
		}
	}

	if ('article-markdown' in options) {
		updateSet['article.markdown'] = ('' + [options['article-markdown']]).trim();
		if (!updateSet['article.markdown']) {
			error = new Error('Artikel er påkrævet.');
			error.code = 'invalid_request';
			error.status = 400;
			return callback(error);
		}
	}

	if ('name' in options) {
		updateSet['author.name'] = ('' + [options.name]).replace(/\s+/g, ' ').trim();
		if (!updateSet['author.name']) {
			error = new Error('Navn er påkrævet.');
			error.code = 'invalid_request';
			error.status = 400;
			return callback(error);
		}
	}

	if ('email' in options) {
		updateSet['author.email'] = ('' + [options.email]).trim();
		if (!updateSet['author.email']) {
			error = new Error('E-mail-adresse er påkrævet.');
			error.code = 'invalid_request';
			error.status = 400;
			return callback(error);
		}

		if (!/^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,20}$/.test(updateSet['author.email'])) {
			error = new Error('Den angivne e-mail-adresse er ugyldig.');
			error.code = 'invalid_request';
			error.status = 400;
			return callback(error);
		}
	}

	if ('image' in options) {
		updateSet['image.normal'] = ('' + [options.image]).trim() || null;
		if (updateSet['image.normal']) {
			updateSet['image.normal'] = dataurl.image(updateSet['image.normal']);
			if (!updateSet['image.normal']) {
				error = new Error('Det angivne billede er i et ugyldigt format.');
				error.code = 'invalid_request';
				error.status = 400;
				return callback(error);
			}
		}
	}

	if (Object.keys(updateSet).length === 0) {
		error = new Error('Der er ikke angivet nogle opdateringer.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	updateSet.updated = {
		client: clientInfo,
		time: new Date()
	};

	var query = {
		query: {
			_id: db.decodeId(id)
		},
		update: {
			$set: updateSet
		},
		new: true
	};

	db.news.findAndModify(query, function(error, doc) {
		if (error || !doc) {
			console.error('Could not update news object:', error);
			error = new Error('Nyheden blev ikke fundet eller kunne ikke opdateres.');
			error.code = 'not_found';
			error.status = 404;
			return callback(error);
		}

		callback(null, formatNews(doc));
	});
}

function removeNews(id, callback) {
	db.news.remove({ _id: db.decodeId(id) }, function(error) {
		if (error) {
			console.error('Could not delete news object:', error);
			error = new Error('Nyheden blev ikke slettet.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		callback(null, { status: 'ok' });
	});
}

function listNews(options, callback) {
	var offset = Math.max(0, options.offset | 0);
	var limit = Math.max(0, Math.min(1000, options.limit | 0 || 10));

	var cursor = db.news.find({}).sort({ 'created.time': -1 });

	var newsCount = common.future();
	cursor.count(newsCount.put);

	cursor.limit(limit).skip(offset, function(error, docs) {
		if (error) {
			console.error('Could not find list of news:', error);
			error = new Error('Der opstod en fejl ved indlæsning af nyheder.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		docs = docs || [];

		newsCount.get(function(error, count) {
			if (error) {
				console.error('Could not count list of news:', error);
				error = new Error('Der opstod en fejl ved indlæsning af nyheder.');
				error.code = 'server_error';
				error.status = 500;
				return callback(error);
			}

			callback(null, {
				pagination: {
					offset: offset,
					limit: limit
				},
				count: count,
				items: docs.map(function(doc) {
					return formatNews(doc, { simple: true });
				})
			});
		});
	});
}

function getNews(id, callback) {
	db.news.findOne({ _id: db.decodeId(id) }, function(error, doc) {
		if (error || !doc) {
			console.error('Could not lookup news object:', error);
			error = new Error('Nyheden blev ikke fundet.');
			error.code = 'not_found';
			error.status = 404;
			return callback(error);
		}

		callback(null, formatNews(doc));
	});
}

exports.create = createNews;
exports.update = updateNews;
exports.remove = removeNews;
exports.list = listNews;
exports.get = getNews;
