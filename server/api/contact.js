/** @module server/api/contact */
'use strict';

var email = require('../email');

function contact(clientInfo, options, callback) {
	var error;

	options.name = ('' + [options.name]).replace(/\s+/g, ' ').trim();
	options.email = ('' + [options.email]).trim();
	options.message = ('' + [options.message]).replace(/\r\n?/g, '\n').replace(/\n{3,}/g, '\n\n').replace(/[ \t]+/g, ' ').trim();

	if (!options.name) {
		error = new Error('Dit navn er påkrævet.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (!options.email) {
		error = new Error('Din e-mail-adresse er påkrævet.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (!options.message) {
		error = new Error('En besked er påkrævet.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (!/^[A-Z0-9._%+\-]+@[A-Z0-9.\-]+\.[A-Z]{2,20}$/i.test(options.email)) {
		error = new Error('Den angivne e-mail-adresse er ugyldig.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	email.contact.send(options.email, options.name, options.message, function(error) {
		if (error) {
			console.error('Could not send contact e-mail:', error);
			error = new Error('Kunne ikke sende beskeden.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		callback(null, { name: options.name, email: options.email, message: options.message });
	});
}

module.exports = contact;
