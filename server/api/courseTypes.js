/** @module server/api/courseTypes */
'use strict';

var clientCourseTypes = require('../../app/enums/courseTypes');

var courseTypes = {};
for (var id in clientCourseTypes) {
	var clientType = clientCourseTypes[id];
	var name = clientType.name;

	courseTypes[id] = {
		id: id,
		name: name,
		appendId: name[0].toLowerCase()
	};
}

module.exports = courseTypes;
