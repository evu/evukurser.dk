/** @module server/api/completedClasses */
'use strict';

var courseModule = require('./course');
var courseTypes = require('./courseTypes');
var common = require('common');
var cpr = require('./cpr');
var db = require('../db');

function noop() {}

function updateCompletedClassesForUser(userId, callback) {
	var dbUserId = db.decodeId(userId);
	callback = callback || noop;

	common.step([

		function(next) {
			db.users.findOne({ _id: dbUserId }, next);
		},

		function(user, next) {
			if (!user) return next(new Error('User not found.'));
			if (!(user.cpr && user.cpr.validated && user.cpr.data)) return next(new Error('User is not CPR validated.'));

			cpr.completedClasses(user.cpr.data, next);
		},

		function(completedClasses, next) {
			completedClasses.updated = { time: new Date() };

			var query = {
				query: { _id: dbUserId },
				update: {
					$set: {
						'cpr.completedClasses': completedClasses
					}
				},
				new: true
			};

			db.users.findAndModify(query, next);
		},

		function(user) {
			callback(null, user && user.cpr && user.cpr.completedClasses);
		}

	], function(error) {
		console.log('Could not load completed classes from UNI-C proxy:', error.stack || error);
		error = new Error('Der opstod en fejl med at hente listen af gennemførte kurser.');
		error.code = 'server_error';
		error.status = 500;

		callback(error);
	});
}

function normalizeCompletedClasses(completedClasses, callback) {
	var courseIds = {};
	var schoolId = null;

	completedClasses.items.forEach(function(cls) {
		cls.courses.forEach(function(course) {
			var id = course.id | 0;
			course.title = 'Ukendt kursusnavn';
			course.description = course.description ? { text: course.description } : null;
			course.rating = null;
			course.links = [];
			course.type = {
				id: 'other',
				name: 'Andre'
			};

			if (id) {
				id = '' + id;
				course.id = id;
				courseIds[id] = true;
			}
		});

		if (cls.school) {
			if (cls.school.id) {
				schoolId = '' + cls.school.id;
				cls.school.id = schoolId;
			}
			var name = ('' + [cls.school.name]).trim();
			var phoneOriginal = ('' + [cls.school.phone]).trim();
			var phone = phoneOriginal.replace(/\s+/, '').replace(/^\+45/, '');
			if (!/^[0-9]{8}$/.test(phone)) {
				if (name) name += ', ';
				name += phoneOriginal;
				phone = null;
			}
			cls.school.phone = phone;
			cls.school.name = name;
		}
	});
	courseIds = Object.keys(courseIds);

	db.courses.find({ _id: { '$in': courseIds } }, {
		'src.title': 1,
		'src.description': 1,
		'overwrites.rating': 1,
		'overwrites.types': 1,
		'overwrites.links': 1
	}, function(error, dbCourses) {
		if (error) {
			console.log('Could not lookup completed courses in local DB (continuing with unknown course titles):', error.stack || error);
		}

		var indexedDbCourses = {};
		dbCourses.forEach(function(dbCourse) {
			indexedDbCourses[dbCourse._id] = dbCourse;
		});

		completedClasses.items.forEach(function(cls) {
			cls.courses.forEach(function(course) {
				var dbCourse = indexedDbCourses[course.id];
				if (dbCourse) {
					var descriptionText = '' + [dbCourse.src.description && dbCourse.src.description.text];
					var types = dbCourse.overwrites.types;
					var type = courseTypes[types && types[0]];

					course.links = courseModule.formatLinks(dbCourse.overwrites.links);
					course.title = dbCourse.src.title;
					course.description = descriptionText ? { text: descriptionText } : null;
					course.rating = dbCourse.overwrites.rating || null;
					if (type) {
						course.type = {
							id: type.id,
							name: type.name
						};
					}
				}
			});
		});

		callback(null, {
			items: completedClasses.items,
			count: completedClasses.items.length
		});
	});
}

function listRawCompletedClasses(userId, callback) {
	callback = common.once(callback);

	db.users.findOne({ _id: db.decodeId(userId) }, function(error, doc) {
		if (error) {
			console.error('Could not find user:', error.stack || error);
			error = new Error('Der opstod en fejl med at finde brugeren.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		if (!doc) {
			error = new Error('Brugeren blev ikke fundet.');
			error.code = 'not_found';
			error.status = 404;
			return callback(error);
		}

		if (!(doc.cpr && doc.cpr.validated && doc.cpr.data)) {
			error = new Error('Brugeren er ikke CPR-valideret.');
			error.code = 'forbidden';
			error.status = 403;
			return callback(error);
		}

		if (doc.cpr.completedClasses) {
			callback(null, doc.cpr.completedClasses);
		}

		var updatedTime = new Date(0); // long time in the past
		if (doc.cpr.completedClasses) {
			updatedTime = doc.cpr.completedClasses.updated.time;
		}

		// Update completed courses again, if older than one day or if not existing.
		if (new Date() - updatedTime > 24 * 60 * 60 * 1000) {
			updateCompletedClassesForUser(userId, callback);
		}
	});
}

function listCompletedClasses(userId, callback) {
	listRawCompletedClasses(userId, function(error, completedClasses) {
		if (error) return callback(error);
		normalizeCompletedClasses(completedClasses, callback);
	});
}

exports.updateCompletedClassesForUser = updateCompletedClassesForUser;
exports.list = listCompletedClasses;
