/** @module server/api/course */
'use strict';

var courseTypes = require('./courseTypes');
var common = require('common');
var db = require('../db');
var moment = require('moment');

function formatCourseLinks(courseLinks) {
	if (!Array.isArray(courseLinks)) return [];

	var now = new Date();
	var currentDate = new Date(now.getTime() - now.getTimezoneOffset() * 60 * 1000).toJSON().substr(0, 10);

	return courseLinks.filter(function(link) {
		if (link.begin && link.begin > currentDate) return false;
		if (link.end && link.end < currentDate) return false;
		return true;
	}).map(function(link) {
		return {
			url: link.url,
			since: link.begin || null
		};
	});
}

function formatCourse(isAuthenticated, doc, overwriteType, options) {
	doc = doc || {};
	options = options || {};

	if (overwriteType && overwriteType.id) {
		overwriteType = overwriteType.id;
	}

	var src = doc.src || {};
	var overwrites = doc.overwrites || {};

	var types = overwrites.types;
	var type = courseTypes[overwriteType || (types && types[0])] || {
		id: 'other',
		name: 'Andre',
		appendId: ''
	};

	var jobareas = (overwrites.jobareas && overwrites.jobareas[type.id]) || [];
	if (jobareas.length === 0) {
		jobareas.push({
			main: null,
			sub: null,
			text: 'Ingen område'
		});
	}

	var themes = (overwrites.themes && overwrites.themes[type.id]) || [];
	if (themes.length === 0) {
		themes.push({
			main: null,
			sub: null,
			text: 'Ingen pakke'
		});
	}

	var classes = doc.classes || [];
	classes.sort(function(a, b) {
		var aBegin = '' + [a && a.duration && a.duration.begin];
		var bBegin = '' + [b && b.duration && b.duration.begin];
		var cmp = aBegin.localeCompare(bBegin);
		if (cmp) return cmp;

		var aSchoolName = '' + [a && a.school && a.school.name];
		var bSchoolName = '' + [b && b.school && b.school.name];

		cmp = aSchoolName.localeCompare(bSchoolName);
		return cmp;
	});

	if (src.isCommon) {
		type = {
			id: 'common',
			name: 'Fællesudbud',
			appendId: type.appendId
		};
	}

	var course = {
		id: doc._id,
		type: {
			id: type.id,
			name: type.name
		},
		courseStart: (doc.classesInfo && doc.classesInfo.duration && doc.classesInfo.duration.begin) ? doc.classesInfo.duration.begin : null,
		title: src.title,
		description: src.description,
		rating: (overwrites.rating) || null,
		duration: src.duration,
		admission: src.admission,
		jobareas: jobareas,
		theme: themes[0],
		apprenticeships: (overwrites.apprenticeships) || null,
		featured: !!overwrites.featured,
		certificate: (overwrites.certificates) || null
	};

	// Add resource links for authenticated users, or if specifically requested.
	if (isAuthenticated || options.links) {
		course.links = formatCourseLinks(overwrites.links);
	}

	course.classesCount = classes.length;

	if (!options.less) {
		course.classes = classes;
	}

	return course;
}

function formatCourseForUser(formattedCourse, user) {
	if (!user) return formattedCourse;
	var wishlist = user.wishlist || {};
	var wishlistTimestamp = wishlist[formattedCourse.id];
	formattedCourse.wishlist = wishlistTimestamp ? { added: wishlistTimestamp } : null;
	return formattedCourse;
}

function getCourse(auth, id, options, callback) {
	var authUserId = auth && auth.user && auth.user.id;
	var idMatch = ('' + id).match(/^(s?\d{1,8})([evs]?)$/);
	var error;

	if (!idMatch) {
		error = new Error('Ugyldig kursus-ID');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	options.region = '' + [options.region];
	options.school = '' + [options.school];
	options.guaranteed = options.guaranteed === 'true';
	options['signup-open'] = options['signup-open'] !== 'false';
	options['start-date'] = '' + [options['start-date']];
	options['end-date'] = '' + [options['end-date']];
	options.quarter = '' + [options.quarter];

	if (options['start-date'] && !/^\d{4}-\d{2}-\d{2}$/.test(options['start-date'])) {
		error = new Error('Ugyldig parameter: start-date. Skal være en gyldig dato i ÅÅÅÅ-MM-DD format.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (options['end-date'] && !/^\d{4}-\d{2}-\d{2}$/.test(options['end-date'])) {
		error = new Error('Ugyldig parameter: end-date. Skal være en gyldig dato i åååå-mm-dd format.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (options.quarter && !/^\d{4}Q[1-4]$/.test(options.quarter)) {
		error = new Error('Ugyldig parameter: quarter. Skal være formateret "ååååQn", hvor n er 1-4.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	var courseId = idMatch[1];
	var appendId = idMatch[2];

	common.step([

		function(next) {
			if (!authUserId) return next();
			db.users.findOne({ _id: db.decodeId(authUserId) }, { wishlist: 1 }, next);
		},

		function(user) {
			db.courses.findOne({ _id: courseId }, function(error, doc) {
				if (error) {
					error = new Error('Kunne ikke slå kursus op med det angivne ID.');
					error.code = 'server_error';
					error.status = 500;
					return callback(error);
				}

				if (doc) {
					var types = ((doc && doc.overwrites && doc.overwrites.types) || []).map(function(id) {
						return courseTypes[id];
					}).filter(function(t) { return t; });

					var classAlreadyPresented = {};

					doc.classes = (doc.classes || []).filter(function(cls) {
						if (!cls) return false;

						var registrationLink = cls.registration && cls.registration.link;
						if (!registrationLink) return false;

						if (classAlreadyPresented[registrationLink]) return false;
						classAlreadyPresented[registrationLink] = true;

						if (options.region) {
							var region =
								cls.school &&
								cls.school.geo &&
								cls.school.geo.properties &&
								cls.school.geo.properties.address &&
								cls.school.geo.properties.address.region;
							if (region !== options.region) return false;
						}

						if (options.school) {
							var schoolId =
								cls.school &&
								cls.school.id;
							if (schoolId !== options.school) return false;
						}

						if (options.guaranteed) {
							var guaranteed =
								cls.guaranteed;
							if (!guaranteed) return false;
						}

						if (options['signup-open']) {
							var remaining =
								cls.participants &&
								cls.participants.remaining;
							if (remaining === 0) return false;
						}

						// Only show courses that is still open
						if (options && !options['start-date']) {
							options['start-date'] = moment().format('YYYY-MM-DD');
						}

						if (options['start-date']) {
							var begin =
								cls.duration &&
								cls.duration.begin;
							if (begin < options['start-date']) return false;
						}

						if (options['end-date']) {
							var end =
								cls.duration &&
								cls.duration.end;
							if (end > options['end-date']) return false;
						}

						if (options.quarter) {
							var quarter =
								cls.duration &&
								cls.duration.quarter;
							if (quarter !== options.quarter) return false;
						}

						return true;
					});

					if (types.length <= 1) {
						if (appendId === '') {
							return callback(null, formatCourseForUser(formatCourse(!!auth, doc, types[0]), user));
						}
					} else {
						types = types.filter(function(t) { return t.appendId === appendId; });
						if (types.length === 1 || appendId === '') {
							return callback(null, formatCourseForUser(formatCourse(!!auth, doc, types[0]), user));
						}
					}
				}

				error = new Error('Der findes ikke et kursus med det angivne ID.');
				error.code = 'not_found';
				error.status = 404;
				callback(error);
			});
		}

	], function(error) {
		callback(error);
	});
}

exports.formatForUser = formatCourseForUser;
exports.formatLinks = formatCourseLinks;
exports.format = formatCourse;
exports.get = getCourse;
