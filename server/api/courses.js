/** @module server/api/courses */
'use strict';

var courseTypes = require('./courseTypes');
var through2 = require('through2');
var course = require('./course');
var common = require('common');
var LRU = require('lru-cache');
var db = require('../db');
var moment = require('moment');

var cache = LRU({
	max: 25 * 1024 * 1024, // 25 MiB
	length: function(doc) { return JSON.stringify(doc) + 16; },
	maxAge: 5 * 60 * 1000 // 5 minutes
});

function purgeCoursesQueryCache() {
	cache.reset();
}

var requests = {};

function formatDocsStream(queryType) {
	var hasQueryType = !!courseTypes[queryType];

	return through2.obj(function(doc, encoding, done) {
		if (hasQueryType) {
			this.push(course.format(false, doc, queryType, { less: true }));
		} else {
			var types = doc.overwrites && doc.overwrites.types;

			if (types && types.length > 1) {
				for (var i = 0; i < types.length; i++) {
					this.push(course.format(false, doc, types[i], { less: true }));
				}
			} else {
				this.push(course.format(false, doc, null, { less: true }));
			}
		}

		done();
	}, function(done) {
		done();
	});
}

function formatCoursesForUser(formattedCourses, user) {
	if (!user) return formattedCourses;

	return formattedCourses.map(function(formattedCourse) {
		return course.formatForUser(formattedCourse, user);
	});
}

function collectDocs(callback) {
	var docs = [];

	return through2.obj(function(doc, encoding, done) {
		if (doc) docs.push(doc);
		done();
	}, function(done) {
		callback(null, docs);
		done();
	}).on('error', function(error) {
		callback(error);
	});
}

function performQuery(options, callback) {
	var query = {};
	var queryIds = {};
	var queryText = [];
	
	if (options.q) {
		// Support searching for specific course IDs.
		options.q.split(/\s+/).forEach(function(word) {
			if (/^(\d{3,5}[evs]?|s\d{6})$/.test(word)) {
				word = word.replace(/[evs]$/, '');
				queryIds[word] = true;
			} else {
				queryText.push(word);
			}
		});
		queryIds = Object.keys(queryIds);
		if (queryIds.length === 1) {
			query._id = queryIds[0];
		} else if (queryIds.length > 1) {
			query._id = { '$in': queryIds };
		}



		// Support free text search.
		if (queryText.length > 0) {
			query.$text = { $search: queryText.join(' ') };
		}
	}

	if (options.type) {
		// private arrangements 
		if (courseTypes[options.type] === courseTypes.private_arrangements) {
			query['overwrites.custom'] = true;
		} else {
			query['overwrites.types'] = options.type;
		}
	} else {
		query['overwrites.types'] = { $exists: true };
	}
	if (options.featured) {
		query['overwrites.featured'] = true;
	}
	if (options.region) {
		query['classes.school.geo.properties.address.region'] = options.region;
	}
	if (options.school) {
		//query['classes.school.nameId'] = options.school;
		query['classes.school.id'] = options.school;
	}
	if (options.theme) {
		query['overwrites.themes.' + (options.type || 'all') + '.main.id'] = options.theme;
	}
	if (options.sub_theme) {
		query['overwrites.themes.' + (options.type || 'all') + '.sub.id'] = options.sub_theme;
	}
	if (options.jobarea) {
		query['overwrites.jobareas.' + (options.type || 'all') + '.main.id'] = options.jobarea;
	}
	if (options.sub_jobarea) {
		query['overwrites.jobareas.' + (options.type || 'all') + '.sub.id'] = options.sub_jobarea;
	}
	if (options.starred) {
		query['overwrites.starred'] = true;
	}
	if (options.guaranteed) {
		query['classesInfo.guaranteed'] = true;
	}
	if (options['signup-open'] && queryIds.length !== 1) {
		query['classesInfo.participants.remaining'] = {$not:{$eq:0}};
		query.classesInfo = { $exists: true };
	}

	if (options['start-date'] && options['end-date']) {
		query['classes'] = {
		    $elemMatch: {
		        $and: [{
		            'duration.begin': {
		                $gte: options['start-date']
		            }
		        }, {
		            'duration.end': {
		                $lte: options['end-date']
		            }
		        }]
		    }
		};
	} else if (options['start-date']) {
		query['classes'] = {
		    $elemMatch: {
		        'duration.begin': {
		            $gte: options['start-date']
		        }
		    }
		};
	} else if (options['end-date']) {
		query['classes'] = {
		    $elemMatch: {
		        'duration.end': {
		            $lte: options['end-date']
		        }
		    }
		};
	}

	if (options.quarter) {
		query['classesInfo.duration.quarters'] = options.quarter;
	}
	if (!options.common) {
		query['src.isCommon'] = false;
	}

	var sortOptions;
	if (options.sort === 'date') {
		sortOptions = { 'classesInfo.duration.begin': 1 };
	} else {
		sortOptions = { 'src.isCommon': 1, 'src.title': 1 };
	}

	query['src.id'] = { $exists: true };

	// query.classesInfo = { $exists: true };

	// console.log(
	// 	'db.courses.find(%s).sort(%s) - type: %j',
	// 	require('util').inspect(query, { colors: true, depth: 5}),
	// 	require('util').inspect(sortOptions, { colors: true, depth: 5}),
	// 	options.type);
	db.courses.find(query).sort(sortOptions).pipe(formatDocsStream(options.type)).pipe(collectDocs(callback));
}

function getList(options, callback) {
	var key = JSON.stringify([options.type, options.q, options.featured, options.region, options.school, options.theme, options.sub_theme, options.jobarea, options.sub_jobarea, options.starred, options.guaranteed, options['signup-open'], options['start-date'], options['end-date'], options.quarter, options.common, options.sort]);
	
	// If we want only to show courses that has start-date >= today 
	/*if (options && !options['start-date']) {
		options['start-date'] = moment().format('YYYY-MM-DD');
	}*/

	var list = cache.get(key);
	if (list) {
		return callback(null, list);
	}

	var request = requests[key];
	if (!request) {
		request = requests[key] = common.future();
		request.get(function(error, docs) {
			delete requests[key];

			if (!error && docs) {
				cache.set(key, docs);
			}
		});
		performQuery(options, request.put);
	}
	request.get(callback);
}

function get(auth, options, callback) {
	var authUserId = auth && auth.user && auth.user.id;
	var error;

	options.offset = Math.max(0, options.offset | 0);
	options.limit = Math.min(Math.max(1, (options.limit | 0) || 50), 10000);

	options.type = '' + [options.type];
	options.q = ('' + [options.q]).trim(); // TODO: Unicode normalization
	options.featured = options.featured === 'true';
	options.region = '' + [options.region];
	options.school = '' + [options.school];
	options.theme = '' + [options.theme];
	options.sub_theme = '' + [options.sub_theme];
	options.jobarea = '' + [options.jobarea];
	options.sub_jobarea = '' + [options.sub_jobarea];
	options.starred = options.starred === 'true';
	options.guaranteed = options.guaranteed === 'true';
	options['signup-open'] = options['signup-open'] === 'true';
	options['start-date'] = '' + [options['start-date']];
	options['end-date'] = '' + [options['end-date']];
	options.quarter = '' + [options.quarter];
	options.common = options.common !== 'false';
	options.sort = '' + [options.sort];

	if (options.sort === '') options.sort = 'name';

	if (!(options.sort === 'date' || options.sort === 'name')) {
		error = new Error('Ugyldig parameter: sort. Skal være en af værdierne: date, name');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (options['start-date'] && !/^\d{4}-\d{2}-\d{2}$/.test(options['start-date'])) {
		error = new Error('Ugyldig parameter: start-date. Skal være en gyldig dato i ÅÅÅÅ-MM-DD format.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (options['end-date'] && !/^\d{4}-\d{2}-\d{2}$/.test(options['end-date'])) {
		error = new Error('Ugyldig parameter: end-date. Skal være en gyldig dato i åååå-mm-dd format.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (options.quarter && !/^\d{4}Q[1-4]$/.test(options.quarter)) {
		error = new Error('Ugyldig parameter: quarter. Skal være formateret "ååååQn", hvor n er 1-4.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	var user;

	common.step([

		function(next) {
			if (!authUserId) return next();
			db.users.findOne({ _id: db.decodeId(authUserId) }, { wishlist: 1 }, next);
		},

		function(_user, next) {
			user = _user;
			getList(options, next);
		},

		function(docs, next) {
			moveOldClassesToEndOfArray(docs, options, next);
		},
		function(docs) {
			callback(null, {
				pagination: {
					offset: options.offset,
					limit: options.limit
				},
				count: docs.length,
				items: formatCoursesForUser(docs.slice(options.offset, options.offset + options.limit), user)
			});
		}
		], function(error) {
			callback(error);
		});
}

// Moves courses with expired classes to end of array of courses
function moveOldClassesToEndOfArray(courses, options, callback) {
	if (!courses || (options && options['start-date'])) {
		callback(null, courses);
		return;
	}

	var now = moment();
	var coursesWithActiveClasses = [];
	var coursesWithExpiredClasses = [];
	var coursesWithNoClasses = [];

	courses.forEach(function(course) {
		if (course.courseStart && now.diff(course.courseStart) > 0) {
			coursesWithExpiredClasses.push(course);
		} else if (!course.courseStart) {
			coursesWithNoClasses.push(course);
		} else {
			coursesWithActiveClasses.push(course);
		}
	});

	var mergedCourses = coursesWithActiveClasses.concat(coursesWithExpiredClasses).concat(coursesWithNoClasses);
	callback(null, mergedCourses);
}

exports.purgeQueryCache = purgeCoursesQueryCache;
exports.formatForUser = formatCoursesForUser;
exports.get = get;
