/** @module server/api/courseRating */
'use strict';

var courses = require('./courses');
var db = require('../db');

function updateGlobalCourseRating(error, doc, callback) {
	if (error) {
		error = new Error('Kunne ikke opdatere rating på det angivne kursus.');
		error.code = 'server_error';
		error.status = 500;
		return callback(error);
	}

	if (!doc) {
		error = new Error('Kunne ikke finde kursus med det angivne ID.');
		error.code = 'not_found';
		error.status = 404;
		return callback(error);
	}

	var ratings = doc.ratings;
	var ratingSum = 0;
	var ratingCount = 0;
	if (ratings) {
		for (var key in ratings) {
			var rating = ratings[key];
			if (rating && rating.stars) {
				ratingSum += rating.stars | 0;
				ratingCount++;
			}
		}
	}

	var globalRating = null;
	if (ratingCount >= 1) {
		var ratingAverage = ratingSum / ratingCount;
		globalRating = {
			stars: Math.round(ratingAverage),
			value: ratingAverage
		};
	}

	var query = {
		query: { _id: doc._id },
		update: {
			$set: {
				'overwrites.rating': globalRating
			}
		}
	};

	db.courses.findAndModify(query, function(error, doc) {
		if (error || !doc) {
			error = new Error('Der opstod en fejl med at opdatere rating på det angivne kursus.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		courses.purgeQueryCache();

		callback(null, {
			rating: globalRating
		});
	});
}

function updateRating(fullCourseId, userId, options, callback) {
	var stars = '' + [options.rating];
	if (!stars) {
		error = new Error('Mangler parameteren rating.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}
	if (!/^[1-5]$/.test(stars)) {
		error = new Error('Ugyldig rating, skal være 1-5.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}
	stars = stars | 0;

	var idMatch = ('' + fullCourseId).match(/^(s?\d{1,8})([evs]?)$/);
	var error;

	if (!idMatch) {
		error = new Error('Ugyldig kursus-ID');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	var courseId = idMatch[1];

	userId = db.decodeId(userId);
	if (!userId) {
		error = new Error('Ugyldig bruger-ID');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	var updateSet = {};
	updateSet['ratings.' + userId + '.stars'] = stars;

	var query = {
		query: { _id: courseId },
		update: {
			$set: updateSet
		},
		new: true
	};

	db.courses.findAndModify(query, function(error, doc) {
		updateGlobalCourseRating(error, doc, callback);
	});
}

function removeRating(fullCourseId, userId, callback) {
	var idMatch = ('' + fullCourseId).match(/^(s?\d{1,8})([evs]?)$/);
	var error;

	if (!idMatch) {
		error = new Error('Ugyldig kursus-ID');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	var courseId = idMatch[1];

	userId = db.decodeId(userId);
	if (!userId) {
		error = new Error('Ugyldig bruger-ID');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	var updateUnset = {};
	updateUnset['ratings.' + userId] = '';

	var query = {
		query: { _id: courseId },
		update: {
			$unset: updateUnset
		},
		new: true
	};

	db.courses.findAndModify(query, function(error, doc) {
		updateGlobalCourseRating(error, doc, function(error) {
			if (error) return callback(error);

			callback(null, { status: 'ok' });
		});
	});
}

function getRating(fullCourseId, userId, callback) {
	var idMatch = ('' + fullCourseId).match(/^(s?\d{1,8})([evs]?)$/);
	var error;

	if (!idMatch) {
		error = new Error('Ugyldig kursus-ID');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	var courseId = idMatch[1];

	userId = db.decodeId(userId);
	if (!userId) {
		error = new Error('Ugyldig bruger-ID');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	db.courses.findOne({ _id: courseId }, function(error, doc) {
		if (error) {
			error = new Error('Kunne ikke slå rating op for det angivne kursus.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		if (!doc) {
			error = new Error('Kunne ikke finde kursus med det angivne ID.');
			error.code = 'not_found';
			error.status = 404;
			return callback(error);
		}

		var ratings = doc.ratings;
		var userRating = null;

		if (ratings && ratings[userId] && ratings[userId].stars) {
			userRating = ratings[userId].stars | 0;
		}

		userRating = userRating ? { stars: userRating, value: userRating } : null;
		if (userRating) {
			callback(null, { rating: userRating });
		} else {
			error = new Error('Du har ikke givet dette kursus en rating.');
			error.code = 'not_found';
			error.status = 404;
			callback(error);
		}
	});
}

exports.update = updateRating;
exports.remove = removeRating;
exports.get = getRating;
