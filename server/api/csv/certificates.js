/** @module server/api/csv/certificates */
'use strict';

var csvParse = require('csv-parse');
var through2 = require('through2');
var courses = require('../courses');
var csvUtil = require('./util');
var common = require('common');
var db = require('../../db');

var columnMatch = {
	'kursusnr': 'kursusnummer',
	'kursus': 'kursusnummer',
	'varighedmonths': 'months',
	'notificationdays': 'notifikation',
	'newcourse': 'nytkursus'
};

function listErrors(errors) {
	return csvUtil.listErrors(errors, 'kursusnummer', 'kursusnumre');
}

function uploadCertificatesCSV(request, callback) {
	callback = common.once(callback);
	var running = true;
	var updated = 0;
	var notFound = [];
	var errors = [];

	var purgeOldCertificates = common.future();
	db.courses.update(
		{ 'overwrites.certificates': { $exists: true } },
		{ $unset: { 'overwrites.certificates': '' } },
		{ multi: true },
		function(error) {
			if (error) {
				return purgeOldCertificates.put(error);
			}

			setTimeout(function() {
				purgeOldCertificates.put(null);
			}, 500);
		}
	);

	function errorHalt(message) {
		running = false;
		var error = new Error(message);
		error.code = 'invalid_request';
		error.status = 400;
		callback(error);
	}

	request.pipe(csvParse({
		delimiter: ';',
		columns: function(row) {
			return row.map(function(item) {
				item = item.toLowerCase().replace(/[^a-z]+/g, '');
				return columnMatch[item] || item;
			});
		}
	})).on('error', function(error) {
		var message = 'Ugyldig CSV-fil.';
		if (error && error.message) {
			message += ' Engelsk fejlmeddelse: ' + error.message;
		}
		errorHalt(message);
	}).pipe(through2.obj(function(obj, _, done) {
		if (!running) return done();

		if ('kursusnummer' in obj && 'months' in obj && 'notifikation' in obj && 'nytkursus' in obj) {
			var courseId = '' + obj.kursusnummer;
			var months = obj.months | 0;
			var notification = obj.notifikation | 0;
			var newCourseId = '' + obj.nytkursus;

			var query = {
				query: {
					_id: courseId
				},
				update: {
					$set: {
						'overwrites.certificates': {
							duration: {
								months: months
							},
							notification: {
								days: notification
							},
							newCourse: {
								id: newCourseId
							}
						}
					}
				},
				new: true
			};

			purgeOldCertificates.get(function(error) {
				if (!running) return done();

				if (error) {
					console.error('Could not purge old certificates courses:', error);
					errorHalt('Der opstod en database-fejl. Prøv igen om lidt.', true);
					return done();
				}

				db.courses.findAndModify(query, function(error, doc) {
					if (error) {
						console.error('Could not update certificates property of course:', error);
						errors.push(courseId);
					} else if (!doc) {
						notFound.push(courseId);
					} else {
						updated++;
					}
					done();
				});
			});
		} else {
			errorHalt('Mangler en eller flere kolonner: Kursusnummer, Varighed_months, Notification_days, Nyt_kursus.');
			done();
		}
	}, function(done) {
		courses.purgeQueryCache();

		if (updated === 0) {
			var errorMessage = 'Ingen rækker blev opdateret. Tjek at du har de rette kolonnenavne (Kursusnummer, Varighed_months, Notification_days, Nyt_kursus) og at alle rækker er gyldige.';
			var errorMessageDetails = [];
			if (errors.length > 0) {
				errorMessageDetails.push(errors.length + ' række' + (errors.length === 1 ? '' : 'r') + ' med databasefejl' + listErrors(errors));
			}
			if (notFound.length > 0) {
				errorMessageDetails.push(notFound.length + ' række' + (notFound.length === 1 ? '' : 'r') + ' hvor kursusnummer ikke findes' + listErrors(notFound));
			}
			if (errorMessageDetails.length > 0) {
				errorMessage += ' Der var ' + errorMessageDetails.join(' og ') + '.';
			}
			var error = new Error(errorMessage);
			error.code = 'invalid_request';
			error.status = 400;
			return callback(error);
		}

		var message;
		var warnings = false;
		if (errors.length > 0) {
			warnings = true;
			message = 'Der var ' + errors.length + ' ' + (errors.length === 1 ? 'kursus' : 'kurser') + ', som ikke blev opdateret pga. databasefejl' + listErrors(errors) + '. Prøv venligst at uploade filen igen.';
			if (notFound.length > 0) {
				message += ' Der var ' + notFound.length + ' ' + (notFound.length === 1 ? 'kursus' : 'kurser') + ', hvor kursusnummeret ikke eksisterer' + listErrors(notFound) + '.';
			}
			message += ' Der blev med success opdateret ' + updated + ' ' + (updated === 1 ? 'kursus' : 'kurser') + '.';
		} else if (notFound.length > 0) {
			warnings = true;
			message = 'Der var ' + notFound.length + ' ' + (notFound.length === 1 ? 'kursus' : 'kurser') + ', hvor kursusnummeret ikke eksisterer' + listErrors(notFound) + '.';
			if (updated === 1) {
				message += ' Ét kursus blev med success opdateret!';
			} else {
				message += ' ' + updated + ' kurser blev med success opdateret!';
			}
		} else {
			if (updated === 1) {
				message = 'Ét kursus blev opdateret!';
			} else {
				message = updated + ' kurser blev opdateret!';
			}
		}

		callback(null, { message: message, warnings: warnings, updated: updated, errors: errors.length, notFound: notFound.length, total: updated + notFound.length + errors.length });
		done();
	}));
}

exports.upload = uploadCertificatesCSV;
