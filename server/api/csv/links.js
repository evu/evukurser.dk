/** @module server/api/csv/links */
'use strict';

var csvParse = require('csv-parse');
var through2 = require('through2');
var csvUtil = require('./util');
var common = require('common');
var async = require('async');
var db = require('../../db');

var validUrlRegexp = /^https?:\/\//i;
var validDateRegexp = /^20\d{6}$/;

var columnMatch = {
	'kursusnr': 'kursusnummer',
	'kursus': 'kursusnummer',
	'link': 'url',
	'startdate': 'start',
	'startdato': 'start',
	'slutdate': 'slut',
	'slutdato': 'slut',
	'enddate': 'slut',
	'enddato': 'slut',
	'end': 'slut'
};

function parseDate(date) {
	date = ('' + [date]).replace(/[\s\-_]+/g, '');

	if (validDateRegexp.test(date)) {
		return date.substr(0,4) + '-' + date.substr(4,2) + '-' + date.substr(6,2);
	} else {
		return null;
	}
}

function listErrors(errors) {
	return csvUtil.listErrors(errors, 'kursusnummer', 'kursusnumre');
}

function uploadLinksCSV(request, callback) {
	callback = common.once(callback);
	var running = true;
	var updated = 0;
	var notFound = 0;
	var errors = 0;
	var notFoundCourses = {};
	var errorCourses = {};

	var courses = {};

	function errorHalt(message) {
		running = false;
		var error = new Error(message);
		error.code = 'invalid_request';
		error.status = 400;
		callback(error);
	}

	request.pipe(csvParse({
		delimiter: ';',
		columns: function(row) {
			return row.map(function(item) {
				item = item.toLowerCase().replace(/[^a-z]+/g, '');
				return columnMatch[item] || item;
			});
		}
	})).on('error', function(error) {
		var message = 'Ugyldig CSV-fil.';
		if (error && error.message) {
			message += ' Engelsk fejlmeddelse: ' + error.message;
		}
		errorHalt(message);
	}).pipe(through2.obj(function(obj, _, done) {
		if (!running) return done();

		if ('kursusnummer' in obj && 'url' in obj && 'start' in obj && 'slut' in obj) {
			var courseId = ('' + [obj.kursusnummer]).trim();
			var url = ('' + [obj.url]).trim();
			var begin = parseDate(obj.start);
			var end = parseDate(obj.slut);

			if (!validUrlRegexp.test(url)) {
				url = 'http://' + url;
			}

			if (courseId) {
				courses[courseId] = courses[courseId] || [];
				courses[courseId].push({
					url: url,
					begin: begin,
					end: end
				});
			} else {
				errors++;
				errorCourses[courseId] = true;
			}
			done();
		} else {
			errorHalt('Mangler en eller flere kolonner: Kursusnummer, URL, Start_dato, Slut_dato.');
			done();
		}
	}, function(done) {
		if (!running) return done();

		async.eachLimit(Object.keys(courses), 5, function(courseId, eachDone) {
			var links = courses[courseId];

			var query = {
				query: {
					_id: courseId
				},
				update: {
					$set: {
						'overwrites.links': links
					}
				}
			};

			db.courses.findAndModify(query, function(error, doc) {
				if (error) {
					errors += links.length;
					errorCourses[courseId] = true;
				} else if (!doc) {
					notFound += links.length;
					notFoundCourses[courseId] = true;
				} else {
					updated += links.length;
				}
				eachDone();
			});
		}, function() {
			errorCourses = Object.keys(errorCourses);
			notFoundCourses = Object.keys(notFoundCourses);

			if (updated === 0) {
				var errorMessage = 'Ingen rækker blev opdateret. Tjek at du har de rette kolonnenavne (Kursusnummer, URL, Start_dato, Slut_dato) og at alle rækker er gyldige.';
				var errorMessageDetails = [];
				if (errors > 0) {
					errorMessageDetails.push(errors + ' række' + (errors === 1 ? '' : 'r') + ' med databasefejl' + listErrors(errorCourses));
				}
				if (notFound > 0) {
					errorMessageDetails.push(notFound + ' række' + (notFound === 1 ? '' : 'r') + ' hvor kursusnummer ikke findes' + listErrors(notFoundCourses));
				}
				if (errorMessageDetails.length > 0) {
					errorMessage += ' Der var ' + errorMessageDetails.join(' og ') + '.';
				}
				var error = new Error(errorMessage);
				error.code = 'invalid_request';
				error.status = 400;
				return callback(error);
			}

			var message;
			var warnings = false;
			if (errors > 0) {
				warnings = true;
				message = 'Der var ' + errors + ' ' + (errors === 1 ? 'link' : 'links') + ', som ikke blev opdateret pga. databasefejl' + listErrors(errorCourses) + '. Prøv venligst at uploade filen igen.';
				if (notFound > 0) {
					message += ' Der var ' + notFound + ' ' + (notFound === 1 ? 'link' : 'links') + ', hvor kursusnummeret ikke eksisterer' + listErrors(notFoundCourses) + '.';
				}
				message += ' Der blev med success opdateret ' + updated + ' ' + (updated === 1 ? 'link' : 'links') + '.';
			} else if (notFound > 0) {
				warnings = true;
				message = 'Der var ' + notFound + ' ' + (notFound === 1 ? 'link' : 'links') + ', hvor kursusnummeret ikke eksisterer' + listErrors(notFoundCourses) + '.';
				if (updated === 1) {
					message += ' Ét link blev med success opdateret!';
				} else {
					message += ' ' + updated + ' links blev med success opdateret!';
				}
			} else {
				if (updated === 1) {
					message = 'Ét link blev opdateret!';
				} else {
					message = updated + ' links blev opdateret!';
				}
			}

			callback(null, { message: message, warnings: warnings, updated: updated, errors: errors, notFound: notFound, total: updated + notFound + errors });
			done();
		});
	}));
}

exports.upload = uploadLinksCSV;
