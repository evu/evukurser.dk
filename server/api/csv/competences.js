/** @module server/api/csv/competences */
'use strict';

var csvParse = require('csv-parse');
var through2 = require('through2');
var courses = require('../courses');
var csvUtil = require('./util');
var common = require('common');
var db = require('../../db');

var columnMatch = {
	'type': 'fag'
};

function listErrors(errors) {
	return csvUtil.listErrors(errors, 'FKB', 'FKB\'er');
}

function normalizeText(text) {
	return ('' + [text]).replace(/\s+/g, ' ').trim();
}

function uploadCompetencesCSV(request, callback) {
	callback = common.once(callback);
	var running = true;
	var updated = 0;
	var notFound = [];
	var errors = [];

	var purgeOldCompetences = common.future();
	db.competences.update(
		{},
		{ $unset: { 'overwrites.type': '' } },
		{ multi: true },
		function(error) {
			if (error) {
				return purgeOldCompetences.put(error);
			}

			setTimeout(function() {
				purgeOldCompetences.put(null);
			}, 500);
		}
	);

	function errorHalt(message) {
		running = false;
		var error = new Error(message);
		error.code = 'invalid_request';
		error.status = 400;
		callback(error);
	}

	request.pipe(csvParse({
		delimiter: ';',
		columns: function(row) {
			return row.map(function(item) {
				item = item.toLowerCase().replace(/[^a-z]+/g, '');
				return columnMatch[item] || item;
			});
		}
	})).on('error', function(error) {
		var message = 'Ugyldig CSV-fil.';
		if (error && error.message) {
			message += ' Engelsk fejlmeddelse: ' + error.message;
		}
		errorHalt(message);
	}).pipe(through2.obj(function(obj, _, done) {
		if (!running) return done();

		if ('fkb' in obj && 'fag' in obj) {
			var competenceId = normalizeText(obj.fkb);
			var typeInCSV = normalizeText(obj.fag).toLowerCase();

			var type = '';
			if (/skorsten/i.test(typeInCSV)) {
				type = 'chimney';
			} else if (/vvs/i.test(typeInCSV)) {
				type = 'plumbing';
			} else if (/el/i.test(typeInCSV)) {
				type = 'electrical';
			}

			if (!type) {
				errors.push(competenceId);
				return done();
			}

			var query = {
				query: {
					_id: competenceId
				},
				update: {
					$addToSet: {
						'overwrites.type': type
					}
				},
				upsert: true,
				new: true
			};

			purgeOldCompetences.get(function(error) {
				if (!running) return done();

				if (error) {
					console.error('Could not purge old competences:', error);
					errorHalt('Der opstod en database-fejl. Prøv igen om lidt.', true);
					return done();
				}

				db.competences.findAndModify(query, function(error, doc) {
					if (error) {
						console.error('Could not update type property of competences:', error);
						errors.push(competenceId);
					} else if (!doc) {
						notFound.push(competenceId);
					} else {
						updated++;
					}
					done();
				});
			});
		} else {
			errorHalt('Mangler kolonne FKB og/eller Fag.');
			done();
		}
	}, function(done) {
		db.updateCourseTypes(function(error) {
			courses.purgeQueryCache();

			if (error) {
				console.log('Could not update course types after competence.csv upload:', error.stack || error);
				errorHalt('Kunne ikke opdatere kursustyper i databasen. Prøv venligst igen.');
				return done();
			}

			if (updated === 0) {
				var errorMessage = 'Ingen rækker blev opdateret. Tjek at du har de rette kolonnenavne (FKB, Fag) og at alle rækker er gyldige.';
				var errorMessageDetails = [];
				if (errors.length > 0) {
					errorMessageDetails.push(errors.length + ' række' + (errors.length === 1 ? '' : 'r') + ' med databasefejl' + listErrors(errors));
				}
				if (notFound.length > 0) {
					errorMessageDetails.push(notFound.length + ' række' + (notFound.length === 1 ? '' : 'r') + ' hvor FKB ID ikke findes' + listErrors(notFound));
				}
				if (errorMessageDetails.length > 0) {
					errorMessage += ' Der var ' + errorMessageDetails.join(' og ') + '.';
				}
				error = new Error(errorMessage);
				error.code = 'invalid_request';
				error.status = 400;
				return callback(error);
			}

			var message;
			var warnings = false;
			if (errors.length > 0) {
				warnings = true;
				message = 'Der var ' + errors.length + ' ' + (errors.length === 1 ? 'FKB' : 'FKB\'er') + ', som ikke blev opdateret pga. databasefejl' + listErrors(errors) + '. Prøv venligst at uploade filen igen.';
				if (notFound.length > 0) {
					message += ' Der var ' + notFound.length + ' ' + (notFound.length === 1 ? 'FKB' : 'FKB\'er') + ', hvor FKB ID ikke eksisterer' + listErrors(notFound) + '.';
				}
				message += ' Der blev med success opdateret ' + updated + ' ' + (updated === 1 ? 'FKB' : 'FKB\'er') + '.';
			} else if (notFound.length > 0) {
				warnings = true;
				message = 'Der var ' + notFound.length + ' ' + (notFound.length === 1 ? 'FKB' : 'FKB\'er') + ', hvor FKB ID ikke eksisterer' + listErrors(notFound) + '.';
				if (updated === 1) {
					message += ' Én FKB blev med success opdateret!';
				} else {
					message += ' ' + updated + ' FKB\'er blev med success opdateret!';
				}
			} else {
				if (updated === 1) {
					message = 'Én FKB blev opdateret!';
				} else {
					message = updated + ' FKB\'er blev opdateret!';
				}
			}

			callback(null, { message: message, warnings: warnings, updated: updated, errors: errors.length, notFound: notFound.length, total: updated + notFound.length + errors.length });
			done();
		});
	}));
}

exports.upload = uploadCompetencesCSV;
