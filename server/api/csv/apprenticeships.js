/** @module server/api/csv/apprenticeships */
'use strict';

var csvParse = require('csv-parse');
var through2 = require('through2');
var courses = require('../courses');
var csvUtil = require('./util');
var common = require('common');
var db = require('../../db');

var columnMatch = {
	'kursusnr': 'kursusnummer',
	'kursus': 'kursusnummer',
	'udlrtspeciale': 'udlaert',
	'udlartspeciale': 'udlaert',
	'udlaertspeciale': 'udlaert',
	'nsketspeciale': 'oensket',
	'onsketspeciale': 'oensket',
	'oensketspeciale': 'oensket'
};

function normalizeText(text) {
	return ('' + [text]).replace(/\s+/g, ' ').trim();
}

function listErrors(errors) {
	return csvUtil.listErrors(errors, 'kursusnummer', 'kursusnumre');
}

function uploadApprenticeshipsCSV(request, callback) {
	callback = common.once(callback);
	var running = true;
	var updated = 0;
	var notFound = [];
	var errors = [];

	function errorHalt(message, isServerError) {
		running = false;
		var error = new Error(message);
		error.code = isServerError ? 'server_error' : 'invalid_request';
		error.status = isServerError ? 500 : 400;
		callback(error);
	}

	var purgeOldApprenticeships = common.future();
	db.courses.update(
		{ 'overwrites.apprenticeships': { $exists: true } },
		{ $unset: { 'overwrites.apprenticeships': '' } },
		{ multi: true },
		function(error) {
			if (error) {
				return purgeOldApprenticeships.put(error);
			}

			setTimeout(function() {
				purgeOldApprenticeships.put(null);
			}, 500);
		}
	);

	request.pipe(csvParse({
		delimiter: ';',
		columns: function(row) {
			return row.map(function(item) {
				item = item.toLowerCase().replace(/[^a-z]+/g, '');
				return columnMatch[item] || item;
			});
		}
	})).on('error', function(error) {
		var message = 'Ugyldig CSV-fil.';
		if (error && error.message) {
			message += ' Engelsk fejlmeddelse: ' + error.message;
		}
		errorHalt(message);
	}).pipe(through2.obj(function(obj, _, done) {
		if (!running) return done();

		if ('kursusnummer' in obj && 'udlaert' in obj && 'oensket' in obj) {
			var courseId = normalizeText(obj.kursusnummer);
			var current = normalizeText(obj.udlaert);
			var wanted = normalizeText(obj.oensket);

			var querySet = {};
			querySet['overwrites.apprenticeships.' + current.replace(/[.$]+/g, '')] = wanted;

			var query = {
				query: {
					_id: courseId
				},
				update: {
					$addToSet: querySet
				},
				new: true
			};

			purgeOldApprenticeships.get(function(error) {
				if (!running) return done();

				if (error) {
					console.error('Could not purge old apprenticeships:', error);
					errorHalt('Der opstod en database-fejl. Prøv igen om lidt.', true);
					return done();
				}

				db.courses.findAndModify(query, function(error, doc) {
					if (error) {
						console.error('Could not update apprenticeships property of course:', error);
						errors.push(courseId);
					} else if (!doc) {
						notFound.push(courseId);
					} else {
						updated++;
					}
					done();
				});
			});
		} else {
			errorHalt('Mangler en eller flere kolonner: Kursusnummer, Udlært_speciale, Ønsket_speciale.');
			done();
		}
	}, function(done) {
		courses.purgeQueryCache();

		if (updated === 0) {
			var errorMessage = 'Ingen rækker blev opdateret. Tjek at du har de rette kolonnenavne (Kursusnummer, Udlært_speciale, Ønsket_speciale) og at alle rækker er gyldige.';
			var errorMessageDetails = [];
			if (errors.length > 0) {
				errorMessageDetails.push(errors.length + ' række' + (errors.length === 1 ? '' : 'r') + ' med databasefejl' + listErrors(errors));
			}
			if (notFound.length > 0) {
				errorMessageDetails.push(notFound.length + ' række' + (notFound.length === 1 ? '' : 'r') + ' hvor kursusnummer ikke findes' + listErrors(notFound));
			}
			if (errorMessageDetails.length > 0) {
				errorMessage += ' Der var ' + errorMessageDetails.join(' og ') + '.';
			}
			var error = new Error(errorMessage);
			error.code = 'invalid_request';
			error.status = 400;
			return callback(error);
		}

		var message;
		var warnings = false;
		if (errors.length > 0) {
			warnings = true;
			message = 'Der var ' + errors.length + ' ' + (errors.length === 1 ? 'kursus' : 'kurser') + ', som ikke blev opdateret pga. databasefejl' + listErrors(errors) + '. Prøv venligst at uploade filen igen.';
			if (notFound.length > 0) {
				message += ' Der var ' + notFound.length + ' ' + (notFound.length === 1 ? 'kursus' : 'kurser') + ', hvor kursusnummeret ikke eksisterer' + listErrors(notFound) + '.';
			}
			message += ' Der blev med success opdateret ' + updated + ' ' + (updated === 1 ? 'kursus' : 'kurser') + '.';
		} else if (notFound.length > 0) {
			warnings = true;
			message = 'Der var ' + notFound.length + ' ' + (notFound.length === 1 ? 'kursus' : 'kurser') + ', hvor kursusnummeret ikke eksisterer' + listErrors(notFound) + '.';
			if (updated === 1) {
				message += ' Ét kursus blev med success opdateret!';
			} else {
				message += ' ' + updated + ' kurser blev med success opdateret!';
			}
		} else {
			if (updated === 1) {
				message = 'Ét kursus blev opdateret!';
			} else {
				message = updated + ' kurser blev opdateret!';
			}
		}

		callback(null, { message: message, warnings: warnings, updated: updated, errors: errors.length, notFound: notFound.length, total: updated + notFound.length + errors.length });
		done();
	}));
}

exports.upload = uploadApprenticeshipsCSV;
