/** @module server/api/csv/themes */
'use strict';

var csvParse = require('csv-parse');
var through2 = require('through2');
var courses = require('../courses');
var csvUtil = require('./util');
var common = require('common');
var db = require('../../db');

var columnMatch = {
	'kursusnr': 'kursusnummer',
	'kursus': 'kursusnummer',
	'hoved': 'hovedtema',
	'main': 'hovedtema',
	'under': 'undertema',
	'sub': 'undertema'
};

function listErrors(errors) {
	return csvUtil.listErrors(errors, 'kursusnummer', 'kursusnumre');
}

function createId(name) {
	return name
		.toLocaleLowerCase()
		.replace(/é/g, 'e')
		.replace(/æ/g, 'ae')
		.replace(/ø/g, 'oe')
		.replace(/å/g, 'aa')
		.replace(/ä/g, 'ae')
		.replace(/ö/g, 'oe')
		.replace(/ü/g, 'ue')
		.replace(/[^a-z0-9]+/g, '-')
		.replace(/^-|-$/g, '');
}

function normalizeText(text) {
	return ('' + [text]).replace(/\s+/g, ' ').trim();
}

function uploadThemesCSV(request, callback) {
	callback = common.once(callback);
	var running = true;
	var updated = 0;
	var notFound = [];
	var errors = [];

	var purgeOldThemes = common.future();
	db.courses.update(
		{},
		{
			$set: {
				'overwrites.themes': {
					'electrical': [],
					'plumbing': [],
					'chimney': [],
					'all': []
				}
			}
		},
		{ multi: true },
		function(error) {
			if (error) {
				return purgeOldThemes.put(error);
			}

			setTimeout(function() {
				purgeOldThemes.put(null);
			}, 500);
		}
	);

	function errorHalt(message) {
		running = false;
		var error = new Error(message);
		error.code = 'invalid_request';
		error.status = 400;
		callback(error);
	}

	request.pipe(csvParse({
		delimiter: ';',
		columns: function(row) {
			return row.map(function(item) {
				item = item.toLowerCase().replace(/[^a-z]+/g, '');
				return columnMatch[item] || item;
			});
		}
	})).on('error', function(error) {
		var message = 'Ugyldig CSV-fil.';
		if (error && error.message) {
			message += ' Engelsk fejlmeddelse: ' + error.message;
		}
		errorHalt(message);
	}).pipe(through2.obj(function(obj, _, done) {
		if (!running) return done();

		if ('kursusnummer' in obj && 'hovedtema' in obj && 'undertema' in obj) {
			var courseId = '' + obj.kursusnummer;
			var mainText = normalizeText(obj.hovedtema);
			var subText = normalizeText(obj.undertema);

			var types = ['electrical', 'plumbing', 'chimney', 'all'];

			var mainId = createId(mainText);
			var subId = createId(subText);

			var text = [];
			var theme = {
				main: null,
				sub: null,
				text: null
			};
			if (mainText) {
				theme.main = {
					id: mainId,
					text: mainText
				};
				text.push(mainText);
				if (subText) {
					theme.sub = {
						id: subId,
						text: subText
					};
					text.push(subText);
				}
			}
			theme.text = text.join(' / ') || 'Ingen område';

			var query = {
				query: {
					_id: courseId
				},
				update: {
					$push: {}
				},
				new: true
			};

			types.forEach(function(type) {
				query.update.$push['overwrites.themes.' + type] = theme;
			});

			purgeOldThemes.get(function(error) {
				if (!running) return done();

				if (error) {
					console.error('Could not purge old themes for courses:', error);
					errorHalt('Der opstod en database-fejl. Prøv igen om lidt.', true);
					return done();
				}

				db.courses.findAndModify(query, function(error, doc) {
					if (error) {
						console.error('Could not update themes property of course:', error);
						errors.push(courseId);
					} else if (!doc) {
						notFound.push(courseId);
					} else {
						updated++;
					}
					done();
				});
			});
		} else {
			errorHalt('Mangler en eller flere kolonner: Kursusnummer, Hovedtema, Undertema.');
			done();
		}
	}, function(done) {
		courses.purgeQueryCache();

		if (updated === 0) {
			var errorMessage = 'Ingen rækker blev opdateret. Tjek at du har de rette kolonnenavne (Kursusnummer, Hovedtema, Undertema) og at alle rækker er gyldige.';
			var errorMessageDetails = [];
			if (errors.length > 0) {
				errorMessageDetails.push(errors.length + ' række' + (errors.length === 1 ? '' : 'r') + ' med databasefejl' + listErrors(errors));
			}
			if (notFound.length > 0) {
				errorMessageDetails.push(notFound.length + ' række' + (notFound.length === 1 ? '' : 'r') + ' hvor kursusnummer ikke findes' + listErrors(notFound));
			}
			if (errorMessageDetails.length > 0) {
				errorMessage += ' Der var ' + errorMessageDetails.join(' og ') + '.';
			}
			var error = new Error(errorMessage);
			error.code = 'invalid_request';
			error.status = 400;
			return callback(error);
		}

		var message;
		var warnings = false;
		if (errors.length > 0) {
			warnings = true;
			message = 'Der var ' + errors.length + ' ' + (errors.length === 1 ? 'kursus' : 'kurser') + ', som ikke blev opdateret pga. databasefejl' + listErrors(errors) + '. Prøv venligst at uploade filen igen.';
			if (notFound.length > 0) {
				message += ' Der var ' + notFound.length + ' ' + (notFound.length === 1 ? 'kursus' : 'kurser') + ', hvor kursusnummeret ikke eksisterer' + listErrors(notFound) + '.';
			}
			message += ' Der blev med success opdateret ' + updated + ' ' + (updated === 1 ? 'kursus' : 'kurser') + '.';
		} else if (notFound.length > 0) {
			warnings = true;
			message = 'Der var ' + notFound.length + ' ' + (notFound.length === 1 ? 'kursus' : 'kurser') + ', hvor kursusnummeret ikke eksisterer' + listErrors(notFound) + '.';
			if (updated === 1) {
				message += ' Ét kursus blev med success opdateret!';
			} else {
				message += ' ' + updated + ' kurser blev med success opdateret!';
			}
		} else {
			if (updated === 1) {
				message = 'Ét kursus blev opdateret!';
			} else {
				message = updated + ' kurser blev opdateret!';
			}
		}

		callback(null, { message: message, warnings: warnings, updated: updated, errors: errors.length, notFound: notFound.length, total: updated + notFound.length + errors.length });
		done();
	}));
}

exports.upload = uploadThemesCSV;
