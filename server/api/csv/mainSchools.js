/** @module server/api/csv/mainSchools */
'use strict';

var csvParse = require('csv-parse');
var through2 = require('through2');
var csvUtil = require('./util');
var crypto = require('crypto');
var common = require('common');
var db = require('../../db');

var columnMatch = {
	'ID': 'id',
	'hovedskolenavn': 'schoolName',
	'is_el': 'is_el',
	'is_vvs': 'is_vvs',
	'is_skorsten': 'is_skorten'
};

function normalizeText(text) {
	return ('' + [text]).replace(/\s+/g, ' ').trim();
}


function listErrors(errors) {
	return csvUtil.listErrors(errors, 'titel', 'titler');
}

function uploadMainSchoolsCSV(request, callback) {
	callback = common.once(callback);
	var running = true;
	var updated = 0;
	var mainSchools = [];
	var errors = [];

	function errorHalt(message, isServerError) {
		running = false;
		var error = new Error(message);
		error.code = isServerError ? 'server_error' : 'invalid_request';
		error.status = isServerError ? 500 : 400;
		callback(error);
	}

	db.allowedSchools.remove({}, function(error, result) {
		if (error) {
			console.log('Could not remove allowed schools', error);
			errorHalt('Der opstod en database-fejl. Prøv igen om lidt.');
			callback(error);
			return;
		} 
	});

	request.pipe(csvParse({
		delimiter: ';',
		columns: function(row) {
			return row.map(function(item) {
				item = item.toLowerCase().replace(/[^a-z]+/g, '');
				return columnMatch[item] || item;
			});
		}
	})).on('error', function(error) {
		var message = 'Ugyldig CSV-fil.';
		if (error && error.message) {
			message += ' Engelsk fejlmeddelse: ' + error.message;
		}
		errorHalt(message);
	}).pipe(through2.obj(function(obj, _, done) {
		if (!running) return done();

		if ('id' in obj && 'schoolName' in obj && 'isel' in obj && 'isvvs' in obj && 'isskorsten' in obj) {
			var id = obj.id;
			var schoolName = normalizeText(obj.schoolName);

			var types = [];

			if (obj.isel === 'x') {
				types.push('electrical');
			}

			if (obj.isvvs === 'x') {
				types.push('plumbing');
			}

			if (obj.isskorsten === 'x') {
				types.push('chimney');
			}

			var mainSchoolsData = {
				id: id,
				schoolName: schoolName,
				types: types
			};

			mainSchools.push(mainSchoolsData);
			done();

	} else {
		errorHalt('Mangler en eller flere kolonner: ID, Hovedskolenavn, is_el, is_vvs, is_skorsten');
		done();
	}
	}, function(done) {
		var message = null;
		var warnings = false;

		db.allowedSchools.insert(mainSchools, function(err) {
				if (err) {
					warnings = true;
					message = 'Kunne ikke indsætte hovedskoler pga. databasefejl. Prøv venligst at uploade filen igen.';
				} else {
					message = 'Hovedskoler blev uploadet!';
				}

				callback(null, { message: message, warnings: warnings, updated: updated, errors: errors.length, total: updated + errors.length });
				done();
		});
	}));
}

exports.upload = uploadMainSchoolsCSV;
