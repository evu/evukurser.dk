/** @module server/api/csv/campaigns */
'use strict';

var csvParse = require('csv-parse');
var through2 = require('through2');
var courses = require('../courses');
var csvUtil = require('./util');
var common = require('common');
var db = require('../../db');

var columnMatch = {
	'kviknumre': 'kviknummer',
	'kviknr': 'kviknummer'
};

function listErrors(errors) {
	return csvUtil.listErrors(errors, 'kviknummer', 'kviknumre');
}

function uploadCampaignsCSV(request, callback) {
	callback = common.once(callback);
	var running = true;
	var updated = 0;
	var notFound = [];
	var errors = [];

	var purgeOldCampaigns = common.future();
	db.courses.update(
		{ 'overwrites.campaigns': { $exists: true } },
		{ $unset: { 'overwrites.campaigns': '' } },
		{ multi: true },
		function(error) {
			if (error) {
				return purgeOldCampaigns.put(error);
			}

			setTimeout(function() {
				purgeOldCampaigns.put(null);
			}, 500);
		}
	);

	function errorHalt(message) {
		running = false;
		var error = new Error(message);
		error.code = 'invalid_request';
		error.status = 400;
		callback(error);
	}

	request.pipe(csvParse({
		delimiter: ';',
		columns: function(row) {
			return row.map(function(item) {
				item = item.toLowerCase().replace(/[^a-z]+/g, '');
				return columnMatch[item] || item;
			});
		}
	})).on('error', function(error) {
		var message = 'Ugyldig CSV-fil.';
		if (error && error.message) {
			message += ' Engelsk fejlmeddelse: ' + error.message;
		}
		errorHalt(message);
	}).pipe(through2.obj(function(obj, _, done) {
		if (!running) return done();

		if ('kviknummer' in obj) {
			var classId = ('' + [obj.kviknummer]).trim();

			var query = {
				query: {
					'classes.id': classId
				},
				update: {
					$addToSet: {
						'overwrites.campaigns': classId
					}
				},
				new: true
			};

			purgeOldCampaigns.get(function(error) {
				if (!running) return done();

				if (error) {
					console.error('Could not purge old campaign classes:', error);
					errorHalt('Der opstod en database-fejl. Prøv igen om lidt.', true);
					return done();
				}

				db.courses.findAndModify(query, function(error, doc) {
					if (error) {
						console.error('Could not update campaigns property of course:', error);
						errors.push(classId);
					} else if (!doc) {
						notFound.push(classId);
					} else {
						updated++;
					}
					done();
				});
			});
		} else {
			errorHalt('Mangler kolonnen Kviknummer.');
			done();
		}
	}, function(done) {
		courses.purgeQueryCache();

		if (updated === 0) {
			var errorMessage = 'Ingen rækker blev opdateret. Tjek at du har de rette kolonnenavne (Kviknummer) og at alle rækker er gyldige.';
			var errorMessageDetails = [];
			if (errors.length > 0) {
				errorMessageDetails.push(errors.length + ' række' + (errors.length === 1 ? '' : 'r') + ' med databasefejl' + listErrors(errors));
			}
			if (notFound.length > 0) {
				errorMessageDetails.push(notFound.length + ' række' + (notFound.length === 1 ? '' : 'r') + ' hvor kviknummer ikke findes' + listErrors(notFound));
			}
			if (errorMessageDetails.length > 0) {
				errorMessage += ' Der var ' + errorMessageDetails.join(' og ') + '.';
			}
			var error = new Error(errorMessage);
			error.code = 'invalid_request';
			error.status = 400;
			return callback(error);
		}

		var message;
		var warnings = false;
		if (errors.length > 0) {
			warnings = true;
			message = 'Der var ' + errors.length + ' hold, som ikke blev opdateret pga. databasefejl' + listErrors(errors) + '. Prøv venligst at uploade filen igen.';
			if (notFound.length > 0) {
				message += ' Der var ' + notFound.length + ' hold, hvor kviknummeret ikke eksisterer' + listErrors(notFound) + '.';
			}
			message += ' Der blev med success opdateret ' + updated + ' ' + (updated === 1 ? 'kursus' : 'kurser') + '.';
		} else if (notFound.length > 0) {
			warnings = true;
			message = 'Der var ' + notFound.length + ' hold, hvor kviknummeret ikke eksisterer' + listErrors(notFound) + '.';
			if (updated === 1) {
				message += ' Ét hold blev med success opdateret!';
			} else {
				message += ' ' + updated + ' hold blev med success opdateret!';
			}
		} else {
			if (updated === 1) {
				message = 'Ét hold blev opdateret!';
			} else {
				message = updated + ' hold blev opdateret!';
			}
		}

		callback(null, { message: message, warnings: warnings, updated: updated, errors: errors.length, notFound: notFound.length, total: updated + notFound.length + errors.length });
		done();
	}));
}

exports.upload = uploadCampaignsCSV;
