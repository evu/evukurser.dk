/** @module server/api/csv/courses */
'use strict';

var zipcodeRegions = require('../../webservices/courses/zipcodeRegions');
var zipcodes = require('../../webservices/courses/zipcodes');
var csvParse = require('csv-parse');
var through2 = require('through2');
var courses = require('../courses');
var csvUtil = require('./util');
var crypto = require('crypto');
var common = require('common');
var db = require('../../db');

var columnMatch = {
	'navn': 'titel',
	'kursustitel': 'titel',
	'postnummer': 'sted',
	'postnr': 'sted',
	'mlgruppe': 'maalgruppe',
	'målgruppe': 'maalgruppe',
	'url': 'link'
};

function normalizeText(text) {
	return ('' + [text]).replace(/\s+/g, ' ').trim();
}

function normalizePrice(text) {
	text = normalizeText(text);
	if (!text || /^gratis$/i.test(text)) text = '0';
	var amount = parseInt(text, 10) | 0;
	text = text.replace(/^[,.\d]+/, '').trim();
	var price = {
		amount: amount,
		currency: 'DKK'
	};
	if (text) {
		price.note = text;
	}
	return price;
}

function normalizeDescriptionText(text) {
	return ('' + [text]).replace(/[ \t]+/g, ' ').replace(/[ \t]+\n|\n[ \t]+/g, '\n').replace(/\n+/g, '\n\n').trim();
}

function normalizeDate(text) {
	text = normalizeText(text);
	if (!text) return null;
	if (/^\d{4}-\d{2}-\d{2}$/.test(text)) return text;

	var fields = text.match(/^(\d{1,2})[\-\/](\d{1,2})[\-\/](\d{2}|\d{4})$/);
	if (!fields) return undefined;

	var day = fields[1] | 0;
	var month = fields[2] | 0;
	var year = fields[3] | 0;
	if (year < 100) year += 2000;

	return year + '-' + ('00' + month).slice(-2) + '-' + ('00' + day).slice(-2);
}

function normalizeTime(text) {
	text = normalizeText(text);
	if (!text) return null;

	var fields = text.match(/^(\d{1,2})[:.](\d{2})(?:[:.]\d{2})?$/);
	if (!fields) return undefined;

	var hours = fields[1] | 0;
	var minutes = fields[2] | 0;

	return hours + ':' + ('00' + minutes).slice(-2);
}

function parseTimeAsHours(text) {
	var fields = text.match(/^(\d{1,2}):(\d{2})$/);
	if (!fields) return 0;
	return fields[1] + fields[2] * (1/60);
}

function quarterForDate(text) {
	var fields = text.match(/^(\d{4})-(\d{2})-(\d{2})$/);
	if (!fields) return null;
	return fields[1] + 'Q' + (((fields[2] - 1) / 3 | 0) + 1);
}

function makeId(title, type, zipcode, address) {
	var key = [title, type, zipcode, address].join('\n');
	var keyId = parseInt(crypto.createHash('md5').update(key).digest('hex').slice(0, 7), 16);
	return 's' + (keyId % 1000000);
}

function listErrors(errors) {
	return csvUtil.listErrors(errors, 'titel', 'titler');
}

function uploadCoursesCSV(request, callback) {
	callback = common.once(callback);
	var running = true;
	var updated = 0;
	var errors = [];

	var purgeOldCourses = common.future();
	db.courses.remove({ 'overwrites.custom': true }, function(error) {
		if (error) {
			return purgeOldCourses.put(error);
		}

		setTimeout(function() {
			purgeOldCourses.put(null);
		}, 500);
	});

	function errorHalt(message, isServerError) {
		running = false;
		var error = new Error(message);
		error.code = isServerError ? 'server_error' : 'invalid_request';
		error.status = isServerError ? 500 : 400;
		callback(error);
	}

	request.pipe(csvParse({
		delimiter: ';',
		columns: function(row) {
			return row.map(function(item) {
				item = item.toLowerCase().replace(/[^a-z]+/g, '');
				return columnMatch[item] || item;
			});
		}
	})).on('error', function(error) {
		var message = 'Ugyldig CSV-fil.';
		if (error && error.message) {
			message += ' Engelsk fejlmeddelse: ' + error.message;
		}
		errorHalt(message);
	}).pipe(through2.obj(function(obj, _, done) {
		if (!running) return done();

		if ('titel' in obj && 'sted' in obj && 'branche' in obj && 'holdbeskrivelse' in obj &&
				'maalgruppe' in obj && 'link' in obj && 'prisalle' in obj &&
				'prisandre' in obj && 'beskrivelse' in obj && 'adresse' in obj &&
				'maxantal' in obj && 'dato' in obj && 'start' in obj && 'slut' in obj) {

			var title = normalizeText(obj.titel);
			var zipcode = normalizeText(obj.sted);
			var typeInCSV = normalizeText(obj.branche).toLowerCase();
			//var organizerName = normalizeText(obj.arrangoer);
			


			var schoolName = normalizeText(obj.holdbeskrivelse);
			var targetAudience = normalizeText(obj.maalgruppe);
			

			var registrationLink = normalizeText(obj.link);
			var priceFull = normalizePrice(obj.prisalle);
			var priceTargetGroup = normalizePrice(obj.prisandre);
			var description = normalizeDescriptionText(obj.beskrivelse);
			var address = normalizeText(obj.adresse);
			var maxParticipants = normalizeText(obj.maxantal);
			var date = normalizeDate(obj.dato);
			var timeBegin = normalizeTime(obj.start);
			var timeEnd = normalizeTime(obj.slut);

			if (date === undefined || timeBegin === undefined || timeEnd === undefined ||
					(timeBegin && !timeEnd) || (timeEnd && !timeBegin)) {

				var errorFields = [];
				if (date === undefined) errorFields.push('Dato');
				if (!(timeBegin && timeEnd)) {
					if (!timeBegin) errorFields.push('Start');
					if (!timeEnd) errorFields.push('Slut');
				}

				var errorFieldsMessage = '';
				if (errorFields.length === 1) {
					errorFields = ' (ugyldig felt: ' + errorFields[0] + ')';
				} else if (errorFields.length > 1) {
					errorFields = ' (ugyldige felter: ' + errorFields.slice(0, 	-1).join(', ') + ' og ' + errorFields.slice(-1)[0] + ')';
				}

				console.error('Invalid date, begin, and/or end time for custom course titled: ' + title);
				errors.push('“' + title + '”' + errorFieldsMessage);
				return done();
			}

			var timeDurationText = null;
			var timeDuration = null;
			if (timeBegin && timeEnd) {
				timeDurationText = timeBegin + '-' + timeEnd;
				timeDuration = parseTimeAsHours(timeEnd) - parseTimeAsHours(timeBegin);
			}

			if (/^\d{3}$/.test(zipcode)) {
				zipcode = '0' + zipcode;
			}
			var city = zipcodes[zipcode];
			var region = zipcodeRegions[zipcode] || 'Ukendt region';

			var types;
			if (/skorsten/.test(typeInCSV)) {
				types = ['chimney'];
			} else if (/vvs/.test(typeInCSV)) {
				types = ['plumbing'];
			} else if (/el/.test(typeInCSV)) {
				types = ['electrical'];
			} else {
				types = [];
			}

			var type = types[0] || 'other';
			var id = makeId(title, type, zipcode, address);

			var courseData = {
				_id: id,
				src: {
					competences: [],
					schools: [],
					certificate: false,
					admission: {
						text: targetAudience
					},
					duration: null,
					description: {
						text: description
					},
					title: title,
					id: id
				},
				overwrites: {
					types: types,
					typesCount: types.length,
					jobareas: {
						'electrical': [],
						'plumbing': [],
						'chimney': []
					},
					custom: true
				},
				classes: [{
					school: {
						id: id,
						name: schoolName,
						geo: {
							properties: {
								address: {
									formatted: (address ? address + ', ' : '') + zipcode + (city ? ' ' + city : ''),
									zipcode: zipcode,
									city: city,
									region: region
								}
							}
						}
					},
					type: {
						id: 'day',
						name: 'Dagundervisning'
					},
					canceled: false,
					guaranteed: false,
					duration: {
						begin: date,
						end: date,
						quarter: date ? quarterForDate(date) : null,
						days: 1,
						time: timeDurationText,
						hoursPerDay: timeDuration,
						text: '1 dag' + (timeDurationText ? ' ' + timeDurationText : '')
					},
					participants: {
						maximum: maxParticipants,
						minimum: null,
						remaining: null
					},
					registration: {
						deadline: null,
						link: registrationLink
					},
					price: {
						targetGroup: priceTargetGroup,
						full: priceFull
					},
					contact: null
				}],
				classesInfo: {
					guaranteed: false,
					duration: (date ? {
						begin: date,
						end: date,
						quarters: [ quarterForDate(date) ]
					} : null),
					participants: {
						remaining: null
					}
				}
			};

			purgeOldCourses.get(function(error) {
				if (!running) return done();

				if (error) {
					console.error('Could not purge old custom courses:', error);
					errorHalt('Der opstod en database-fejl. Prøv igen om lidt.', true);
					return done();
				}

				db.courses.save(courseData, function(error) {
					if (error) {
						console.error('Could not save custom course:', error);
						errors.push('“' + title + '”');
					} else {
						updated++;
					}
					done();
				});
			});
		} else {
			errorHalt('Mangler en eller flere kolonner: Titel, Sted, Branche, Holdbeskrivelse, Målgruppe, Link, Pris_alle, Pris_andre, Beskrivelse, Adresse, Max_antal, Dato, Start, Slut.');
			done();
		}
	}, function(done) {
		courses.purgeQueryCache();

		if (updated === 0) {
			var errorMessage = 'Ingen rækker blev opdateret. Tjek at du har de rette kolonnenavne (Titel, Sted, Branche, Holdbeskrivelse, Målgruppe, Link, Pris_alle, Pris_andre, Beskrivelse, Adresse, Max_antal, Dato, Start, Slut) og at alle rækker er gyldige.';
			var errorMessageDetails = [];
			if (errors.length > 0) {
				errorMessageDetails.push(errors.length + ' række' + (errors.length === 1 ? '' : 'r') + ' med databasefejl' + listErrors(errors));
			}
			if (errorMessageDetails.length > 0) {
				errorMessage += ' Der var ' + errorMessageDetails.join(' og ') + '.';
			}
			var error = new Error(errorMessage);
			error.code = 'invalid_request';
			error.status = 400;
			return callback(error);
		}

		var message;
		var warnings = false;
		if (errors.length > 0) {
			warnings = true;
			message = 'Der var ' + errors.length + ' arrangement' + (errors.length === 1 ? '' : 'er') + ', som ikke blev uploadet pga. databasefejl' + listErrors(errors) + '. Prøv venligst at uploade filen igen. Der blev med success uploadet ' + updated + ' arrangement' + (updated === 1 ? '' : 'er') + '.';
		} else {
			if (updated === 1) {
				message = 'Ét arrangement blev uploadet!';
			} else {
				message = updated + ' arrangementer blev uploadet!';
			}
		}

		callback(null, { message: message, warnings: warnings, updated: updated, errors: errors.length, total: updated + errors.length });
		done();
	}));
}

exports.upload = uploadCoursesCSV;
