/** @module server/api/csv/util */
'use strict';

function listErrors(errors, single, multiple) {
	if (errors.length === 0) return '';
	if (errors.length === 1) return ' (' + single + ': ' + errors[0] + ')';
	if (errors.length <= 500) return ' (' + multiple + ': ' + errors.join(', ') + ')';
	var andMore = 'og ' + (errors.length - 500) + ' til ...';
	return ' (' + multiple + ': ' + errors.slice(0, 500).join(', ') + ', ' + andMore + ')';
}

exports.listErrors = listErrors;
