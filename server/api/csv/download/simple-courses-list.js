/** @module server/api/csv/download/simple-courses-list */
'use strict';

var generator = require('./generator');
var db = require('../../../db');

function downloadSimpleCoursesList(type, response) {
	if (!response) {
		response = type;
		type = null;
	}

	var filenameType;

	var header = ['Kursusnummer'];
	if (type === 'electrical') {
		filenameType = 'el';
		header.push('El');
	} else if (type === 'plumbing') {
		filenameType = 'vvs';
		header.push('VVS');
	} else if (type === 'chimney') {
		filenameType = 'skorsten';
		header.push('Skorsten');
	} else {
		filenameType = 'alle';
		type = null;
		header.push('El');
		header.push('VVS');
		header.push('Skorsten');
	}
	header.push('Kursustitel');

	var csv = generator('kursusliste-' + filenameType + '.csv', {
		'Kursusnummer': 'id',
		'El': 'electrical',
		'VVS': 'plumbing',
		'Skorsten': 'chimney',
		'Kursustitel': 'title'
	}, header, response);

	var query = {};
	if (type) {
		query['overwrites.types'] = type;
	} else {
		query['overwrites.typesCount'] = { $gte: 1 };
	}

	db.courses.find(query, {
		'src.title': 1,
		'overwrites.typesCount': 1,
		'overwrites.types': 1
	}).sort({
		'overwrites.typesCount': 1,
		'overwrites.types.0': 1,
		'_id': 1
	}).forEach(function(error, doc) {
		if (error) {
			return csv.emitError(error);
		}

		if (!doc) {
			return csv.emitEnd();
		}

		var overwrites = doc.overwrites || {};
		var types = overwrites.types || [];
		var src = doc.src || {};

		csv.emitRow({
			id: doc._id,
			electrical: types.indexOf('electrical') >= 0 ? 'EL' : '',
			plumbing: types.indexOf('plumbing') >= 0 ? 'VVS' : '',
			chimney: types.indexOf('chimney') >= 0 ? 'SKORSTEN' : '',
			title: src.title
		});
	});
}

module.exports = downloadSimpleCoursesList;
