/** @module server/api/csv/download/generator */
'use strict';

function noop() {}

function generator(filename, map, header, response) {
	var self = {};
	var emittedHeader = false;
	var properties = header.map(function(column) {
		return map[column];
	});

	response.setHeader('Content-Type', 'text/csv; charset=UTF-8');
	response.setHeader('Content-Disposition', 'attachment; filename="' + filename + '"');

	function emitCSV(entries) {
		response.write(entries.map(function(entry) {
			return '' + [entry];
		}).join(';') + '\r\n');
	}

	function closeHandlers() {
		self.emitRow = noop;
		self.emitError = noop;
		self.emitEnd = noop;
	}

	function emitHeader() {
		if (!emittedHeader) {
			emittedHeader = true;
			emitCSV(header);
		}
	}

	self.emitRow = function(obj) {
		emitHeader();
		emitCSV(properties.map(function(property) {
			return obj[property];
		}));
	};

	self.emitEnd = function() {
		closeHandlers();
		emitHeader();
		response.end();
	};

	self.emitError = function(error) {
		closeHandlers();
		if (!emittedHeader) {
			response.status(500);
		}

		response.end('\r\nDer opstod en fejl med at generere CSV-filen!\r\n');
		console.error('Failed to generate CSV file ' + filename + ':', error.stack || error);
	};

	return self;
}

module.exports = generator;
