/** @module server/api/csv/download/simple-courses-list */
'use strict';

var generator = require('./generator');
var db = require('../../../db');

function downloadMainSchools(type, response) {
	if (!response) {
		response = type;
		type = null;
	}

	var csv = generator('hovedskoler.csv', {
		'"ID"': 'id',
		'"Hovedskolenavn"': 'schoolName',
		'"is_el"': 'is_el',
		'"is_vvs"': 'is_vvs',
		'"is_skorsten"': 'is_skorsten'
	}, ['"ID"', '"Hovedskolenavn"', '"is_el"', '"is_vvs"', '"is_skorsten"'], response);

	var query = {};

	db.allowedSchools.find(query).forEach(function(error, doc) {
		if (error) {
			return csv.emitError(error);
		}

		if (!doc) {
			return csv.emitEnd();
		}

		var is_el = '';
		var is_vvs = '';
		var is_skorsten = '';

		if (doc.types && Array.isArray(doc.types)) {
			var types = doc.types;

			if (types.indexOf('electrical') !== -1) {
				is_el = 'x';
			}

			if (types.indexOf('plumbing') !== -1) {
				is_vvs = 'x';
			}

			if (types.indexOf('chimney') !== -1) {
				is_skorsten = 'x';
			}
		}

		csv.emitRow({
			id: doc.id,
			schoolName: doc.schoolName,
			is_el: is_el,
			is_vvs: is_vvs,
			is_skorsten: is_skorsten
		});
	});
}

module.exports = downloadMainSchools;

