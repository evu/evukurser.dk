/** @module server/api/csv/download/simple-courses-list */
'use strict';

var generator = require('./generator');
var db = require('../../../db');

function downloadAllSchools(type, response) {
	if (!response) {
		response = type;
		type = null;
	}

	var csv = generator('skoler.csv', {
		'"ID"': 'id',
		'"Hovedskolenavn"': 'schoolName'
	}, ['"ID"', '"Hovedskolenavn"'], response);

	var query = {};

	db.courses.distinct('classes.school', query, function(error, docs) {
		if (!docs || !docs.length) {
			csv.emitError('Ingen skoler blev fundet');
			return;
		} 

		docs.forEach(function(doc) {
			if (error) {
				return csv.emitError(error);
			}

			if (doc && doc.id && doc.name) {
				csv.emitRow({
					'id': doc.id,
					'schoolName': doc.name
				});
			}

		});

		csv.emitEnd();

	});
}

module.exports = downloadAllSchools;