/** @module server/api/csv/download */
'use strict';

exports.simple_courses_list = require('./simple-courses-list');
exports.courseAndSchoolExtract = require('./courseAndSchoolExtract');
exports.mainSchools = require('./mainSchools');
exports.allSchools = require('./allSchools');