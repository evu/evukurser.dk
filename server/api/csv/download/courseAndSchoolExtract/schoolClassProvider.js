'use strict';

var db = require('../../../../db');
var request = require('request');
var common = require('common');
var moment = require('moment');

// Logging
var log4js = require('log4js');
var logger = log4js.getLogger('Get-all-courses');


function get(callback) {
    common.step([
        // Get all course ID's
        function(next) {
            getCourseIDs(null, null, next);
        },
        // Get course info for each course and extend the existing course objects
        function(courseIDs, next) {
            //courseIDs = ['40003'];
            logger.info(courseIDs.length + ' course IDs found');

            courseIDs.forEach(function(course) {
                getCourseInfo(course, next.parallel());
            });
        },
        function(courses, next) {
            courses.forEach(function(course) {
                parseSchoolsFromCourseInfo(course, next.parallel());
            });
        },
        function(courses, next) {
            logger.info('Parsed schools from course info');

            mergeSchoolClasses(courses, next);
        },
        function(schoolClasses, next) {
            logger.info('Merged ' + schoolClasses.length + ' school classes');
            logger.info('Done');

            callback(null, schoolClasses);
            return;
        },
        function(result, next) {
            logger.info('Done');

        }
    ], function(error) {
        logger.fatal(error);
        callback(error);
    });
}

function mergeSchoolClasses(courses, callback) {
    var coursesMerged = [];

    courses.forEach(function(arrayInRow) {
        coursesMerged = coursesMerged.concat(arrayInRow);
    });

    callback(null, coursesMerged);
}

function parseSchoolsFromCourseInfo(course, callback) {
    var result = [];

    course.klasser.forEach(function(schoolClass) {
        var now = moment();
        var startDate = moment(schoolClass.duration.begin);

        if (startDate < now) {
            return;
        }

        var parsedSchoolClass = course.courseDescription;
        parsedSchoolClass.region = (schoolClass.school.geo && schoolClass.school.geo.properties && schoolClass.school.geo.properties.address && schoolClass.school.geo.properties.address.region) ? schoolClass.school.geo.properties.address.region : 'ingen region oplyst';
        parsedSchoolClass.undervisningssted = (schoolClass.school && schoolClass.school.name) ? schoolClass.school.name : 'intet navn';
        parsedSchoolClass.varighed_dage = schoolClass.duration.days;
        parsedSchoolClass.start_dato = schoolClass.duration.begin;
        parsedSchoolClass.pris = (schoolClass.price && schoolClass.price.targetGroup && schoolClass.price.targetGroup.amount) ? schoolClass.price.targetGroup.amount + schoolClass.price.targetGroup.currency : 'ingen pris oplyst';
        //parsedSchoolClass.undervisnings_sted = (schoolClass.school.geo && schoolClass.school.geo.properties && schoolClass.school.geo.properties.address && schoolClass.school.geo.properties.address.formatted) ? schoolClass.school.geo.properties.address.formatted : 'ingen adresse oplyst';
        parsedSchoolClass.kviknummer = (schoolClass.registration && schoolClass.registration.link) ? schoolClass.registration.link.replace('http://www.efteruddannelse.dk/kursus?kviknr=', '') : 'intet kviknummer';

        if (parsedSchoolClass.jobomraader && parsedSchoolClass.jobomraader.length >= 1) {

            for (var i = 0; i < parsedSchoolClass.jobomraader.length; i++) {
                var jobomraade = parsedSchoolClass.jobomraader[i];
                parsedSchoolClass.jobomraade = jobomraade.main.text;
                parsedSchoolClass.temaomraade = jobomraade.sub.text;
                result.push(parsedSchoolClass);
            }
        } else {
            parsedSchoolClass.jobomraade = null;
            result.push(parsedSchoolClass);
        }

        delete parsedSchoolClass.jobomraader;
    });

    callback(null, result);
}

function getCourseInfo(courseID, callback) {
    db.courses.findOne({
        '_id': courseID
    }, function(error, course) {
        if (error) {
            callback(error, course);
            return;
        }

        var result = {
            courseDescription: {
                fagnummer: courseID,
                fagbetegnelse: (course.src.title) ? course.src.title : 'ingen titel',
                //kursusnavnbetegnelse: (course.src.description && course.src.description.text) ? course.src.description.text : 'Ingen beskrivelse', 
                //holdbetegnelse: (course.src.admission && course.src.admission.text) ? course.src.admission.text : 'ingen holdbetegnelse',
                garantikursus: ((course.classesInfo && course.classesInfo.guaranteed && (course.classesInfo.guaranteed === true || course.classesInfo.guaranteed === "true")) ? 'ja' : 'nej'),
                jobomraader: (course.overwrites.jobareas && course.overwrites && course.overwrites.jobareas.all) ? course.overwrites.jobareas.all : null

            },
            klasser: course.classes
        };

        callback(null, result);
    });
}

function getCourseIDs(offset, rows, callback) {
    var startDate = moment().format('YYYY-MM-DD');
    //var url = 'http://127.0.0.1:10001/api/v1/courses?start-date=' + startDate + '&offset=' + offset;
    var url = 'http://127.0.0.1:3000/api/v1/courses?start-date=' + startDate + '&offset=' + offset;


    if (!rows) {
        rows = [];
    }

    if (!offset) {
        offset = 0;
    }

    request.get(url, function(error, response, body) {
        parseJSON(body, function(error, data) {
            if (error) {
                callback(error);
                return;
            }

            var items = data.items;
            offset = data.pagination.offset + 50;

            for (var key in items) {
                var course = items[key];
                rows.push(course.id);
            }

            if ((data.pagination.limit + data.pagination.offset) < data.count) {
                getCourseIDs(offset, rows, callback);
                return;
            }

            callback(error, rows);
        });
    });
}

function parseJSON(data, callback) {
    var parsedData = null;

    try {
        parsedData = JSON.parse(data);
    } catch (error) {
        callback(new Error('Could not parse JSON: ' + error));
        return;
    }

    callback(null, parsedData);
}

module.exports.get = get;
