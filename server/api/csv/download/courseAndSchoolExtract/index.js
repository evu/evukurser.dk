'use strict';

var generator = require('../generator');
var schoolClassProvider = require('./schoolClassProvider');
var _ = require('underscore');

function download(type, response) {
    if (!response) {
        response = type;
        type = null;
    }

    var header = [];
    var headerMapping = [];
    schoolClassProvider.get(function(error, schoolClasses) {
        if (error) {
            response.write('Der opstod en fejl under eksportering af skoleklasser.');
            response.end();
            return;
        }


        for (var key in schoolClasses[0]) {
            header.push(key);
            headerMapping[key] = key;
        }

        var csv = generator('schoolclasslist.csv', headerMapping, header, response);

        var uniques = _.uniq(schoolClasses, function (a) {
    		return 'kvik: ' + a.kviknummer + 'region:' + a.region + 'start_dato:' + a.start_dato + 'temaomraade:' + a.temaomraade;
		});

        uniques.forEach(function(schoolClass) {
            csv.emitRow(schoolClass);
        });

        csv.emitEnd();

    });
}

module.exports = download;
