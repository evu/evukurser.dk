/** @module server/api/apilog */
'use strict';

var util = require('util');
var db = require('../db');

function noop() {}

function removeSecretDataRecursively(data) {
	if (typeof(data) === 'object') {
		if (Array.isArray(data)) {
			var size = data.length;
			for (var i = 0; i < size; i++) {
				removeSecretDataRecursively(data[i]);
			}
		} else {
			for (var key in data) {
				var value = data[key];
				if (/password/i.test(key) && typeof(value) === 'string') {
					data[key] = '****';
				} else if (/cpr/i.test(key) && typeof(value) === 'string') {
					data[key] = value.replace(/\d/g, '*');
				} else {
					removeSecretDataRecursively(data[key]);
				}
			}
		}
	}
}

function censoredJson(data) {
	try {
		// Make sure data is proper JSON serializable.
		// Also ensures that data is deep cloned before potentially removing secret data.
		var json = JSON.stringify(data || null);
		data = JSON.parse(json);

		// Test for secret data keys and if so, remove those secret data.
		if (/password|cpr/i.test(json)) {
			removeSecretDataRecursively(data);
		}

		return data;
	} catch (error) {
		return { JSON_ERROR: '' + (error.message || error) };
	}
}

function apilogSaveRequest(request, callback) {
	callback = callback || noop;

	var now = new Date();

	var doc = {
		req: {
			time: now,
			clientip: request.client.remoteAddress,
			method: request.method,
			url: request.url,
			headers: request.headers,
			body: censoredJson(request.body)
		},
		created: now,
		updated: now
	};

	// console.log(util.inspect(doc, { colors: true, depth: 5 }));

	var query = {
		query: { _id: db.ObjectId() },
		update: doc,
		upsert: true,
		new: true
	};

	db.apilog.findAndModify(query, function(error, doc) {
		if (error) return callback(error);
		if (!doc) return callback(new Error('Failed to save API request in log.'));
		callback(null, doc._id);
	});
}

function apilogSaveResponse(requestObjectId, status, body, headers, callback) {
	callback = callback || noop;

	var now = new Date();

	var doc = {
		res: {
			time: now,
			status: status,
			body: censoredJson(body),
			headers: headers || {}
		},
		updated: now
	};

	// console.log(util.inspect(doc, { colors: true, depth: 5 }));

	var query = {
		query: { _id: requestObjectId },
		update: {
			$set: doc
		},
		upsert: true,
		new: true
	};

	if (!requestObjectId) {
		query.query._id = db.ObjectId();
	}

	db.apilog.findAndModify(query, function(error, doc) {
		if (error) return callback(error);
		if (!doc) return callback(new Error('Failed to save API response in log.'));
		callback(null, doc._id);
	});
}

function apilogSaveAuth(requestObjectId, authInfo, callback) {
	callback = callback || noop;

	var now = new Date();

	// Cut away the repeating stuff and add time stamp.
	authInfo = util._extend({}, authInfo);
	delete authInfo.client;
	authInfo.time = now;
	authInfo.scope = Object.keys(authInfo.scope).sort();

	var query = {
		query: { _id: requestObjectId },
		update: {
			$set: {
				'req.auth': authInfo,
				updated: now
			}
		},
		upsert: true,
		new: true
	};

	if (!requestObjectId) {
		query.query._id = db.ObjectId();
	}

	// console.log(util.inspect(query, { colors: true, depth: 5 }));

	db.apilog.findAndModify(query, function(error, doc) {
		if (error) return callback(error);
		if (!doc) return callback(new Error('Failed to save API auth in log.'));
		callback(null, doc._id);
	});
}

exports.saveRequest = apilogSaveRequest;
exports.saveResponse = apilogSaveResponse;
exports.saveAuth = apilogSaveAuth;
