/** @module server/api/user */
'use strict';

var provider = require('./oauth2/provider');
var dataurl = require('../dataurl');
var scrypt = require('scrypt');
var common = require('common');
var email = require('../email');
var uuid = require('../uuid');
var cpr = require('./cpr');
var db = require('../db');

function formatUser(doc, options) {
	var notifications = doc.notifications || {};

	var user = {
		id: db.encodeId(doc._id),
		name: doc.name,
		picture: doc.picture,
		email: doc.email,
		emailVerified: !!doc.emailVerifiedTime,
		roles: doc.roles,
		notifications: {
			certificateExpiration: !!notifications.certificateExpiration,
			registeredCourses: !!notifications.registeredCourses,
			wishlistCourses: !!notifications.wishlistCourses 
		}
	};

	var cprVerified = !!(doc.cpr && doc.cpr.data && doc.cpr.validated && doc.cpr.validated.time);

	if (!(options && options.listing)) {
		var cprValue = '';
		if (cprVerified) {
			cprValue = cpr.decryptCPRDate(doc.cpr.data).slice(0, 6);
			cprValue = cprValue ? cprValue + '-••••' : '-';
		}
		user.cpr = cprValue;
	}
	user.cprVerified = cprVerified;

	return user;
}

function getUser(userId, callback) {
	db.users.findOne({ _id: db.decodeId(userId) }, function(error, doc) {
		if (error || !doc) {
			console.error('Could not find user:', error);
			error = new Error('Brugeren blev ikke fundet.');
			error.code = 'not_found';
			error.status = 404;
			return callback(error);
		}

		callback(null, formatUser(doc));
	});
}

function updateUser(authInfo, clientInfo, userId, options, callback) {
	var dbUserId = db.decodeId(userId);

	db.users.findOne({ _id: dbUserId }, function(error, oldUser) {
		if (error || !oldUser) {
			console.error('Could not find old user object when updating profile:', error);
			error = new Error('Brugeren blev ikke fundet.');
			error.code = 'not_found';
			error.status = 404;
			return callback(error);
		}

		var updateSet = {};

		var now = new Date();

		var oldPassword = '' + [options.oldPassword];
		if (authInfo.scope.admin) oldPassword = '';
		var canChangeEmailAndPassword = !!(oldPassword || authInfo.scope.admin);

		if ('name' in options) {
			updateSet.name = ('' + [options.name]).replace(/\s+/g, ' ').trim();
			if (!updateSet.name) {
				error = new Error('Et navn er påkrævet.');
				error.code = 'invalid_request';
				error.status = 400;
				return callback(error);
			}
		}

		if ('email' in options) {
			var newEmail = ('' + [options.email]).trim().toLowerCase();
			if (newEmail !== oldUser.email) {
				updateSet.email = newEmail;
				updateSet.emailVerifiedTime = null;
				updateSet.emailVerifyCodes = [
					{ code: uuid(), time: now }
				];
				if (!canChangeEmailAndPassword) {
					error = new Error('For at ændre e-mail-adresse skal du angive din gamle adgangskode.');
					error.code = 'invalid_request';
					error.status = 400;
					return callback(error);
				}
				if (!newEmail) {
					error = new Error('En e-mail-adresse er påkrævet.');
					error.code = 'invalid_request';
					error.status = 400;
					return callback(error);
				}
				if (!/^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,20}$/.test(newEmail)) {
					error = new Error('Den angivne e-mail-adresse er ugyldig.');
					error.code = 'invalid_request';
					error.status = 400;
					return callback(error);
				}
			}
		}

		if ('picture' in options) {
			var pictureURL = ('' + [options.picture]).trim() || null;
			if (pictureURL) {
				updateSet.picture = dataurl.image(pictureURL);
				if (!updateSet.picture) {
					error = new Error('Det angivne billede er i et ugyldigt format.');
					error.code = 'invalid_request';
					error.status = 400;
					return callback(error);
				}
			} else {
				updateSet.picture = null;
			}
		}

		if ('password' in options) {
			updateSet.password = '' + [options.password];
			updateSet.passwordResetCodes = [];
			if (!canChangeEmailAndPassword) {
				error = new Error('For at ændre adgangskode skal du angive din gamle adgangskode.');
				error.code = 'invalid_request';
				error.status = 400;
				return callback(error);
			}
			if (!updateSet.password) {
				error = new Error('En adgangskode er påkrævet.');
				error.code = 'invalid_request';
				error.status = 400;
				return callback(error);
			}
			if (updateSet.password.length < 6) {
				error = new Error('Adgangskoden er for kort (mindst 6 bogstaver, tal og/eller tegn).');
				error.code = 'invalid_request';
				error.status = 400;
				return callback(error);
			}
		}

		if (options.notifications) {
			var notifications = options.notifications;

			if (notifications.certificateExpiration !== undefined) {
				if (notifications.certificateExpiration === true || notifications.certificateExpiration === false) {
					updateSet['notifications.certificateExpiration'] = notifications.certificateExpiration;
				} else {
					error = new Error('Notifikationen certificateExpiration skal være en boolsk værdi.');
					error.code = 'invalid_request';
					error.status = 400;
					return callback(error);
				}
			} 

			if (notifications.wishlistCourses !== undefined) {
				if (notifications.wishlistCourses === true || notifications.wishlistCourses === false) {
					updateSet['notifications.wishlistCourses'] = notifications.wishlistCourses;
				} else {
					error = new Error('Notifikationen wishlistCourses skal være en boolsk værdi.');
					error.code = 'invalid_request';
					error.status = 400;
					return callback(error);
				}
			}

			if (notifications.registeredCourses !== undefined) {
				if (notifications.registeredCourses === true || notifications.registeredCourses === false) {
					updateSet['notifications.registeredCourses'] = notifications.registeredCourses;
				} else {
					error = new Error('Notifikationen registeredCourses skal være en boolsk værdi.');
					error.code = 'invalid_request';
					error.status = 400;
					return callback(error);
				}
			}
		}

		if (Object.keys(updateSet).length === 0) {
			error = new Error('Der er ikke angivet nogle opdateringer.');
			error.code = 'invalid_request';
			error.status = 400;
			return callback(error);
		}

		updateSet.updated = {
			time: now,
			client: clientInfo
		};

		common.step([

			function(next) {
				if (oldPassword) {
					provider.authenticate({ _id: dbUserId }, oldPassword, function(error) {
						if (error) {
							error = new Error('Den gamle adgangskode er ikke korrekt angivet.');
							error.code = 'invalid_grant';
							error.status = 400;
							return next(error);
						}
						next();
					});
				} else {
					next();
				}
			},

			function(next) {
				if (updateSet.password) {
					scrypt.passwordHash(options.password, {N:15,r:8,p:1}, function(error, hashedPassword) {
						if (error) {
							console.error('Could not create password hash for new user:', error);
							error = new Error('Der opstod en fejl ved opdatering af adgangskode.');
							error.code = 'server_error';
							error.status = 500;
							return next(error);
						}

						updateSet.password = hashedPassword;
						next();
					});
				} else {
					next();
				}
			},

			function(next) {
				var query = {
					query: {
						_id: dbUserId
					},
					update: {
						$set: updateSet
					},
					new: true
				};
				db.users.findAndModify(query, function(error, doc) {
					if (error || !doc) {
						console.error('Could not find or update user:', error);
						error = new Error('Brugeren blev ikke fundet eller kan ikke opdateres.');
						error.code = 'not_found';
						error.status = 404;
						return next(error);
					}

					if (updateSet.email) {
						email.emailChanged.send(doc.email, doc.name, doc.emailVerifyCodes[0].code);
					} else if (updateSet.password) {
						email.passwordChanged(doc.lastVerifiedEmail || doc.email, doc.name);
					}

					next(null, formatUser(doc));
				});
			},

			function(user) {
				callback(null, user);
			}

		], function(error) {
			callback(error);
		});

	});
}

function verifyEmail(code, callback) {
	db.users.findOne({ 'emailVerifyCodes.code': code }, function(error, doc) {
		if (error) {
			console.error('Could not find e-mail verification code:', error);
			error = new Error('Der opstod en fejl med at verificere e-mail-adressen.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		if (!doc) {
			console.error('E-mail verification code does not exists:', error);
			error = new Error('Den angivne e-mail verifikationskode er ikke gyldig.');
			error.code = 'not_found';
			error.status = 404;
			return callback(error);
		}

		var query = {
			query: {
				'email': doc.email,
				'emailVerifyCodes.code': code
			},
			update: {
				$set: {
					lastVerifiedEmail: doc.email,
					emailVerifiedTime: new Date(),
					emailVerifyCodes: []
				}
			},
			new: false
		};

		db.users.findAndModify(query, function(error, doc) {
			if (error || !doc) {
				console.error('Could not update e-mail verification:', error);
				error = new Error('Der opstod en fejl med at verificere e-mail-adressen.');
				error.code = 'server_error';
				error.status = 500;
				return callback(error);
			}

			callback(null, !doc.lastVerifiedEmail); // 2nd param: newUser (bool)
		});
	});
}

function startPasswordReset(clientInfo, options, callback) {
	options.email = ('' + [options.email]).trim().toLowerCase();
	var error;

	if (!options.email) {
		error = new Error('E-mail-adresse er påkrævet.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (!/^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,20}$/.test(options.email)) {
		error = new Error('Den angivne e-mail-adresse er ugyldig.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	var result = common.future();

	setTimeout(function() {
		result.get(callback);
	}, 500);

	var code = uuid();

	var query = {
		query: {
			'email': options.email
		},
		update: {
			$push: {
				passwordResetCodes: {
					code: code,
					time: new Date()
				}
			}
		},
		new: true
	};

	db.users.findAndModify(query, function(error, doc) {
		if (error || !doc) {
			console.error('Could not find user to reset password for:', error);
			// Fake successful response for security reasons.
			return result.put(null, { status: 'ok' });
		}

		// New users do not have a verified e-mail, in this case we send it to the unverified e-mail for convenience.
		var userEmail = doc.lastVerifiedEmail || doc.email;

		email.resetPassword.send(userEmail, doc.name, code, function(error) {
			if (error) {
				console.error('Could not send password e-mail to user:', error);
				// Tell user that e-mail failed (should be rare, and very helpful for user):
				error = new Error('E-mailen for at nulstille adgangskode kunne ikke sendes, prøv igen om lidt.');
				error.code = 'email_failed';
				error.status = 500;
				return result.put(error);
			}

			result.put(null, { status: 'ok' });
		});
	});
}

function resetPassword(clientInfo, options, callback) {
	options.code = ('' + [options.code]).trim();
	options.password = '' + [options.password];
	var error;

	if (options.code.length !== 22) {
		error = new Error('Koden til at nulstille adgangskoden er ugyldig.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (!options.password) {
		error = new Error('En ny adgangskode er påkrævet.');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	if (options.password.length < 6) {
		error = new Error('Den nye adgangskode er for kort (mindst 6 bogstaver, tal og/eller tegn).');
		error.code = 'invalid_request';
		error.status = 400;
		return callback(error);
	}

	scrypt.passwordHash(options.password, {N:15,r:8,p:1}, function(error, hashedPassword) {
		if (error) {
			console.error('Could not create password hash for new user:', error);
			error = new Error('Der opstod en fejl ved at ændre adgangskoden.');
			error.code = 'server_error';
			error.status = 500;
			return callback(error);
		}

		var query = {
			query: {
				'passwordResetCodes.code': options.code
			},
			update: {
				$set: {
					'password': hashedPassword,
					'passwordResetTime': new Date(),
					'passwordResetCodes': []
				}
			},
			new: true
		};

		db.users.findAndModify(query, function(error, doc) {
			if (error || !doc) {
				console.error('Could not find any user with given reset code:', error);
				error = new Error('Koden til at nulstille adgangskoden er ugyldig.');
				error.code = 'invalid_request';
				error.status = 400;
				return callback(error);
			}

			email.passwordChanged.send(doc.lastVerifiedEmail || doc.email, doc.name);

			callback(null, formatUser(doc));
		});
	});
}

exports.startPasswordReset = startPasswordReset;
exports.resetPassword = resetPassword;
exports.verifyEmail = verifyEmail;
exports.format = formatUser;
exports.update = updateUser;
exports.get = getUser;
