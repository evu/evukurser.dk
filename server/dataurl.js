/** @module server/db */
'use strict';

function detectImageFormat(buffer) {
	if (buffer.slice(0, 8).toString('base64') === 'iVBORw0KGgo=') return 'png';
	if (buffer.slice(0, 2).toString('base64') === '/9g=') return 'jpeg';
	return null;
}

function normalizeImageDataUrl(url) {
	var imageURLMatch = url.match(/data:image\/(png|jpeg);base64,([a-z0-9+\/]+)={0,2}/i);
	if (!imageURLMatch) return null;

	var imageFormat = imageURLMatch[1].toLowerCase();
	var imageData = new Buffer(imageURLMatch[2], 'base64');
	if (detectImageFormat(imageData) !== imageFormat) return null;

	return 'data:image/' + imageFormat + ';base64,' + imageData.toString('base64');
}

exports.image = normalizeImageDataUrl;
