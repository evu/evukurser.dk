/** @module server/email/verifyEmail */
'use strict';

var provider = require('./provider');
var config = require('../config');

function noop() {}

function sendVerifyEmailEmail(email, name, verify_code, callback) {
	callback = callback || noop;

	provider.sendEmail({
		email: email,
		name: name,
		template: 'verify-email'
	}, {
		subject: 'Verificer din e-mail på EVUKurser.dk',
		name: name, // TODO: Cut out first name if name is properly formatted
		verify_link: config.baseurl + '/verificer-email-kode/' + verify_code
	}, callback);
}

exports.send = sendVerifyEmailEmail;
