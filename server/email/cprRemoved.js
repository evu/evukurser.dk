/** @module server/email/cprRemoved */
'use strict';

var provider = require('./provider');

function noop() {}

function sendCprRemovedEmail(email, name, callback) {
	callback = callback || noop;

	provider.sendEmail({
		email: email,
		name: name,
		template: 'cpr-removed'
	}, {
		subject: 'Du har fjernet CPR-validering på EVUKurser.dk',
		name: name // TODO: Cut out first name if name is properly formatted
	}, callback);
}

exports.send = sendCprRemovedEmail;
