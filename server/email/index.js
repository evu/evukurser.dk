/** @module server/email */
'use strict';

exports.passwordChanged = require('./passwordChanged');
exports.resetPassword = require('./resetPassword');
exports.emailChanged = require('./emailChanged');
exports.cprRemoved = require('./cprRemoved');
exports.cprFailed = require('./cprFailed');
exports.cprAdded = require('./cprAdded');
exports.provider = require('./provider');
exports.contact = require('./contact');
exports.signup = require('./signup');
