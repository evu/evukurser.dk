/** @module server/email/templates */
'use strict';

require('handlebars');

exports.header = require('./header.hbs');
exports.signup = require('./signup.hbs');
exports.contact = require('./contact.hbs');
exports['verify-email'] = require('./verify-email.hbs');
exports['email-changed'] = require('./email-changed.hbs');
exports['reset-password'] = require('./reset-password.hbs');
exports['password-changed'] = require('./password-changed.hbs');
exports['cpr-added'] = require('./cpr-added.hbs');
exports['cpr-failed'] = require('./cpr-failed.hbs');
exports['cpr-removed'] = require('./cpr-removed.hbs');
exports['certificateExpired'] = require('./certificateExpired.hbs');
exports['wishlistOpenClasses'] = require('./wishlistOpenClasses.hbs');


