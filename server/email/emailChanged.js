/** @module server/email/emailChanged */
'use strict';

var provider = require('./provider');
var config = require('../config');

function noop() {}

function sendEmailChangedEmail(email, name, verify_code, callback) {
	callback = callback || noop;

	provider.sendEmail({
		email: email,
		name: name,
		template: 'email-changed'
	}, {
		subject: 'Skift af e-mail på EVUKurser.dk',
		name: name, // TODO: Cut out first name if name is properly formatted
		verify_link: config.baseurl + '/verificer-email-kode/' + verify_code
	}, callback);
}

exports.send = sendEmailChangedEmail;
