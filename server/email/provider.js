/** @module server/email/provider */
'use strict';

var templates = require('./templates');
var mandrill = require('mandrill-api/mandrill');
var config = require('../config');

var mandrillClient = new mandrill.Mandrill(config.ws.mandrill.apiKey);

function sendEmail(options, templateParameters, callback) {
	options = options || {};
	templateParameters = templateParameters || {};

	var template = templates[options.template];
	if (!template) {
		throw new Error('Template not found. Please provide valid options.template');
	}

	console.log(
		'E-mail [%s]: Sending \'%s\' to %j <%s> with content %j',
		new Date().toUTCString(),
		options.template,
		options.name,
		options.email,
		templateParameters
		);

	var mandrillOptions = {
		template_name: 'evukurser-dk',
		template_content: [
		{
			name: 'header',
			content: templates.header(templateParameters)
		},
		{
			name: 'main',
			content: template(templateParameters)
		}
		],
		message: {
			subject: '' + [templateParameters.subject],
			to: [{
				email: '' + [options.email],
				name: '' + [options.name]
			}],
			important: !!options.important,
			tags: options.tags || []
		},
		async: false
	};

	mandrillClient.messages.sendTemplate(mandrillOptions, function(result) {
		callback(null, result);
	}, function(error) {
		console.error('E-mail send failed:', error);
		callback(error);
	});
}

exports.sendEmail = sendEmail;
