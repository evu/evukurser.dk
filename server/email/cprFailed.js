/** @module server/email/cprFailed */
'use strict';

var provider = require('./provider');

function noop() {}

function sendCprFailedEmail(email, name, callback) {
	callback = callback || noop;

	provider.sendEmail({
		email: email,
		name: name,
		template: 'cpr-failed'
	}, {
		subject: 'CPR-validering på EVUKurser.dk fejlede',
		name: name // TODO: Cut out first name if name is properly formatted
	}, callback);
}

exports.send = sendCprFailedEmail;
