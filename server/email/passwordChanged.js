/** @module server/email/passwordChanged */
'use strict';

var provider = require('./provider');

function noop() {}

function sendPasswordChangedEmail(email, name, callback) {
	callback = callback || noop;

	provider.sendEmail({
		email: email,
		name: name,
		template: 'password-changed'
	}, {
		subject: 'Skift af adgangskode på EVUKurser.dk',
		name: name // TODO: Cut out first name if name is properly formatted
	}, callback);
}

exports.send = sendPasswordChangedEmail;
