/** @module server/email/contact */
'use strict';

var provider = require('./provider');

function noop() {}

function sendContactEmail(fromEmail, fromName, message, callback) {
	callback = callback || noop;

	provider.sendEmail({
		email: 'evu@evu.dk',
		name: 'EVU',
		template: 'contact'
	}, {
		subject: 'Besked fra EVUKurser.dk',
		email: fromEmail,
		name: fromName,
		message: message
	}, callback);
}

exports.send = sendContactEmail;
