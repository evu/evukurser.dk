/** @module server/email/resetPassword */
'use strict';

var provider = require('./provider');
var config = require('../config');

function noop() {}

function sendResetPasswordEmail(email, name, reset_password_code, callback) {
	callback = callback || noop;

	provider.sendEmail({
		email: email,
		name: name,
		template: 'reset-password'
	}, {
		subject: 'Ny adganskode til EVUKurser.dk',
		name: name, // TODO: Cut out first name if name is properly formatted
		reset_password_link: config.baseurl + '/nulstil-adgangskode/' + reset_password_code
	}, callback);
}

exports.send = sendResetPasswordEmail;
