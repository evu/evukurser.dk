/** @module server/email/cprAdded */
'use strict';

var provider = require('./provider');

function noop() {}

function sendCprAddedEmail(email, name, callback) {
	callback = callback || noop;

	provider.sendEmail({
		email: email,
		name: name,
		template: 'cpr-added'
	}, {
		subject: 'Du er CPR-valideret på EVUKurser.dk',
		name: name // TODO: Cut out first name if name is properly formatted
	}, callback);
}

exports.send = sendCprAddedEmail;
