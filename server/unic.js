/** @module server/unic */
'use strict';

var byline = require('byline');
var spawn = require('child_process').spawn;
var path = require('path');

var proxy;
var queue = [];
var busy = false;

function noop() {}

function parseJSON(json) {
	try {
		return JSON.parse(json);
	} catch (error) {
		return null;
	}
}

function runProxyInBackground() {
	proxy = spawn('bash', ['-c', './run.sh'], {
		cwd: path.resolve(__dirname, 'uni-c-proxy')
	});

	proxy.on('close', function(code, signal) {
		proxy = null;
		console.log(
			'UNI-C proxy exited with code %s%s. Restarting in a few seconds!',
			code,
			signal ? ' (killed by signal ' + signal + ')' : ''
		);
		setTimeout(runProxyInBackground, 1000);
	});

	proxy.stdout.setEncoding('utf8');
	proxy.stderr.setEncoding('utf8');

	var stdout = byline(proxy.stdout);
	var stderr = byline(proxy.stderr);

	stderr.on('data', function(stderrLine) {
		console.log('UNI-C proxy (err): ' + stderrLine);
	});

	stdout.on('data', function(stdoutLine) {
		var message = parseJSON(stdoutLine);
		if (message) {
			if (queue.length > 0) {
				var callback = queue.shift().callback;
				if (!(message.response && message.info)) {
					callback(new Error('An error occurred while communicated with UNI-C proxy.'));
				} else if (message.response.length === 0 && message.info.statusMessage) {
					callback(new Error(
						message.info.statusMessage +
						(message.info.statusCode ? ' (#' + message.info.statusCode + ')' : '')
					));
				} else {
					var response = (message && message.response && message.response[0]) || {};

					callback(null, {
						items: response.classes || []
					});
				}
			}
			busy = false;
			runNextInQueue();
		} else {
			console.log('UNI-C proxy (out): ' + stdoutLine);
		}
	});
}

process.on('exit', function() {
	if (proxy) {
		proxy.kill();
	}
});

runProxyInBackground();

function runNextInQueue() {
	if (busy || queue.length === 0 || !proxy) return;
	busy = true;
	proxy.stdin.write(queue[0].cpr + '\n');
}

function completedClasses(cpr, callback) {
	cpr = '' + cpr;
	if (typeof(callback) !== 'function') {
		callback = noop;
	}

	if (!/^\d{10}$/.test(cpr)) {
		return callback(new Error('Invalid CPR number.'));
	}

	if (queue.length > 10) {
		return callback(new Error('Too busy serving requests.'));
	}

	if (process.env.NODE_ENV !== 'production') {
		cpr = '0102030405'; // works in test env
	}

	queue.push({
		cpr: cpr,
		callback: callback
	});
	runNextInQueue();
}

exports.completedClasses = completedClasses;
