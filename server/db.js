/** @module server/db */
'use strict';

var async = require('async');
var common = require('common');
var mongojs = require('mongojs');
var db = mongojs('127.0.0.1:27000/evukurser_dev', [
	'competences',
	'courses',
	'geocode',
	'users',
	'refreshToken',
	'accessToken',
	'news',
	'apilog',
	'certificateEmailReminders',
	'mainSchools',
	'allowedSchools',
	'schoolTypes'
]);

function indexEnsured(error) {
	if (error) {
		console.error('Could not ensure index in database:', error);
	}
}

db.courses.ensureIndex({
	'src.title': 'text',
	'src.description.text': 'text',
	'src.admission.text': 'text'
}, {
	default_language: 'danish',
	weights: {
		'src.title': 10,
		'src.description.text': 3
	},
	name: 'TextIndex'
}, indexEnsured);

db.geocode.ensureIndex({ address: 1 }, { unique: true }, indexEnsured);

db.users.ensureIndex({ email: 1 }, { unique: true }, indexEnsured);

function getAllowedSchools(callback) {
	db.courses.find({
		'overwrites.types': { $not: { $size: 0 } }
	}, {
		'src.schools': 1,
		_id: 0
	}, function(error, docs) {
		if (error) return callback(error);

		var schoolsList = {};
		docs.forEach(function(doc) {
			var schools = doc && doc.src && doc.src.schools;
			if (!schools) return;

			schools.forEach(function(id) {
				schoolsList[id] = true;
			});
		});
		schoolsList = Object.keys(schoolsList);

		callback(null, schoolsList);
	});
}
db.getAllowedSchools = getAllowedSchools;

function getActiveCourses(callback) {
	db.courses.find({
		'overwrites.types': { $not: { $size: 0 } }
	}, {
		_id: 1
	}, function(error, docs) {
		if (error) return callback(error);

		callback(null, docs.map(function(doc) {
			return doc._id;
		}).sort());
	});
}
db.getActiveCourses = getActiveCourses;

function updateCourseTypes(callback) {
	var typesByCompetenceId = {};
	var typesByCourseId = {};

	common.step([

		function(next) {
			db.courses.find({
				'overwrites.extraAmu': { $exists: true }
			}, {
				'overwrites.extraAmu': 1
			}, next);
		},

		function(courses, next) {
			courses = courses || [];
			courses.forEach(function(course) {
				var id = '' + course._id;
				var types = course.overwrites.extraAmu;
				if (Array.isArray(types)) {
					typesByCourseId[id] = types;
				}
			});

			db.competences.find({ 'overwrites.type': { $exists: true } }, next);
		},

		function(competences, next) {
			competences = competences || [];
			competences.forEach(function(competence) {
				var id = '' + competence._id;
				var types = competence.overwrites.type;
				if (Array.isArray(types)) {
					typesByCompetenceId[id] = typesByCompetenceId[id] || {};
					types.forEach(function(type) {
						typesByCompetenceId[id][type] = true;
					});
				}
			});

			var competenceIds = Object.keys(typesByCompetenceId);
			db.courses.find({
				'src.competences.0': { $in: competenceIds }
				// 'src.competencePrimary': { $in: competenceIds }
			}, {
				'src.competences': 1,
				'src.competencePrimary': 1
			}, next);
		},

		function(primaryCourses, next) {
			/*
			// The following algorithm looks on related competences and adds them
			// to the list. This is currently commented out as this is not the right
			// way to go about it.

			primaryCourses = primaryCourses || [];
			primaryCourses.forEach(function(course) {
				var competenceIds = course.src.competences;
				var competencePrimary = null;
				if (!Array.isArray(competenceIds)) competenceIds = [];

				if ('competencePrimary' in course.src) {
					competencePrimary = course.src.competencePrimary;
				} else {
					competencePrimary = competenceIds[0];
				}

				if (competencePrimary) {
					var types = typesByCompetenceId[competencePrimary];
					if (types) {
						competenceIds.forEach(function(id) {
							typesByCompetenceId[id] = typesByCompetenceId[id] || {};
							Object.keys(types).forEach(function(type) {
								typesByCompetenceId[id][type] = true;
							});
						});
					}
				}
			});
			*/

			Object.keys(typesByCompetenceId).forEach(function(id) {
				typesByCompetenceId[id] = Object.keys(typesByCompetenceId[id]);
			});

			db.courses.update({}, {
				$set: {
					'overwrites.typesCount': 0
				},
				$unset: {
					'overwrites.types': ''
				}
			}, { multi: true }, function(error) {
				if (error) {
					console.error('Could not purge old course types (when re-calculating new course types):', error.stack || error);
				}

				// Ignore error in the process, since we really don't want course types to be lost.
				next();
			});
		},

		function(next) {
			async.eachLimit(Object.keys(typesByCompetenceId), 1, function(competenceId, iteratorNext) {
				db.courses.update({
					'src.competences': competenceId
				}, {
					$addToSet: {
						'overwrites.types': {
							$each: typesByCompetenceId[competenceId]
						}
					}
				}, {
					multi: true
				}, iteratorNext);
			}, next);
		},

		function(next) {
			async.eachLimit(Object.keys(typesByCourseId), 10, function(courseId, iteratorNext) {
				db.courses.update({
					'_id': courseId
				}, {
					$addToSet: {
						'overwrites.types': {
							$each: typesByCourseId[courseId]
						}
					}
				}, iteratorNext);
			}, next);
		},

		function(next) {
			async.eachLimit([1,2,3,4,5], 1, function(size, iteratorNext) {
				db.courses.update({
					'overwrites.types': { $size: size }
				}, {
					$set: {
						'overwrites.typesCount': size
					}
				}, {
					multi: true
				}, iteratorNext);
			}, next);
		},

		function() {
			callback();
		}

	], function(error) {
		callback(error);
	});
}
db.updateCourseTypes = updateCourseTypes;

function encodeId(id) {
	return new Buffer('' + id, 'hex')
		.toString('base64')
		.replace(/={1,2}$/, '')
		.replace(/\//g, '_')
		.replace(/\+/g, '-');
}
db.encodeId = encodeId;

function decodeId(id) {
	id = '' + [id];
	if (!/^[A-Za-z0-9_\-]{16}$/.test(id)) return null;

	return mongojs.ObjectId(
		new Buffer(
			id.replace(/-/g, '+').replace(/_/g, '/'),
			'base64'
		).toString('hex')
	);
}
db.decodeId = decodeId;

module.exports = db;
