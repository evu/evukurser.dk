'use strict';

// Database
var db = require('../db');
var moment = require('moment');
var schools = {};
var lastUpdated = null;

function add(id, schoolName) {
	db.mainSchools.update({ id: id }, { id: id, schoolName: schoolName }, { upsert: true });
}

function getSchools(courseType, callback) {
	if (schools[courseType] && !isOutdated()) {
		callback(null, schools[courseType]);
		return;
	}

	var query = {};

	if (courseType) {
		query.types = courseType;
	}

	db.allowedSchools.find(query).sort({ schoolName:1 }, function(error, docs) {
		if (error) {
			callback(error);
			return;
		}

		callback(null, docs);
		schools[courseType] = docs;
	});

} 

function isOutdated() {
	if (!lastUpdated || !moment(lastUpdated).isValid()) {
		lastUpdated = moment();
		return true;
	} 

	var wasUpdatedToday = lastUpdated.startOf('day').isSame(moment().startOf('day'));
	return (wasUpdatedToday === false);
}

module.exports.add = add;
module.exports.getSchools = getSchools;