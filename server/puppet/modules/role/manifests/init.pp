# Base role for servers
class role {
	include profile::base
}
class role::base inherits role {}

# Base role for servers running evukurser.dk
class role::evukurser inherits role {
	include profile::mongodb
	include profile::nodejs
}
class role::evukurser::base inherits role::evukurser {}

# Development / staging / production roles for evukurser.dk servers
class role::evukurser::development inherits role::evukurser {
	include profile::evukurser::development_box
}
class role::evukurser::staging inherits role::evukurser {
}
class role::evukurser::production inherits role::evukurser {
}
