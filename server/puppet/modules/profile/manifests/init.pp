# Base profile for servers
class profile {
	include apt
}
class profile::base inherits profile {}

# Base profile for servers running evukurser.dk
class profile::evukurser inherits profile {}
class profile::evukurser::base inherits profile::evukurser {}

# Development box for evukurser.dk
class profile::evukurser::development_box inherits profile::evukurser {
	file { '/etc/motd':
		content => "\n----[  EVUKurser.dk Development Box  ]----\n\n",
	}

	$sharedDir = '/vagrant'
	$devDir = '/home/vagrant/dev'

	file { $devDir:
		ensure => directory,
		owner  => 'vagrant',
		group  => 'vagrant',
	}
	file { "$devDir/package.json":
		ensure  => "$sharedDir/package.json",
		owner   => 'vagrant',
		group   => 'vagrant',
		require => File[$devDir],
	}
	file { "$devDir/gulpfile.js":
		ensure  => present,
		source  => "$sharedDir/gulpfile.js",
		owner   => 'vagrant',
		group   => 'vagrant',
		require => File[$devDir],
	}
	file { "$devDir/app":
		ensure  => "$sharedDir/app",
		owner   => 'vagrant',
		group   => 'vagrant',
		require => File[$devDir],
	}
	file { "$devDir/lib":
		ensure  => "$sharedDir/lib",
		owner   => 'vagrant',
		group   => 'vagrant',
		require => File[$devDir],
	}
	file { "$devDir/server":
		ensure  => "$sharedDir/server",
		owner   => 'vagrant',
		group   => 'vagrant',
		require => File[$devDir],
	}
}

# MongoDB setup
class profile::mongodb inherits profile {
	apt::source { 'mongodb':
		location    => 'http://downloads-distro.mongodb.org/repo/ubuntu-upstart',
		release     => 'dist',
		repos       => '10gen',
		key         => '7F0CEB10',
		key_server  => 'keyserver.ubuntu.com',
		include_src => false,
	} ~>
	package { 'mongodb-org':
		ensure => latest,
	} ~>
	service { 'mongod':
		ensure => running,
		enable => true,
	}
}

# NodeJS setup
class profile::nodejs inherits profile {
	apt::source { 'nodesource':
		location    => 'https://deb.nodesource.com/node',
		key         => '68576280',
		key_source  => 'https://deb.nodesource.com/gpgkey/nodesource.gpg.key',
		include_src => true,
	} ~>
	package { 'nodejs':
		ensure => latest,
	} ~>
	exec { 'Install NPM package: gulp':
		command => '/usr/bin/npm install -g gulp'
	}
}
