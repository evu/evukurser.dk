# Site-specific configuration

node 'dev.evukurser.dk'     { include role::evukurser::development }
node 'staging.evukurser.dk' { include role::evukurser::staging     }
node 'beta.evukurser.dk'    { include role::evukurser::production  }
node 'evukurser.dk'         { include role::evukurser::production  }
