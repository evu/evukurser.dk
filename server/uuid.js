/** @module server/uuid */
'use strict';

var crypto = require('crypto');

function encodeBuffer(buffer) {
	return buffer
		.toString('base64')
		.replace(/={1,2}$/, '')
		.replace(/\//g, '_')
		.replace(/\+/g, '-');
}

function uuid() {
	return encodeBuffer(crypto.randomBytes(16));
}

module.exports = uuid;
