/** @module server/nemid */
'use strict';

var querystring = require('querystring');
var request = require('request');

var nemIdServerPort = 30564;

function parseCookies(session, response) {
	var setCookie = response.headers['set-cookie'];
	if (!setCookie) return session;

	if (!Array.isArray(setCookie)) {
		setCookie = [setCookie];
	}

	setCookie.forEach(function(cookie) {
		var item = cookie.split(';')[0].split('=');
		var key = item[0];
		var value = item.slice(1).join('=');
		if (value) {
			session[key] = value;
		} else {
			delete session[key];
		}
	});

	return session;
}

function stringifyCookies(session) {
	return querystring.stringify(session || {}, '; ', '=');
}

function parseJSON(data) {
	try {
		return JSON.parse('' + data) || {};
	} catch (error) {
		return {};
	}
}

function loginParams(callback) {
	request('http://127.0.0.1:' + nemIdServerPort + '/tuexample/proxy/login.jsp', {
		timeout: 2000,
		followAllRedirects: true
	}, function(error, response, body) {
		if (error) return callback(error);
		if (response.statusCode !== 200) return callback(new Error('Got HTTP ' + response.statusCode + ' from NemID server requesting login parameters.'));

		body = parseJSON(body);

		if (typeof(body.parameters) === 'string' && typeof(body.applet_origin) === 'string' && typeof(body.applet_path) === 'string') {
			callback(null, {
				parameters: body.parameters,
				applet: {
					origin: body.applet_origin,
					path: body.applet_path
				},
				session: parseCookies({}, response)
			});
		} else {
			callback(new Error('Did not get proper login parameters from NemID server.'));
		}
	});
}

function login(session, response, callback) {
	request('http://127.0.0.1:' + nemIdServerPort + '/tuexample/proxy/logon.html', {
		timeout: 2000,
		followAllRedirects: true,
		method: 'POST',
		form: {
			response: '' + response
		},
		headers: {
			Cookie: stringifyCookies(session)
		}
	}, function(error, response, body) {
		if (error) return callback(error);
		if (response.statusCode !== 200) return callback(new Error('Got HTTP ' + response.statusCode + ' from NemID server when trying to login.'));

		body = parseJSON(body);
		body.session = parseCookies(session, response);

		callback(null, body);
	});
}

function cprMatch(session, cpr, pid, callback) {
	request('http://127.0.0.1:' + nemIdServerPort + '/tuexample/proxy/pid.html', {
		timeout: 10000,
		followAllRedirects: true,
		method: 'POST',
		form: {
			pidmatch: '' + pid,
			cpr: '' + cpr
		},
		headers: {
			Cookie: stringifyCookies(session)
		}
	}, function(error, response, body) {
		if (error) return callback(error);
		if (response.statusCode !== 200) return callback(new Error('Got HTTP ' + response.statusCode + ' from NemID server when trying to match CPR/PID.'));

		body = parseJSON(body);
		body.session = parseCookies(session, response);

		callback(null, body);
	});
}

function logout(session) {
	request('http://127.0.0.1:' + nemIdServerPort + '/tuexample/proxy/logout.html', {
		timeout: 2000,
		followAllRedirects: true,
		headers: {
			Cookie: stringifyCookies(session)
		}
	}, function(error) {
		if (error) {
			console.log('Failed to logout virtual NemID user:', error.stack || error);
		}
	});
}

exports.loginParams = loginParams;
exports.login = login;
exports.cprMatch = cprMatch;
exports.logout = logout;
