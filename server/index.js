/** @module server */
'use strict';

var clientRoutes = require('./clientRoutes');
var express = require('express');
var config = require('./config');
var path = require('path');
var api = require('./api');

// Run Gulp in the background in development environment.
require('./gulp');

// Setup build dir and create express instance.
var buildDir = path.resolve(__dirname, '../build');
var indexHtmlFile = path.resolve(__dirname, '../app/index.html');
var server = express();

// Express configuration.
server.configure(function() {
	// Do not advertise that we use ExpressJS.
	server.disable('x-powered-by');
	// Log requests.
	//server.use(express.logger(':remote-addr - - [:date] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" ":res[content-type]"'));
	// Compress text assets.
	server.use(express.compress());
	// Serve static files built by gulp.
	express.static.mime.define({ 'text/plain': ['raml'] }); // serving RAML files
	server.use(express.static(buildDir, { maxAge: 60 * 60 * 1000 }));
	// Parse post body.
	server.use(express.json({ limit: 3*1024*1024 }));
	server.use(express.urlencoded());
	// Inject client info in request.
	server.use(function(request, response, next) {
		request.clientInfo = {
			ip: request.client.remoteAddress,
			headers: request.headers
		};
		next();
	});
	// Serve routing, specified below. This includes the client routes defined by app/config/routeMap.js
	server.use(server.router);
	// Otherwise show 404.
	server.use(function(request, response) {
		//response.sendfile(indexHtmlFile);
		response.send(404, 'Not Found\n');
	});
});

// Test if server is running.
server.get('/heartbeat', function(request, response) {
	response.send(200, 'OK\n');
});

// Map all client routes to index.html file.
function serveIndexHtmlFile(request, response) {
	response.sendfile(indexHtmlFile, { maxAge: 60 * 60 * 1000 });
}
clientRoutes.forEach(function(url) {
	server.get(url, serveIndexHtmlFile);
});

function respond(response, defaultSuccessStatus) {
	defaultSuccessStatus = defaultSuccessStatus || 200;

	return function(error, body) {
		var status = defaultSuccessStatus;

		response.req.method = 'GET-no-etag'; // Hack to disable that Express sets ETag header.
		try {
			response.removeHeader('ETag');
		} catch (error2) {}
		try {
			response.setHeader('Cache-Control', 'no-store');
		} catch (error2) {}

		if (error) {
			if (error.status === 401) {
				if (error.code === 'invalid_client') {
					response.setHeader('WWW-Authenticate', 'Basic realm="EVUKurser.dk Client"');
				} else {
					response.setHeader('WWW-Authenticate', 'Bearer realm="EVUKurser.dk"');
				}
			}
			status = error.status || 500;
			body = {
				error: error.code || 'server_error',
				error_description: error.message
			};
		}

		response.send(status, body);
		api.apilog.saveResponse(response.apilogId, status, body, response._headers, function(error) {
			if (error) {
				console.error('Logging API response failed:', error.stack || error);
			}
		});
	};
}

function respond401(response, message) {
	return respond(response)({
		status: 401,
		code: 'invalid_grant',
		message: message
	});
}

function apiLogger(request, response, next) {
	api.apilog.saveRequest(request, function(error, id) {
		if (error) {
			console.error('Logging API request failed:', error.stack || error);
		} else {
			request.apilogId = id;
			response.apilogId = id;
		}

		next();
	});
}

function accessLevel(scope) {
	return function checkAccessLevel(request, response, next) {
		apiLogger(request, response, function() {
			var auth = ('' + [request.headers.authorization]).trim();
			if (!auth) {
				if (scope === 'public') {
					request.authInfo = null;
					next();
					return;
				} else {
					return respond401(response, 'Du skal logge ind for at tilgå denne service.');
				}
			}

			var authMatch = auth.match(/^Bearer\s+([A-Z0-9_\-]{22})$/i);
			var accessToken = authMatch ? (authMatch[1] || '') : '';

			api.oauth2.provider.lookup(accessToken, function(error, auth) {
				if (error) {
					console.error('Invalid OAuth2 token:', error.stack || error);
					return respond401(response, 'Ugyldig token.');
				}

				api.apilog.saveAuth(response.apilogId, auth, function(error) {
					if (error) {
						console.error('Logging API auth failed:', error.stack || error);
					}
				});

				if (!(scope === 'public' || auth.scope[scope])) {
					return respond401(response, 'Ugyldig brugerniveau – ' + scope + ' niveau påkrævet.');
				}

				request.authInfo = auth;
				next();
			});
		});
	};
}

var userAPI = accessLevel('user');
var adminAPI = accessLevel('admin');
var publicAPI = apiLogger;
var publicWithAuthAPI = accessLevel('public');

server.get('/api/v1/status', publicAPI, function(request, response) {
	api.wsStatus.get(respond(response));
});

server.get('/api/v1/options', publicAPI, function(request, response) {
	api.options.get(request.query, respond(response));
});

server.get('/api/v1/options/region', publicAPI, function(request, response) {
	api.options.region.get(request.query, respond(response));
});

// TODO: Remove API, it is called jobarea now.
server.get('/api/v1/options/theme', publicAPI, function(request, response) {
	api.options.jobarea.get(request.query, respond(response));
});

server.get('/api/v1/options/jobarea', publicAPI, function(request, response) {
	api.options.jobarea.get(request.query, respond(response));
});

server.get('/api/v1/options/sub_jobarea', publicAPI, function(request, response) {
	api.options.sub_jobarea.get(request.query, respond(response));
});

server.get('/api/v1/options/theme', publicAPI, function(request, response) {
	api.options.theme.get(request.query, respond(response));
});

server.get('/api/v1/options/sub_theme', publicAPI, function(request, response) {
	api.options.sub_theme.get(request.query, respond(response));
});

server.get('/api/v1/options/type', publicAPI, function(request, response) {
	api.options.type.get(request.query, respond(response));
});

server.get('/api/v1/options/sort', publicAPI, function(request, response) {
	api.options.sort.get(request.query, respond(response));
});

server.get('/api/v1/options/school', publicAPI, function(request, response) {
	api.options.school.get(request.query, respond(response));
});

server.get('/api/v1/courses', publicWithAuthAPI, function(request, response) {
	api.courses.get(request.authInfo, request.query, respond(response));
});

server.get('/api/v1/courses/:id', publicWithAuthAPI, function(request, response) {
	api.course.get(request.authInfo, request.params.id, request.query, respond(response));
});

server.get('/api/v1/courses/:courseId/rating/:userId', userAPI, function(request, response) {
	var reply = respond(response);
	if (!(request.params.userId === request.authInfo.user.id || request.authInfo.scope.admin)) {
		return reply({
			status: 403,
			code: 'forbidden',
			message: 'Du kan kun se din egen rating.'
		});
	}

	api.courseRating.get(request.params.courseId, request.params.userId, reply);
});

server.put('/api/v1/courses/:courseId/rating/:userId', userAPI, function(request, response) {
	var reply = respond(response);
	if (!(request.params.userId === request.authInfo.user.id || request.authInfo.scope.admin)) {
		return reply({
			status: 403,
			code: 'forbidden',
			message: 'Du kan kun opdatere din egen rating.'
		});
	}

	api.courseRating.update(request.params.courseId, request.params.userId, request.body, reply);
});

server.del('/api/v1/courses/:courseId/rating/:userId', userAPI, function(request, response) {
	var reply = respond(response);
	if (!(request.params.userId === request.authInfo.user.id || request.authInfo.scope.admin)) {
		return reply({
			status: 403,
			code: 'forbidden',
			message: 'Du kan kun slette din egen rating.'
		});
	}

	api.courseRating.remove(request.params.courseId, request.params.userId, reply);
});

server.post('/api/v1/auth/oauth2/token', publicAPI, function(request, response) {
	api.oauth2.token.create(request.clientInfo, request.body, respond(response));
});

server.get('/api/v1/auth/oauth2/debug', userAPI, function(request, response) {
	var auth = request.authInfo;
	response.send({
		access_token: auth.token,
		token_type: 'bearer',
		expires_in: auth.expires,
		scope: Object.keys(auth.scope).sort().join(' '),
		client: {
			id: auth.client.id
		},
		user: auth.user
	});
});

server.post('/api/v1/users', publicAPI, function(request, response) {
	api.users.create(request.clientInfo, request.body, respond(response, 201));
});

server.get('/api/v1/users', adminAPI, function(request, response) {
	api.users.get(request.query, respond(response));
});

server.get('/api/v1/users/:id', userAPI, function(request, response) {
	var reply = respond(response);
	if (!(request.params.id === request.authInfo.user.id || request.authInfo.scope.admin)) {
		return reply({
			status: 403,
			code: 'forbidden',
			message: 'Du kan kun se din egne profiloplysninger.'
		});
	}

	api.user.get(request.params.id, reply);
});

server.put('/api/v1/users/:id', userAPI, function(request, response) {
	var reply = respond(response);
	if (!(request.params.id === request.authInfo.user.id || request.authInfo.scope.admin)) {
		return reply({
			status: 403,
			code: 'forbidden',
			message: 'Du kan kun opdatere dine egne profiloplysninger.'
		});
	}

	api.user.update(request.authInfo, request.clientInfo, request.params.id, request.body, reply);
});

server.post('/api/v1/users/:id/cpr', userAPI, function(request, response) {
	var reply = respond(response);
	if (request.params.id !== request.authInfo.user.id) {
		return reply({
			status: 403,
			code: 'forbidden',
			message: 'Du kan kun validere dit eget CPR-nummer.'
		});
	}

	api.cpr.create(request.authInfo, request.params.id, request.body, reply);
});

if (process.env.NODE_ENV !== 'production') {
	// API operation for setting user CPR in test environment.
	server.put('/api/v1/users/:id/cpr/test', userAPI, function(request, response) {
		var reply = respond(response);
		if (request.params.id !== request.authInfo.user.id) {
			return reply({
				status: 403,
				code: 'forbidden',
				message: 'Du kan kun sætte dit eget CPR-nummer (under test).'
			});
		}

		api.cpr.updateInTest(request.authInfo, request.params.id, request.body, reply);
	});
}

server.post('/api/v1/users/:id/cpr/:token', userAPI, function(request, response) {
	var reply = respond(response);
	if (request.params.id !== request.authInfo.user.id) {
		return reply({
			status: 403,
			code: 'forbidden',
			message: 'Du kan kun validere dit eget CPR-nummer.'
		});
	}

	api.cpr.validate(request.authInfo, request.params.id, request.params.token, request.body, reply);
});

server.del('/api/v1/users/:id/cpr', userAPI, function(request, response) {
	var reply = respond(response);
	if (!(request.params.id === request.authInfo.user.id || request.authInfo.scope.admin)) {
		return reply({
			status: 403,
			code: 'forbidden',
			message: 'Du kan kun slette dit eget CPR-nummer.'
		});
	}

	api.cpr.remove(request.authInfo, request.params.id, reply);
});

server.get('/api/v1/users/:id/classes/completed', userAPI, function(request, response) {
	var reply = respond(response);
	if (!(request.params.id === request.authInfo.user.id || request.authInfo.scope.admin)) {
		return reply({
			status: 403,
			code: 'forbidden',
			message: 'Du kan kun se dine egne gennemførte kurser.'
		});
	}

	api.completedClasses.list(request.params.id, reply);
});

server.get('/api/v1/users/:id/wishlist', userAPI, function(request, response) {
	var reply = respond(response);
	if (!(request.params.id === request.authInfo.user.id || request.authInfo.scope.admin)) {
		return reply({
			status: 403,
			code: 'forbidden',
			message: 'Du kan kun se din egen ønskeliste.'
		});
	}

	api.wishlist.list(request.params.id, request.query, reply);
});

server.post('/api/v1/users/:id/wishlist', userAPI, function(request, response) {
	var reply = respond(response, 201);
	if (!(request.params.id === request.authInfo.user.id || request.authInfo.scope.admin)) {
		return reply({
			status: 403,
			code: 'forbidden',
			message: 'Du kan kun opdatere din egen ønskeliste.'
		});
	}

	api.wishlist.addItem(request.params.id, request.body.id, reply);
});

server.del('/api/v1/users/:id/wishlist/:courseId', userAPI, function(request, response) {
	var reply = respond(response);
	if (!(request.params.id === request.authInfo.user.id || request.authInfo.scope.admin)) {
		return reply({
			status: 403,
			code: 'forbidden',
			message: 'Du kan kun opdatere din egen ønskeliste.'
		});
	}

	api.wishlist.removeItem(request.params.id, request.params.courseId, reply);
});

server.get('/api/v1/users/:id/wishlist/:courseId', userAPI, function(request, response) {
	var reply = respond(response);
	if (!(request.params.id === request.authInfo.user.id || request.authInfo.scope.admin)) {
		return reply({
			status: 403,
			code: 'forbidden',
			message: 'Du kan kun se din egen ønskeliste.'
		});
	}

	api.wishlist.getItem(request.params.id, request.params.courseId, reply);
});

server.post('/api/v1/users/reset-password', publicAPI, function(request, response) {
	api.user.startPasswordReset(request.clientInfo, request.body, respond(response, 201));
});

server.post('/api/v1/users/reset-password/:code', publicAPI, function(request, response) {
	api.user.resetPassword(request.clientInfo, {
		code: request.params.code,
		password: request.body.password
	}, respond(response));
});

server.post('/api/v1/contact', publicAPI, function(request, response) {
	api.contact(request.clientInfo, request.body, respond(response, 201));
});

server.get('/verificer-email-kode/:code', publicAPI, function(request, response) {
	api.user.verifyEmail(request.params.code, function(error, newUser) {
		if (error) {
			console.error('Could not verify e-mail:', error);
			return response.send(200, 'Der opstod en fejl ved at verificere e-mail-adressen.\n');
		}

		response.redirect(303, newUser ? '/verificer-email/ny-bruger' : '/verificer-email/ny-email');
	});
});

server.put('/api/v1/backend/data/starred.csv', adminAPI, function(request, response) {
	if (Object.keys(request.body).length > 0) {
		return respond(response)({
			status: 400,
			code: 'invalid_request',
			message: 'Ugyldig format, upload fil i CSV-format.'
		});
	}
	api.csv.starred.upload(request, respond(response));
});

server.put('/api/v1/backend/data/links.csv', adminAPI, function(request, response) {
	if (Object.keys(request.body).length > 0) {
		return respond(response)({
			status: 400,
			code: 'invalid_request',
			message: 'Ugyldig format, upload fil i CSV-format.'
		});
	}
	api.csv.links.upload(request, respond(response));
});

server.put('/api/v1/backend/data/themes.csv', adminAPI, function(request, response) {
	if (Object.keys(request.body).length > 0) {
		return respond(response)({
			status: 400,
			code: 'invalid_request',
			message: 'Ugyldig format, upload fil i CSV-format.'
		});
	}
	api.csv.themes.upload(request, respond(response));
});

server.put('/api/v1/backend/data/jobareas.csv', adminAPI, function(request, response) {
	if (Object.keys(request.body).length > 0) {
		return respond(response)({
			status: 400,
			code: 'invalid_request',
			message: 'Ugyldig format, upload fil i CSV-format.'
		});
	}
	api.csv.jobareas.upload(request, respond(response));
});

server.get('/api/v1/backend/data/courselist.csv', publicAPI, function(request, response) {
	api.csv.download.simple_courses_list(request.query.type, response);
});

server.get('/api/v1/backend/data/schoolclasslist.csv', publicAPI, function(request, response) {
	api.csv.download.courseAndSchoolExtract(request.query.type, response);
});

server.get('/api/v1/backend/data/mainschools.csv', publicAPI, function(request, response) {
	api.csv.download.mainSchools(request.query.type, response);
});

server.get('/api/v1/backend/data/schools.csv', publicAPI, function(request, response) {
	api.csv.download.allSchools(request.query.type, response);
});

server.put('/api/v1/backend/data/courses.csv', adminAPI, function(request, response) {
	if (Object.keys(request.body).length > 0) {
		return respond(response)({
			status: 400,
			code: 'invalid_request',
			message: 'Ugyldig format, upload fil i CSV-format.'
		});
	}
	api.csv.courses.upload(request, respond(response));
});

server.put('/api/v1/backend/data/mainSchools.csv', adminAPI, function(request, response) {
	if (Object.keys(request.body).length > 0) {
		return respond(response)({
			status: 400,
			code: 'invalid_request',
			message: 'Ugyldig format, upload fil i CSV-format.'
		});
	}
	api.csv.mainSchools.upload(request, respond(response));
});

server.put('/api/v1/backend/data/apprenticeships.csv', adminAPI, function(request, response) {
	if (Object.keys(request.body).length > 0) {
		return respond(response)({
			status: 400,
			code: 'invalid_request',
			message: 'Ugyldig format, upload fil i CSV-format.'
		});
	}
	api.csv.apprenticeships.upload(request, respond(response));
});

server.put('/api/v1/backend/data/certificates.csv', adminAPI, function(request, response) {
	if (Object.keys(request.body).length > 0) {
		return respond(response)({
			status: 400,
			code: 'invalid_request',
			message: 'Ugyldig format, upload fil i CSV-format.'
		});
	}
	api.csv.certificates.upload(request, respond(response));
});

server.put('/api/v1/backend/data/campaigns.csv', adminAPI, function(request, response) {
	if (Object.keys(request.body).length > 0) {
		return respond(response)({
			status: 400,
			code: 'invalid_request',
			message: 'Ugyldig format, upload fil i CSV-format.'
		});
	}
	api.csv.campaigns.upload(request, respond(response));
});

server.put('/api/v1/backend/data/competences.csv', adminAPI, function(request, response) {
	if (Object.keys(request.body).length > 0) {
		return respond(response)({
			status: 400,
			code: 'invalid_request',
			message: 'Ugyldig format, upload fil i CSV-format.'
		});
	}
	api.csv.competences.upload(request, respond(response));
});

server.put('/api/v1/backend/data/extraamu.csv', adminAPI, function(request, response) {
	if (Object.keys(request.body).length > 0) {
		return respond(response)({
			status: 400,
			code: 'invalid_request',
			message: 'Ugyldig format, upload fil i CSV-format.'
		});
	}
	api.csv.extraamu.upload(request, respond(response));
});

server.post('/api/v1/news', adminAPI, function(request, response) {
	api.news.create(request.clientInfo, request.body, respond(response, 201));
});

server.put('/api/v1/news/:id', adminAPI, function(request, response) {
	api.news.update(request.clientInfo, request.params.id, request.body, respond(response));
});

server.del('/api/v1/news/:id', adminAPI, function(request, response) {
	api.news.remove(request.params.id, respond(response));
});

server.get('/api/v1/news', publicAPI, function(request, response) {
	api.news.list(request.query, respond(response));
});

server.get('/api/v1/news/:id', publicAPI, function(request, response) {
	api.news.get(request.params.id, respond(response));
});


// Start server on specified/default port.
// Can be configured in local config file or by setting environment variable PORT. Default is 3000.
server.listen(config.port, config.bindip);
