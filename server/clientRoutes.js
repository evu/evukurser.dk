/** @module server/clientRoutes */
'use strict';

var routeMap = require('../app/config/routeMap');

function angularUrltoExpressUrl(url) {
	// Right now it just replaces all regular expressions in URLs in ui-route provider to splat arguments in Express (ie. '/:options?*')
	return ('' + [url]).replace(/\{(.+?)\}/g, function(_, regex) {
		var name = regex.match(/^[\w\-]+/);
		name = name[0] || 'optional';
		return '/:' + name + '?*';
	});
}

var resolvedAllUrls = false;

while (!resolvedAllUrls) {
	resolvedAllUrls = true;
	for (var name in routeMap) {
		var item = routeMap[name];
		var url = angularUrltoExpressUrl(item.url);
		if (!item.fullUrl) {
			resolvedAllUrls = false;
			if (item.parent) {
				var parentItem = routeMap[item.parent];
				if (parentItem.fullUrl) {
					item.fullUrl = parentItem.fullUrl + url;
				}
			} else {
				item.fullUrl = url;
			}
		}
	}
}

var urls = {};
for (var name in routeMap) {
	urls[routeMap[name].fullUrl] = true;
}

module.exports = Object.keys(urls).sort();
