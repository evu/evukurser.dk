/** @module server/webservices/courses/competences */
'use strict';

var normalize = require('./normalize');
var xml2json = require('./xml2json');
var through2 = require('through2');
var rtf2md = require('./rtf2md');
var common = require('common');

function competencesStream(institutionsByCompetenceId) {
	return xml2json({
		path: ['CompetenceDescriptionsExtented', 'CompetenceDescriptionExtented', 'CompetenceDescriptionExtented'],
		transform: function(competence) {
			competence = {
				_id: normalize.text(competence.COESAPurpose),
				version: normalize.integer(competence.Version),
				committee: {
					_id: normalize.text(competence.CommitteeNumber)
				},
				title: {
					text: normalize.text(competence.Title),
					short: normalize.text(competence.ShortTitle)
				},
				start: normalize.date(competence.StartDate),
				end: normalize.date(competence.EndDate),
				jobDefinition: normalize.text(competence.DefinitionOfJob),
				workingArea: normalize.text(competence.TypicalWorkingArea),
				employeeDescription: normalize.text(competence.DescriptionOfEmployee),
				workOrganization: normalize.text(competence.OrganizationOfWork),
				errors: [],
				institutions: []
			};
			if (institutionsByCompetenceId) {
				competence.institutions = institutionsByCompetenceId[competence._id];
			}
			return competence;
		}
	});
}

function competencesRtfConversion() {
	return through2.obj(function(obj, encoding, callback) {
		var self = this;

		if (!obj) {
			self.push(null);
			callback();
			return;
		}

		common.step([

			function(next) {
				var jobDefinitionDone = next.parallel();
				rtf2md(obj.jobDefinition, function(error, md) {
					if (error) {
						obj.errors.push('Could not convert RTF for .jobDefinition: ' + (error.message || error));
					}
					obj.jobDefinition = md || null;
					jobDefinitionDone();
				});

				var workingAreaDone = next.parallel();
				rtf2md(obj.workingArea, function(error, md) {
					if (error) {
						obj.errors.push('Could not convert RTF for .workingArea: ' + (error.message || error));
					}
					obj.workingArea = md || null;
					workingAreaDone();
				});

				var employeeDescriptionDone = next.parallel();
				rtf2md(obj.employeeDescription, function(error, md) {
					if (error) {
						obj.errors.push('Could not convert RTF for .employeeDescription: ' + (error.message || error));
					}
					obj.employeeDescription = md || null;
					employeeDescriptionDone();
				});

				var workOrganizationDone = next.parallel();
				rtf2md(obj.workOrganization, function(error, md) {
					if (error) {
						obj.errors.push('Could not convert RTF for .workOrganization: ' + (error.message || error));
					}
					obj.workOrganization = md || null;
					workOrganizationDone();
				});
			},

			function() {
				self.push(obj);
				callback();
			}

		]);
	});
}

exports.stream = competencesStream;
exports.rtfConversion = competencesRtfConversion;
