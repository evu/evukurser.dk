/** @module server/webservices/courses/udda */
'use strict';

var request = require('request');
var config = require('../../config');
var pkg = require('../../../package.json');

var services = {
	institutionsXml: 'GetInstitutions',
	committeesXml: 'GetContinuingTrainingCommittees',
	competenceAllowancesXml: 'GetAllowance_CompetenceDescriptions',
	courseAllowancesXml: 'GetAllowance_Courses',
	courseCompetencesXml: 'GetCompetenceDescriptions_Courses',
	courseTariffsXml: 'GetCoursesTariff',
	relatedCoursesXml: 'GetRelatedCourses',
	// turnoverXml: 'GetTurnover',
	competencesXml: 'GetCompetenceDescriptionExtented',
	coursesXml: 'GetCoursesExtented'
};

function uddaXml(operationName) {
	var options = {
		uri: 'http://www.uddannelsesadministration.dk/webservice/2014/01/01/EfteruddannelseDK.asmx/' + operationName + '?Loginname=' + config.ws.uddannelsesadministration.loginname + '&Password=' + config.ws.uddannelsesadministration.password + '&ValidDate=&CommitteeNumber=&COESAPurpose=&SubjectCode=&ValidYear=&Version=1&Level=&InstitutionNumber=',
		method: 'GET',
		strictSSL: true,
		headers: {
			'User-Agent': pkg.name + '/' + pkg.version,
			'Accept': 'application/xml,text/xml;q=0.9',
			'Accept-Encoding': 'none',
			'Accept-Charset': 'UTF-8'
		},
		timeout: 10 * 60 * 1000
	};

	if (!process.env.CRON) console.log('Fetching UddannelsesAdministration/' + operationName);

	var response = request(options);
	return response;
}

Object.keys(services).forEach(function(name) {
	var operationName = services[name];
	exports[name] = function uddaFetchXml() {
		return uddaXml(operationName);
	};
});

function download() {
	var fs = require('fs');
	var async = require('async');

	async.eachLimit(Object.keys(services), 1, function(name, iteratorNext) {
		var filename = services[name] + '.xml';
		var fetch = exports[name];

		console.log(filename);
		fetch().on('error', iteratorNext).on('end', iteratorNext).pipe(
			fs.createWriteStream(__dirname + '/../../testdata/' + filename)
		);
	}, function(error) {
		if (error) {
			console.log('ERROR:', error.stack || error);
		} else {
			console.log('--END--');
		}
	});
}
exports.download = download;

//download();
