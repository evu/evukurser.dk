/** @module server/webservices/courses/xml2jsonNew */
'use strict';

var through2 = require('through2');
var dextend = require('dextend');
var expat = require('node-expat');
var util = require('util');

function defaultObjectEmit(obj) {
	/* jshint -W040 */
	this.push(obj);
	/* jshint +W040 */
}

function xml2json(options) {
	options = options || {};
	var forwardNS = options.ns || {};
	var reverseNS = {};
	var key;
	for (key in forwardNS) {
		reverseNS[forwardNS[key]] = key;
	}

	var objectMapIndex = 0;
	var objectMap = null;
	if (typeof(options.map) === 'function') {
		objectMap = options.map;
	}

	var objectEmit = defaultObjectEmit;
	if (typeof(options.emit) === 'function') {
		objectEmit = options.emit;
	}

	var base = options.base;
	if (base) {
		if (base[0] !== '/') {
			base = '/' + base;
		}
	} else {
		base = '';
	}

	var collectors = [];
	var collect = options.collect || {};
	for (key in collect) {
		var value = collect[key];
		if (key[0] !== '/') {
			key = '/' + key;
		}

		var collector = {
			value: {},
			match: base + key
		};
		if (value !== '.') {
			collector.transform = value;
		}
		collectors.push(collector);
	}

	var parser = new expat.Parser('UTF-8');
	var objects = [];
	var errors = [];
	var path = [];
	var nsMaps = [];
	var text = '';

	function allocateNS(key, url) {
		var allocatedKey = reverseNS[url];
		if (allocatedKey) return allocatedKey;

		if (!forwardNS[key]) {
			forwardNS[key] = url;
			reverseNS[url] = key;
			return key;
		}

		if (/\d$/.test(key)) key += '-';

		var index = 1;
		while (forwardNS[key + index]) index++;
		key += index;

		forwardNS[key] = url;
		reverseNS[url] = key;
		return key;
	}

	function findNS(name) {
		return nsMaps[nsMaps.length - 1][name];
	}

	function resolveName(name) {
		var nsNameMatch = name.match(/^(.+):/);
		var revName;
		var localName;
		if (nsNameMatch) {
			revName = findNS(nsNameMatch[1]);
			localName = name.substring(nsNameMatch[0].length);
		} else {
			revName = findNS('');
			localName = name;
		}
		return (revName || '#') + ':' + localName;
	}

	function parseText() {
		text = text.trim();
		if (!text) return;

		for (var i = 0; i < collectors.length; i++) {
			var collector = collectors[i];
			if (collector.active) {
				var latestIndex = collector.objects.length - 1;
				var latestObject = collector.objects[latestIndex];
				if (Array.isArray(latestObject)) {
					latestObject.push(text);
				} else if (typeof(latestObject) === 'object' && Object.keys(latestObject).length > 0) {
					latestObject.$text = [latestObject.$text] + text;
				} else {
					var latestName = collector.names[latestIndex];
					var previousObject = collector.objects[latestIndex - 1];
					if (previousObject) {
						previousObject[latestName] = text;
					}
					collector.objects[latestIndex] = text;
				}
			}
		}

		text = '';
	}

	parser.on('startElement', function(name, attrs) {
		parseText();

		var nsMap = nsMaps[nsMaps.length - 1];
		var nsMapCopied = false;

		for (var key in attrs) {
			var revName = reverseNS[attrs[key]];

			if (key === 'xmlns') {
				if (!revName) {
					revName = allocateNS('default', attrs[key]);
				}
				if (!nsMapCopied) {
					nsMap = util._extend({}, nsMap);
					nsMapCopied = true;
				}
				nsMap[''] = revName;
			} else {
				var xmlnsMatch = key.match(/^xmlns:(.+)$/);
				if (xmlnsMatch) {
					if (!revName) {
						revName = allocateNS(xmlnsMatch[1], attrs[key]);
					}
					if (!nsMapCopied) {
						nsMap = util._extend({}, nsMap);
						nsMapCopied = true;
					}
					nsMap[xmlnsMatch[1]] = revName;
				}
			}
		}
		nsMaps.push(nsMap || {});

		var fullName = resolveName(name);
		var localName = fullName.replace(/^.+:/, '');
		path.push('/' + fullName);
		var fullPath = path.join('');

		name = localName;

		for (var i = 0; i < collectors.length; i++) {
			var collector = collectors[i];
			if (collector.active) {
				var latestIndex = collector.objects.length - 1;
				var latestObject = collector.objects[latestIndex];
				// var latestName = collector.names[latestIndex];
				var newObject = {};
				// if (Array.isArray(latestObject) || typeof(latestObject) === 'object') {
				// 	if (!Array.isArray(latestObject)) {
				// 		collector.objects[latestIndex - 1][latestName] = collector.objects[latestIndex] = latestObject = [latestObject];
				// 	}
				// 	latestObject.push(newObject);
				// } else {

				// if (typeof(latestObject) === 'string') {
				// 	if (latestObject === '') {
				// 		latestObject = {};
				// 	} else {
				// 		latestObject = { $text: latestObject };
				// 	}
				// 	collector.objects[latestIndex - 1][latestName] = collector.objects[latestIndex] = latestObject;
				// }
				if (latestObject[name] !== undefined) {
					if (!Array.isArray(latestObject[name])) {
						latestObject[name] = [latestObject[name]];
					}
					latestObject[name].push(newObject);
				} else {
					latestObject[name] = newObject;
				}
				collector.objects.push(newObject);
				collector.names.push(name);
			} else if (collector.match === fullPath) {
				collector.active = true;
				collector.objects = [{}];
				collector.names = [''];
				collector.endIndex = path.length - 1;
			}
		}
	});

	parser.on('endElement', function() {
		parseText();
		path.pop();
		nsMaps.pop();
		var i, j;

		for (i = 0; i < collectors.length; i++) {
			var collector = collectors[i];
			if (collector.endIndex === path.length) {

				var value = collector.objects[0] || {};

				if (collector.transform) {
					var transformPath = collector.transform.split('.');
					for (j = transformPath.length - 1; j >= 0; j--) {
						var key = transformPath[j];
						var newValue = {};
						newValue[key] = value;
						value = newValue;
					}
				}

				collector.value = value;
				collector.active = false;
				collector.endIndex = null;
				collector.objects = null;
				collector.names = null;

				if (!collector.transform) {
					for (j = 0; j < collectors.length; j++) {
						if (i === j) continue;
						value = dextend(value, collectors[j].value);
					}
					if (objectMap) {
						value = objectMap(value, objectMapIndex);
					}
					objects.push(value);
					objectMapIndex++;
				}

			}
			if (collector.active) {
				collector.objects.pop();
				collector.names.pop();
			}
		}
	});

	parser.on('text', function(textChunk) {
		text += textChunk;
	});

	parser.on('error', function(error) {
		errors.push(error);
	});

	return through2.obj(function(buffer, encoding, done) {
		parser.write(new Buffer(buffer, encoding));
		var obj;
		/* jshint -W084 */
		while (obj = objects.shift()) objectEmit.call(this, obj);
		while (obj = errors.shift()) this.emit('error', obj);
		/* jshint +W084 */
		done();
	}, function(done) {
		parser.end();
		var obj;
		/* jshint -W084 */
		while (obj = objects.shift()) objectEmit.call(this, obj);
		while (obj = errors.shift()) this.emit('error', obj);
		/* jshint +W084 */
		done();
	});
}

module.exports = xml2json;

// require('fs').createReadStream(__dirname + '/../test.xml').pipe(xml2json({
// 	ns: {
// 		'w005': 'http://uas.uni-c.dk/easy-a/webservices/schemas/',
// 		'uvm': 'http://rep.oio.dk/uvm.dk/xml/schemas/2004/12/03/'
// 	},
// 	base: 'w005:Result/w005:W005ResultatSamling/w005:SkoleAktivitetsUdbud',
// 	collect: {
// 		'uvm:InstitutionIdentifier': 'InstitutionIdentifier',
// 		'w005:AktivitetUdbud': '.'
// 	}
// })).pipe(through2.obj(function(obj, _, done) {
// 	var FagUdbud = obj.FagUdbud;
// 	delete obj.FagUdbud;

// 	if (!FagUdbud) return done();

// 	if (!Array.isArray(FagUdbud)) {
// 		FagUdbud = [FagUdbud];
// 	}

// 	for (var i = 0; i < FagUdbud.length; i++) {
// 		this.push(util._extend(FagUdbud[i], obj));
// 	}

// 	done();
// })).pipe(through2.obj(function(obj, _, done) {
// 	// console.log(Array.isArray(obj['w005:FagUdbud']));
// 	if (!obj.SkolefagPeriode) {
// 		console.log(util.inspect(obj, { colors: true, depth: 5 }) + '\n');
// 	}
// 	// console.log(
// 	// 	util.inspect(obj.SkolefagPeriode.Lokation, { colors: true, depth: 5 }) + '\n' +
// 	// 	util.inspect(obj.UndervisningOplysninger.Lokation, { colors: true, depth: 5 }) + '\n' +
// 	// 	util.inspect(obj.UndervisningOplysninger.PostNummerKode, { colors: true, depth: 5 }) + '\n');
// 	done();
// }));
