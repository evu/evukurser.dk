/** @module server/webservices/courses/collector */
'use strict';

var through2 = require('through2');

function collecter(callback) {
	var list = [];
	return through2.obj(function(obj, encoding, done) {
		list.push(obj);
		done();
	}, function(done) {
		done();
		callback(null, list);
	});
}

module.exports = collecter;
