/** @module server/webservices/courses */
'use strict';

var competenceAllowances = require('./competenceAllowances');
var courseCompetences = require('./courseCompetences');
var courseTypesIndex = require('./courseTypesIndex');
var courseAllowances = require('./courseAllowances');
var relatedCourses = require('./relatedCourses');
var courseTariffs = require('./courseTariffs');
var institutions = require('./institutions');
var competences = require('./competences');
var committees = require('./committees');
var courses = require('./courses');
var mapper = require('./mapper');
var common = require('common');
var udda = require('./udda');

function updateCourses(db, callback) {
	common.step([

		function(next) {
			if (!process.env.CRON) console.log('Downloading competence allowances.');
			udda.competenceAllowancesXml().pipe(competenceAllowances.stream()).pipe(mapper({ keys: {'competence._id':'all', 'institution._id':'all'} }, next));
		},

		function(competenceAllowancesIndex, next) {
			if (!process.env.CRON) console.log('Downloading course meta data tables.');
			udda.institutionsXml()
				.pipe(institutions.stream(competenceAllowancesIndex['institution._id']))
				.pipe(mapper({ keys: {_id:'first', legal_name:'all'} }, next.parallel()));
			udda.committeesXml()
				.pipe(committees.stream())
				.pipe(mapper({ keys: {_id:'first'} }, next.parallel()));
			udda.courseAllowancesXml()
				.pipe(courseAllowances.stream())
				.pipe(mapper({ keys: {'course._id':'all', 'institution._id':'all'} }, next.parallel()));
			udda.courseCompetencesXml()
				.pipe(courseCompetences.stream())
				.pipe(mapper({ keys: {'course._id':'all', 'competence._id':'all'} }, next.parallel()));
			udda.courseTariffsXml()
				.pipe(courseTariffs.stream())
				.pipe(mapper({ keys: {'course._id':{max:'year'}} }, next.parallel()));
			udda.relatedCoursesXml()
				.pipe(relatedCourses.stream())
				.pipe(relatedCourses.mapById(next.parallel()));
			udda.competencesXml()
				.pipe(competences.stream(competenceAllowancesIndex['competence._id']))
				.pipe(competences.rtfConversion()).pipe(mapper({ keys: {_id:'first'} }, next.parallel()));
		},

		function(indexes, next) {
			if (!process.env.CRON) console.log('Downloading and processing courses (might take 10 minutes or so).');

			var institutionsIndex      = indexes[0];
			var committeesIndex        = indexes[1];
			var courseAllowancesIndex  = indexes[2];
			var courseCompetencesIndex = indexes[3];
			var courseTariffsIndex     = indexes[4];
			var relatedCoursesIndex    = indexes[5];
			var competencesIndex       = indexes[6];

			var index = {
				institutionsById: institutionsIndex._id,
				institutionsByName: institutionsIndex.legal_name,
				committeesById: committeesIndex._id,
				allowancesByCourseId: courseAllowancesIndex['course._id'],
				allowancesByInstitutionId: courseAllowancesIndex['institution._id'],
				coursesByCompetenceId: courseCompetencesIndex['competence._id'],
				competencesByCourseId: courseCompetencesIndex['course._id'],
				tariffsByCourseId: courseTariffsIndex['course._id'],
				relatedCousesById: relatedCoursesIndex._id,
				competencesById: competencesIndex._id,
				coursesByType: courseTypesIndex['type._id'],
				typesByCourseId: courseTypesIndex['course._id']
			};

			for (var id in index.competencesById) {
				index.competencesById[id].courses = index.coursesByCompetenceId[id] || [];
			}

			udda.coursesXml()
				.pipe(courses.stream())
				.pipe(courses.rtfConversion())
				.pipe(courses.normalizerStream(index))
				.pipe(courses.insertToDatabaseStream(db, next));
		},

		function() {
			callback();
		}

	], function(error) {
		callback(error);
	});
}

module.exports = updateCourses;
