/** @module server/webservices/courses/relatedCourses */
'use strict';

var xml2json = require('./xml2json');
var normalize = require('./normalize');
var through2 = require('through2');

function relatedCoursesStream() {
	return xml2json({
		path: ['RelatedCourses', 'RelatedCourse', 'RelatedCourse'],
		transform: function(relatedCourse) {
			return {
				from: {
					_id: normalize.text(relatedCourse.FromSubjectCode),
					level: normalize.text(relatedCourse.FromLevel)
				},
				to: {
					_id: normalize.text(relatedCourse.ToSubjectCode),
					level: normalize.text(relatedCourse.ToLevel)
				}
			};
		}
	});
}

function addRelatedCourseMapping(mapping, id, to) {
	var list = mapping[id] = mapping[id] || [];

	for (var i = 0; i < list.length; i++) {
		var element = list[i];
		if (element._id === to._id && element.level === to.level) return;
	}

	list.push(to);
}

function relatedCoursesMapById(callback) {
	var mapping = {};

	return through2.obj(function(relatedCourse, encoding, done) {
		addRelatedCourseMapping(mapping, relatedCourse.from._id, relatedCourse.to);
		addRelatedCourseMapping(mapping, relatedCourse.to._id, relatedCourse.from);
		done();
	}, function(done) {
		callback(null, { _id: mapping });
		done();
	});
}

exports.stream = relatedCoursesStream;
exports.mapById = relatedCoursesMapById;
