/** @module server/webservices/courses/turnover */
'use strict';

var xml2json = require('./xml2json');
var normalize = require('./normalize');

function turnoverStream() {
	return xml2json({
		path: ['Turnover', 'TurnoverElement', 'TurnoverElement'],
		transform: function(turnover) {
			return {
				course: {
					_id: normalize.text(turnover.SubjectCode),
					level: normalize.text(turnover.Level)
				},
				institution: {
					_id: normalize.text(turnover.InstitutionNumber)
				},
				month: normalize.text(turnover.Year) + '-' + normalize.text(turnover.Month),
				turnover: normalize.float(turnover.Turnover)
			};
		}
	});
}

exports.stream = turnoverStream;
