/** @module server/webservices/courses/normalize */
'use strict';

function normalizeDescription(text) {
	return (' ' + ('' + [text]).replace(/\s+/, ' ') + ' ')
		.replace(/´/g, '\'')
		.replace(/è/g, 'é')
		.replace(/ - /g, ' \u2013 ')
		.replace(/\( /g, '(')
		.replace(/ \)/g, ')')
		.replace(/ "(\S)/g, ' \u201C$1')
		.replace(/(\S)" /g, '$1\u201D ')
		.replace(/ '(\S)/g, ' \u2018$1')
		.replace(/(\S)' /g, '$1\u2019 ')
		.replace(/ \/(\S)/g, '/$1')
		.replace(/(\S)\/ /g, '$1/')
		.trim();
}

function normalizeInteger(value) {
	return value | 0;
}

function normalizeBoolean(value) {
	return value === 'J';
}

function normalizeFloat(value) {
	value = +value;
	return isNaN(value) ? null : value;
}

function normalizeText(text) {
	return '' + [text];
}

function normalizeDate(date) {
	var dateMatch = ('' + date).match(/^(\d{2})-(\d{2})-(\d{4})$/);
	if (dateMatch) {
		return dateMatch[3] + '-' + dateMatch[2] + '-' + dateMatch[1];
	} else {
		return null;
	}
}

exports.description = normalizeDescription;
exports.integer = normalizeInteger;
exports.boolean = normalizeBoolean;
exports.float = normalizeFloat;
exports.text = normalizeText;
exports.date = normalizeDate;
