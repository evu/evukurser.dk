/** @module server/webservices/courses/courseCompetences */
'use strict';

var xml2json = require('./xml2json');
var normalize = require('./normalize');

function courseCompetencesStream() {
	return xml2json({
		path: ['CompetenceDescriptions_Courses', 'CompetenceDescription_Course', 'CompetenceDescription_Course'],
		transform: function(courseCompetence) {
			return {
				competence: {
					_id: normalize.text(courseCompetence.COESAPurpose)
				},
				course: {
					_id: normalize.text(courseCompetence.SubjectCode)
				},
				version: normalize.integer(courseCompetence.Version),
				level: normalize.text(courseCompetence.Level),
				start: normalize.date(courseCompetence.StartDate),
				master: normalize.boolean(courseCompetence.IsMasterCOSAPurpose)
			};
		}
	});
}

exports.stream = courseCompetencesStream;
