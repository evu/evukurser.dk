/** @module server/webservices/courses/parser */
'use strict';

var normalise = require('./normalise');
var sax2json = require('./sax2json');
var common = require('common');

function institutionsById(callback) {
	callback = common.once(callback);
	var map = {};

	return sax2json({ path: ['Institutions', 'Institution', 'Institution'] }, function(error, obj) {
		if (error) return callback(error);

		if (obj) {
			if (obj.institution_number && obj.legal_unit_name) {
				var id = '' + obj.institution_number;
				var name = normalise.text('' + obj.legal_unit_name);
				map[id] = { id: id, name: name };
			}
		} else {
			callback(null, map);
		}
	});
}

exports.institutionsById = institutionsById;
