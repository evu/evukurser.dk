/** @module server/webservices/courses/committees */
'use strict';

var xml2json = require('./xml2json');
var normalize = require('./normalize');

function committeesStream() {
	return xml2json({
		path: ['ContinuingTrainingCommittees', 'ContinuingTrainingCommittee', 'ContinuingTrainingCommittee'],
		transform: function(committee) {
			return {
				_id: normalize.text(committee.CommitteeNumber),
				name: normalize.text(committee.CommitteeName)
			};
		}
	});
}

exports.stream = committeesStream;
