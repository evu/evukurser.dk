/** @module server/webservices/courses/classes */
'use strict';

var municipalitycodes = require('./municipalitycodes');
var regionnames = require('./regionnames');
var zipcodes = require('./zipcodes');
var xml2json = require('./xml2jsonNew');
var through2 = require('through2');
var geocode = require('./geocode');
var common = require('common');
var util = require('util');

// Handling of schools
var schoolMananger = require('../../schoolManager');

function normalizeClassObject(doc) {
	//console.log('---------------------------------------------------------------');
	//console.log(doc);
	//console.log('---------------------------------------------------------------');
	var courseId = ('' + [doc.Skolefag && doc.Skolefag.SkolefagKode]) | 0;
	courseId = courseId ? '' + courseId : null;
	var schoolId = null;

	if (typeof(doc.GennemFoerendeSkoleDsnr) === 'string') {
		schoolId = doc.GennemFoerendeSkoleDsnr;
	} else if (typeof(doc.InstitutionIdentifier) === 'string') {
		schoolId = doc.InstitutionIdentifier;
	} else if (typeof(doc.GodkendtSkoleDsnr) === 'string') {
		schoolId = doc.GodkendtSkoleDsnr;
	}

	var fullSchoolName = [doc.GennemFoerendeSkoleNavn || doc.GodkendtSkoleNavn];
	schoolMananger.add(schoolId ,fullSchoolName);


	var schoolName = '' + [doc.Lokation && doc.Lokation.BetegnelseTekst];
	var schoolLocationName = schoolName;

	if (schoolName.length <= 1) {
		schoolName = '' + [doc.GennemFoerendeSkoleNavn || doc.GodkendtSkoleNavn];
		schoolLocationName = null;
	}
	if (schoolName.length <= 1) {
		schoolName = '-';
	}

	var schoolLocationCity = ('' + [doc.Lokation.PostNavn]).trim();
	var schoolLocationAddress = [];

	if (schoolLocationCity) {
		var schoolLocationZipCode = ('' + [doc.Lokation.PostNummerKode]).trim();
		var schoolLocationStreet = ('' + [doc.Lokation.StreetName]).replace(/\s+/g, ' ').trim().replace(/,+$/, '');

		if (schoolLocationStreet.length <= 1) {
			schoolLocationStreet = schoolLocationName;
		}

		var schoolLocationPostfix = ', ' + (schoolLocationZipCode ? schoolLocationZipCode + ' ' : '') + schoolLocationCity;

		if (!schoolLocationZipCode && schoolLocationStreet !== schoolLocationName) {
			schoolLocationAddress.push(schoolLocationName + ', ' + schoolLocationStreet);
		}

		schoolLocationAddress.push(schoolLocationStreet + schoolLocationPostfix);

		if (/,/.test(schoolLocationStreet)) {
			schoolLocationAddress.push(schoolLocationStreet.split(',')[0] + schoolLocationPostfix);
		}
	}

	var schoolLocationRegion = ('' + [doc.Lokation.RegionNavn]) || null;

	var classTypeName = (doc.UndervisningOplysninger && doc.UndervisningOplysninger.UndervisningsformIdentifikator) || 'Ukendt';
	var classTypeId = 'other';

	switch (classTypeName) {
		case 'Dagundervisning': classTypeId = 'day'; break;
		case 'Aftenundervisning': classTypeId = 'evening'; break;
		case 'Weekendundervisning': classTypeId = 'weekend'; break;
	}

	var canceled = (doc.AflystIndikator === 'true');
	var guaranteed = (doc.GarantiKursus === 'J');
	var days = doc.VarighedDageMaal | 0;

	var timeTextOriginal = doc.UndervisningOplysninger && doc.UndervisningOplysninger.TidspunktTekst;
	var timeTextMatch = ('' + timeTextOriginal).match(/(?:(\d{1,2})[.:,](\d{1,2})\.?|(\d{1,4}))(?:\s*-{1,2}\s*|\s+til\s+)(?:(\d{1,2})[.:,](\d{1,2})|(\d{1,4}))/);
	var timeText = null;
	if (timeTextMatch) {
		var beginHour = timeTextMatch[1] | 0;
		var beginMinute = timeTextMatch[2] | 0;
		var endHour = timeTextMatch[4] | 0;
		var endMinute = timeTextMatch[5] | 0;
		var beginTime;
		var endTime;

		if (timeTextMatch[3]) {
			beginTime = timeTextMatch[3] | 0;
			if (beginTime > 100) {
				beginHour = beginTime / 100 | 0;
				beginMinute = beginTime % 100;
			} else {
				beginHour = beginTime;
				beginMinute = 0;
			}
		}

		if (timeTextMatch[6]) {
			endTime = timeTextMatch[6] | 0;
			if (endTime > 100) {
				endHour = endTime / 100 | 0;
				endMinute = endTime % 100;
			} else {
				endHour = endTime;
				endMinute = 0;
			}
		}

		beginTime = beginHour * 60 + beginMinute;
		endTime = endHour * 60 + endMinute;

		beginTime = Math.floor(beginTime / 5) * 5;
		endTime = Math.ceil(endTime / 5) * 5;

		beginHour = beginTime / 60 | 0;
		beginMinute = beginTime % 60;
		endHour = endTime / 60 | 0;
		endMinute = endTime % 60;

		timeText =
			beginHour + ':' + ('0' + beginMinute).slice(-2) + '-' +
			endHour + ':' + ('0' + endMinute).slice(-2);
	}

	var hoursPerDay = (('' + doc.TimerPrDagRate).replace(/,/, '.') * 1.0) || 0;
	if (hoursPerDay <= 0 || hoursPerDay > 13) {
		hoursPerDay = null;
	}

	var participantsMaximum = doc.KvotientKvantitet ? (doc.KvotientKvantitet | 0) : null;
	var participantsMinimum = doc.AktivitetMinimumAntalMaal ? (doc.AktivitetMinimumAntalMaal | 0) : null;
	var participantsRemaining = null;

	var availability = [];

	if (Array.isArray(doc.SkolefagPladsInfo)) {
		availability = availability.concat(doc.SkolefagPladsInfo);
	}

	if (typeof doc.SkolefagPladsInfo === 'object') {
		availability.push(doc.SkolefagPladsInfo);
	}

	if (Array.isArray(doc.AabentVaerkstedPladsInfo)) {
		availability = availability.concat(doc.AabentVaerkstedPladsInfo);
	}

	if (typeof doc.AabentVaerkstedPladsInfo === 'object') {
		availability.push(doc.AabentVaerkstedPladsInfo);
	}

	if (Array.isArray(availability) && availability.length > 0) {
		var minOffered = Infinity;
		var maxAdmitted = 0;
		availability.forEach(function(day) {
			var offered = day.UdbudtAntalPladserMaal | 0;
			var admitted = day.OptagetAntalPladserMaal | 0;
			if (offered < minOffered) minOffered = offered;
			if (admitted > maxAdmitted) maxAdmitted = admitted;
		});
		if (participantsMaximum === null) {
			participantsMaximum = minOffered;
		}

		participantsRemaining = Math.max(0, minOffered - maxAdmitted);
	}

	var registrationLink = '' + doc.KviknrTekst;
	if (!/^https?:\/\//i.test(registrationLink)) {
		registrationLink = null;
	}

	var price = {};

	var priceTargetGroup = doc.PrisBeloeb | 0;
	if (priceTargetGroup > 0) {
		price.targetGroup = {
			amount: priceTargetGroup.toFixed(2),
			currency: 'DKK'
		};
	}

	var priceFull = doc.FuldPrisBeloeb | 0;
	if (priceFull > 0) {
		price.full = {
			amount: priceFull.toFixed(2),
			currency: 'DKK'
		};
	}

	var contactName = ('' + [doc.Kontaktperson && doc.Kontaktperson.PersonName]).replace(/\s+/g, ' ').trim();
	contactName = (contactName ? contactName.split(/\s*\/\s*/) : []).map(function(name) {
		name = (name.slice(0, 1).toLocaleUpperCase() + name.slice(1))
			.replace(/kursusekretær/ig, 'Kursussekretær');
		if (name === 'Kursussekretær') name = 'Kursussekretæren';
		return name;
	});

	var contactPhoneOriginal = ('' + [(doc.Kontaktperson && doc.Kontaktperson.TelefonNummerTekst) || (doc.Lokation && doc.Lokation.TelefonNummerTekst)]).replace(/\s+/g, '');
	var contactPhone = [];
	if (contactPhoneOriginal) {
		contactPhoneOriginal.split(/\//).forEach(function(phone) {
			if (/^\d{8}$/.test(phone) || contactPhone.length === 0) {
				contactPhone.push(phone);
			} else {
				var lastIndex = contactPhone.length - 1;
				contactPhone[lastIndex] += ' / ' + phone;
			}
		});
	}

	var contactEmail = ('' + [doc.EmailAdresseTekst]).toLowerCase();
	contactEmail = (contactEmail ? contactEmail.split(/\s*[\/,;]\s*/) : []);

	var contactWeb = '' + [doc.HjemmesideTekst];
	if (/@/.test(contactWeb)) {
		var otherEmail = contactWeb.toLowerCase();
		if (contactEmail.indexOf(otherEmail) === -1) {
			contactEmail.push(otherEmail);
		}
		contactWeb = [];
	} else {
		contactWeb = (contactWeb ? [contactWeb] : []).map(function(url) {
			return url.replace(/^[a-z0-9\.\-]+/i, function(host) {
				return host.toLowerCase();
			});
		});
	}

	return {
		course: {
			id: courseId
		},
		school: {
			id: schoolId,
			name: schoolName,
			nameId: createId(schoolName),
			location: {
				name: schoolLocationName,
				address: schoolLocationAddress,
				region: schoolLocationRegion
			}
		},
		type: {
			id: classTypeId,
			name: classTypeName
		},
		canceled: canceled,
		guaranteed: guaranteed,
		duration: {
			begin: doc.StartDato || null,
			end: doc.SlutDato || null,
			quarter: doc.Kvartal || null,
			days: days || null,
			time: timeText,
			hoursPerDay: hoursPerDay,
			text: days ?
				(days === 1 ? '1 dag' : days + ' dage') + (timeText ? ' ' + timeText : '') :
				(timeText ? timeText : 'ukendt længde')
		},
		participants: {
			maximum: participantsMaximum,
			minimum: participantsMinimum,
			remaining: participantsRemaining
		},
		registration: {
			deadline: doc.TilmeldingsfristDato || null,
			link: registrationLink
		},
		price: price,
		contact: {
			name: contactName,
			phone: contactPhone,
			email: contactEmail,
			web: contactWeb
		}
	};
}

function dateToQuarter(date) {
	var dateMatch = ('' + date).match(/^(\d{4})-(\d{2})-(\d{2})$/);
	if (dateMatch) {
		var month = dateMatch[2] | 0;
		var quarter = ((month - 1) / 3 | 0) + 1;
		return dateMatch[1] + 'Q' + quarter;
	} else {
		return null;
	}
}

function classesStream() {
	return xml2json({
		ns: {
			'w005': 'http://uas.uni-c.dk/easy-a/webservices/schemas/',
			'uvm': 'http://rep.oio.dk/uvm.dk/xml/schemas/2004/12/03/'
		},
		base: 'w005:Result/w005:W005ResultatSamling/w005:SkoleAktivitetsUdbud',
		collect: {
			'uvm:InstitutionIdentifier': 'InstitutionIdentifier',
			'w005:AktivitetUdbud': '.'
		},
		emit: function(doc) {
			var FagUdbud = doc.FagUdbud;
			if (!FagUdbud) return;

			delete doc.FagUdbud;

			if (!Array.isArray(FagUdbud)) {
				FagUdbud = [FagUdbud];
			}

			for (var i = 0; i < FagUdbud.length; i++) {
				var courseDoc = util._extend(FagUdbud[i], doc);
				var SkolefagPeriode = courseDoc.SkolefagPeriode;
				delete courseDoc.SkolefagPeriode;

				if (!SkolefagPeriode) {
					SkolefagPeriode = {
						StartDato: courseDoc.StartDato,
						SlutDato: courseDoc.SlutDato,
						VarighedDageMaal: courseDoc.VarighedDageMaal
					};
				}

				if (!Array.isArray(SkolefagPeriode)) {
					SkolefagPeriode = [SkolefagPeriode];
				}

				for (var j = 0; j < SkolefagPeriode.length; j++) {
					var classDoc = util._extend(SkolefagPeriode[j], courseDoc);

					if (!classDoc.Lokation) {
						classDoc.Lokation = classDoc.UndervisningOplysninger.Lokation;
						classDoc.Lokation.PostNummerKode = classDoc.UndervisningOplysninger.PostNummerKode;
					} else {
						if (JSON.stringify(classDoc.Lokation) === JSON.stringify(classDoc.UndervisningOplysninger.Lokation)) {
							classDoc.Lokation.PostNummerKode = classDoc.UndervisningOplysninger.PostNummerKode;
						}
					}

					if (classDoc.Lokation.PostNummerKode) {
						classDoc.Lokation.PostNavn = zipcodes[classDoc.Lokation.PostNummerKode];
					} else if (classDoc.Lokation.MunicipalityCode) {
						classDoc.Lokation.PostNavn = municipalitycodes['' + (classDoc.Lokation.MunicipalityCode | 0)];
					}

					classDoc.Lokation.RegionNavn = regionnames['' + (classDoc.Lokation.MunicipalityCode | 0)];

					classDoc.StartKvartal = dateToQuarter(classDoc.StartDato);
					classDoc.SlutKvartal = dateToQuarter(classDoc.SlutDato);
					classDoc.Kvartal = classDoc.StartKvartal; // TODO: Figure out precise semantics

					this.push(normalizeClassObject(classDoc));
				}
			}
		}
	});
}

function classesGeocoderStream() {
	return through2.obj(function(doc, _, done) {
		var self = this;

		var location = doc.school.location;
		delete doc.school.location;

		var address = location.address;
		geocode(address, function(error, geoAnswer) {
			if (error) {
				console.error('Could not geocode: ' + (address.join(' ; ') || '-'));
				doc.school.geo = {};
			} else {
				doc.school.geo = geoAnswer;
			}

			if (location.region) {
				doc.school.geo.properties = doc.school.geo.properties || {};
				doc.school.geo.properties.address = doc.school.geo.properties.address || {};
				doc.school.geo.properties.address.region = location.region;
			}

			self.push(doc);
			done();
		});
	});
}

function classesGroupedByCourse(callback) {
	var docs = {};

	return through2.obj(function(doc, _, done) {
		var id = doc.course.id;
		delete doc.course;
		if (id) {
			docs[id] = docs[id] || [];
			docs[id].push(doc);
		}
		done();
	}, function(done) {
		callback(null, docs);
		done();
	});
}

function classWithBeginAndEnd(cls) {
	var currentDate = new Date().toISOString().slice(0, 10);
	return (
		cls &&
		cls.duration &&
		cls.duration.begin &&
		cls.duration.end &&
		cls.duration.begin >= currentDate
	);
}

function createId(name) {
	return name
		.toLocaleLowerCase()
		.replace(/é/g, 'e')
		.replace(/æ/g, 'ae')
		.replace(/ø/g, 'oe')
		.replace(/å/g, 'aa')
		.replace(/ä/g, 'ae')
		.replace(/ö/g, 'oe')
		.replace(/ü/g, 'ue')
		.replace(/[^a-z0-9]+/g, '-')
		.replace(/^-|-$/g, '');
}

function coursesInsertToDatabaseStream(db, updateCourseIds, callback) {
	return classesGroupedByCourse(function(error, docs) {
		common.step([
			function(next) {
				updateCourseIds.forEach(function(courseId) {
					var allClasses = docs[courseId] || [];
					var classes = allClasses.filter(classWithBeginAndEnd);

					var classesBegin = null;
					var classesEnd = null;
					var maxRemaining = 0;
					var quarters = {};
					var guaranteed = false;

					for (var i = 0; i < classes.length; i++) {
						var cls = classes[i];
						if (cls.canceled) continue;

						var begin = cls.duration.begin;
						var end = cls.duration.end;
						var quarter = cls.duration.quarter;
						var remaining = cls.participants.remaining;

						if (cls.guaranteed) {
							guaranteed = true;
						}
						if (!classesBegin || begin < classesBegin) {
							classesBegin = begin;
						}
						if (!classesEnd || end > classesEnd) {
							classesEnd = end;
						}
						if (quarter) {
							quarters[quarter] = true;
						}
						if (remaining > maxRemaining) {
							maxRemaining = remaining;
						}
					}

					var query = {
						query: { _id: courseId },
						update: {
							$set: {
								'classes': classes,
								'classesInfo': {
									guaranteed: guaranteed,
									duration: {
										begin: classesBegin,
										end: classesEnd,
										quarters: Object.keys(quarters).sort()
									},
									participants: {
										remaining: maxRemaining
									},
									updated: new Date()
								}
							}
						}
					};

					if (!process.env.CRON) console.log('Updated course %s with %s class(es).', courseId, classes.length);

					if (process.env.DEBUG) {
						console.log('db.courses.findAndModify(\n' + util.inspect(query, { colors: true, depth: 4 }) + ');');
						setTimeout(function(next) { next(); }, 100, next.parallel());
					} else {
						db.courses.findAndModify(query, next.parallel());
					}
				});
			},
			function() {
				callback();
			}
		], function(error) {
			console.error('Could not update course with classes:', error.stack || error);
			callback(error);
		});
	});
}

exports.stream = classesStream;
exports.geocoderStream = classesGeocoderStream;
exports.insertToDatabaseStream = coursesInsertToDatabaseStream;



/*
// var count = 0;

var stream = require('readable-stream');
var path = require('path');
var fs = require('fs');

var allClassesStream = new stream.PassThrough({ objectMode: true });
var testdataDir = path.resolve(__dirname, '../../testdata');

allClassesStream.pipe(coursesInsertToDatabaseStream(require('../../db')));
allClassesStream.setMaxListeners(1000);

var activeStreams = 0;
fs.readdirSync(testdataDir).forEach(function(filename) {
	if (/^W005-.+\.xml$/.test(filename)) {
		activeStreams++;
		filename = path.join(testdataDir, filename);
		require('fs').createReadStream(filename).pipe(classesStream()).pipe(classesGeocoderStream()).pipe(through2.obj(function(obj, _, done) {
			allClassesStream.push(obj);
			done();
		}, function(done) {
			activeStreams--;
			if (activeStreams === 0) {
				allClassesStream.push(null);
			}
			done();
		}));
	}
});
*/

// require('fs').createReadStream(__dirname + '/../../testdata/W005.xml').pipe(classesStream())
// .pipe(classesGeocoderStream())
// .pipe(coursesInsertToDatabaseStream(require('../../db')));

/*.pipe(through2.obj(function(obj, _, done) {
	count++;
	// console.log(count);
	if (!obj.course.id) {
	// if (/^,/.test(obj.school.location.address)) {
	console.log(count + '.\n' + util.inspect(obj, { colors: true, depth: 5 }) + '\n');
	}
	// console.log(obj.school.location.address.join(' ; '));
	done();
}, function(done) {
	console.log(count);
	done();
}));*/
