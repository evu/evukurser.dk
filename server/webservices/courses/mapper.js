/** @module server/webservices/courses/mapper */
'use strict';

var through2 = require('through2');

var getters = {};

function compileKeyGetter(path) {
	var getter = getters[path];
	if (getter) return getter;

	if (!/^([a-z_]\w*)(\.[a-z_]\w*)*$/i.test(path)) throw new Error('mapper: Invalid JSON key path.');

	var name = 'getJSONKey_' + path.replace(/\W/g, '_');

	var elements = [];
	var currentElement = 'obj';
	elements.push(currentElement);
	path.split('.').forEach(function(element) {
		currentElement += '.' + element;
		elements.push(currentElement);
	});

	/*jshint -W054*/
	getter = getters[path] = new Function('return function ' + name + '(obj) { return ' + elements.join(' && ') + '; };')();
	/*jshint +W054*/
	return getter;
}

function mapper(options, callback) {
	var keysType = options.keys;
	var keysList = Object.keys(keysType);
	var sorter = options.sort;
	var result = {};
	var getters = {};

	keysList.forEach(function(key) {
		var keyType = keysType[key];
		result[key] = {};
		getters[key] = compileKeyGetter(key);

		if (keyType.min) {
			keysType[key] = (function(getter) {
				return function returnMinObject(oldObject, newObject) {
					var oldValue = getter(oldObject);
					var newValue = getter(newObject);
					return (newValue < oldValue) ? newObject : oldObject;
				};
			}(compileKeyGetter(keyType.min)));
		}

		if (keyType.max) {
			keysType[key] = (function(getter) {
				return function returnMaxObject(oldObject, newObject) {
					var oldValue = getter(oldObject);
					var newValue = getter(newObject);
					return (newValue > oldValue) ? newObject : oldObject;
				};
			}(compileKeyGetter(keyType.max)));
		}
	});

	return through2.obj(function(obj, encoding, done) {
		keysList.forEach(function(key) {
			var objKey = getters[key](obj);
			if (objKey !== undefined) {
				objKey = '' + objKey;
				var keyType = keysType[key];
				if (keyType === 'first') {
					if (result[key][objKey] === undefined) {
						result[key][objKey] = obj;
					}
				} else if (keyType === 'last') {
					result[key][objKey] = obj;
				} else if (keyType === 'all') {
					result[key][objKey] = result[key][objKey] || [];
					result[key][objKey].push(obj);
					if (sorter) {
						result[key][objKey].sort(sorter);
					}
				} else if (typeof(keyType) === 'function') {
					if (result[key][objKey] === undefined) {
						result[key][objKey] = obj;
					} else {
						result[key][objKey] = keyType(result[key][objKey], obj);
					}
				}
			}
		});
		done();
	}, function(done) {
		done();
		callback(null, result);
	});
}

module.exports = mapper;
