/** @module server/webservices/courses/competenceAllowances */
'use strict';

var xml2json = require('./xml2json');
var normalize = require('./normalize');

function competenceAllowancesStream() {
	return xml2json({
		path: ['Allowance_CompetenceDescriptions', 'Allowance_CompetenceDescription', 'Allowance_CompetenceDescription'],
		transform: function(competenceAllowance) {
			return {
				competence: {
					_id: normalize.text(competenceAllowance.COESAPurpose)
				},
				institution: {
					_id: normalize.text(competenceAllowance.InstitutionNumber)
				},
				version: normalize.integer(competenceAllowance.Version),
				start: normalize.date(competenceAllowance.StartDate)
			};
		}
	});
}

exports.stream = competenceAllowancesStream;
