/** @module server/webservices/courses/courseTariffs */
'use strict';

var xml2json = require('./xml2json');
var normalize = require('./normalize');

function courseTariffsStream() {
	return xml2json({
		path: ['CoursesTariff', 'CourseTariff', 'CourseTariff'],
		transform: function(courseTariff) {
			return {
				course: {
					_id: normalize.text(courseTariff.SubjectCode)
				},
				committee: {
					_id: normalize.text(courseTariff.CommitteeNumber)
				},
				family: {
					group: normalize.text(courseTariff.FamilyGroup),
					description: normalize.description(courseTariff.FamiliyDescription)
				},
				basis: {
					code: normalize.text(courseTariff.BasisCode),
					number: normalize.text(courseTariff.BasisNumber)
				},
				year: normalize.integer(courseTariff.Year),
				description: normalize.description(courseTariff.TariffDescription)
			};
		}
	});
}

exports.stream = courseTariffsStream;
