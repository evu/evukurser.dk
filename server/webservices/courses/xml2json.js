/** @module server/webservices/courses/xml2json */
'use strict';

var sax2jsonOld = require('./sax2jsonOld');
var through2 = require('through2');

function xml2json(options) {
	var stream;

	var parser = sax2jsonOld(options, function(error, obj) {
		if (!stream) return;

		if (error) {
			stream.emit('error', error);
		} else {
			stream.push(obj);
		}
	});

	return through2.obj(function(buffer, enc, callback) {
		stream = this;
		if (parser) {
			parser.write(buffer, enc);
		}
		callback();
	}, function(callback) {
		stream = null;
		if (parser) {
			parser.end();
			parser = null;
		}
		callback();
	});
}

module.exports = xml2json;
