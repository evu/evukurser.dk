/** @module server/webservices/courses/sax2jsonOld */
'use strict';

var expat = require('node-expat');

function sax2json(options, callback) {
	var transform = options.transform;
	var objectPath = options.path || [];
	var objectPathLength = objectPath.length;
	objectPath = objectPath.join('.');
	var parser = new expat.Parser('UTF-8');
	var path = [];
	var obj = [];

	parser.on('startElement', function(name) {
		path.push(name);
		if (path.length === objectPathLength && path.join('.') === objectPath) {
			obj.push({});
		} else if (obj.length > 0) {
			var lastObject = obj[obj.length - 1];
			if (lastObject[name] && !Array.isArray(lastObject[name])) {
				lastObject[name] = [lastObject[name]];
			}
			var subObject = {};
			if (lastObject[name]) {
				lastObject[name].push(subObject);
			} else {
				lastObject[name] = subObject;
			}
			obj.push(subObject);
		}
	});

	parser.on('endElement', function() {
		if (path.length === objectPathLength && path.join('.') === objectPath) {
			var resultingObject = obj[0];
			if (transform) {
				resultingObject = transform(resultingObject);
			}
			callback(null, resultingObject || {});
			obj = [];
		} else if (obj.length > 0) {
			if (obj.length > 1) {
				var lastObject = obj[obj.length - 2];
				var key = path[path.length - 1];
				if (Array.isArray(lastObject[key])) {
					lastObject = lastObject[key];
					key = lastObject.length - 1;
				}
				if (typeof(lastObject[key]) === 'string') {
					var normalizedText = lastObject[key].replace(/\s+/, ' ').trim();
					if (normalizedText) {
						lastObject[key] = normalizedText;
					} else {
						lastObject[key] = obj[obj.length - 1];
						if (Object.keys(lastObject[key]).length === 0) {
							lastObject[key] = null;
						}
					}
				} else if (!Array.isArray(lastObject[key]) && Object.keys(lastObject[key]).length === 0) {
					lastObject[key] = null;
				}
			}
			obj.pop();
		}
		path.pop();
		if (path.length === 0) {
			parser.removeAllListeners();
			parser = null;
			callback();
		}
	});

	parser.on('text', function(text) {
		if (obj.length > 1) {
			var lastObject = obj[obj.length - 2];
			var key = path[path.length - 1];
			if (Array.isArray(lastObject[key])) {
				lastObject = lastObject[key];
				key = lastObject.length - 1;
			}
			if (typeof(lastObject[key]) === 'string') {
				lastObject[key] += text;
			} else {
				lastObject[key] = text;
			}
		}
	});

	parser.on('error', function(error) {
		parser.removeAllListeners();
		parser = null;
		callback(error);
	});

	return parser;
}

module.exports = sax2json;
