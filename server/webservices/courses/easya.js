/** @module server/webservices/courses/easya */
'use strict';

var Socks5ClientHttpsAgent = require('socks5-https-client/lib/Agent');
var through2 = require('through2');
var request = require('request');
var stream = require('readable-stream');
var expat = require('node-expat');
var pkg = require('../../../package.json');
var loggly = require('../../loggly');

function requestEasyA(wsOptions) {
	if (!process.env.CRON) {
		//var dsNumber = '' + wsOptions.dsNumber;
		//if (dsNumber === '999880') dsNumber = 'EasyF';
		console.log('Fetching EasyA/' + wsOptions.moduleNumber + ' (' + wsOptions.dsNumber + ') with query.');
	}

	var xml = '';
	xml += '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:eas="EasyaWS">';
	xml += '<soapenv:Body>';
	xml += '<eas:WSCallEasyA soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">';
	xml += '<modulNummer xsi:type="xsd:string">' + wsOptions.moduleNumber + '</modulNummer>';
	xml += '<dsNr xsi:type="xsd:string">' + wsOptions.dsNumber + '</dsNr>';
	xml += '<parameterList xsi:type="xsd:string"><![CDATA[' + wsOptions.parameterList + ']]></parameterList>';
	xml += '<version xsi:type="xsd:string">' + wsOptions.version + '</version>';
	xml += '<adgangsKode xsi:type="xsd:string">' + wsOptions.password + '</adgangsKode>';
	xml += '</eas:WSCallEasyA>';
	xml += '</soapenv:Body>';
	xml += '</soapenv:Envelope>';

	var options = {
		uri: 'https://ew-x.easy.uni-c.dk:7778/ws',
		method: 'POST',
		body: xml,
		strictSSL: true,
		headers: {
			'User-Agent': pkg.name + '/' + pkg.version,
			'Accept': 'application/xml,text/xml;q=0.9',
			'Accept-Encoding': 'none',
			'Accept-Charset': 'UTF-8',
			'Content-Type': 'text/xml; charset=UTF-8'
		},
		timeout: 10 * 60 * 1000
	};

	options.agent = new Socks5ClientHttpsAgent({
		socksHost: '127.0.0.1',
		socksPort: 10101
	});

	var responseStream = new stream.PassThrough();
	var innerDump = '';

	var parser = new expat.Parser('UTF-8');
	var responseBuffer = null;

	parser.on('startElement', function(name) {
		if (name === 'return') {
			responseBuffer = '';
		}
	});

	parser.on('endElement', function(name) {
		if (name === 'return') {
			if (process.env.DEBUG) {
				innerDump += responseBuffer;
				console.log('EasyA response:\n\u001B[34m' + innerDump.replace(/></g, '>\n<') + '\u001B[0m\n');
			}
			responseStream.end(responseBuffer);
			responseBuffer = null;
		}
	});

	parser.on('text', function(text) {
		if (responseBuffer !== null) {
			responseBuffer += text;
			if (responseBuffer.length >= 65536) {
				var chunk = responseBuffer.slice(0, 65536);
				if (process.env.DEBUG) {
					innerDump += chunk;
				}
				responseStream.write(chunk);
				responseBuffer = responseBuffer.slice(65536);
			}
		}
	});

	var dump = [];

	parser.on('error', function(error) {
		console.log('\n\nEasyA webservice response failed:\n' + Buffer.concat(dump).toString() + '\n\n');
		console.error('XML parse error:', error);
		// responseStream.emit('error', error);
	});

	request(options, function(error, result) {
		if (error) {
			console.log('EasyA webservice response failed: ' +  (error.stack || error));
			loggly.log('EasyA webservice response failed: ' +  (error.stack || error), ['evukurser.dk', 'fatal', 'node-webservice', 'easya']);
			process.exit(1);
		}
	}).pipe(through2(function(buffer, encoding, done) {
		buffer = new Buffer(buffer, encoding);
		dump.push(buffer);
		parser.write(buffer);
		done();
	}, function(done) {
		dump = [];
		parser.end();
		done();
	}));

	return responseStream;
}

function requestW005(wsOptions) {
	var xml = '';
	xml += '<w005:ParameterList xsi:schemaLocation="http://rep.oio.dk/uvm.dk/studieadm/EASY-A/W005/xml/schemas/2007/01/29/ http://uas.uni-c.dk/easy-a/webservices/schemas/W005/7/W005_parametre.xsd" xmlns:w005="http://rep.oio.dk/uvm.dk/studieadm/EASY-A/W005/xml/schemas/2007/01/29/" xmlns:sa="http://rep.oio.dk/uvm.dk/studieadm/common/xml/schemas/2006/02/20/" xmlns:uvm="http://rep.oio.dk/uvm.dk/xml/schemas/2004/12/03/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
	if (Array.isArray(wsOptions.institutions)) {
		xml += '<InstitutionIdentifierList>';
		wsOptions.institutions.forEach(function(id) {
			xml += '<uvm:InstitutionIdentifier>' + id + '</uvm:InstitutionIdentifier>';
		});
		xml += '</InstitutionIdentifierList>';
	}
	if (Array.isArray(wsOptions.courses)) {
		xml += '<SkolefagListe>';
		wsOptions.courses.forEach(function(id) {
			xml += '<sa:SkolefagKode>' + id + '</sa:SkolefagKode>';
		});
		xml += '</SkolefagListe>';
	}
	if (wsOptions.includeAvailability !== undefined) {
		xml += '<VisPladsInformationIndikator>' + wsOptions.includeAvailability + '</VisPladsInformationIndikator>';
	}
	if (wsOptions.startDate !== undefined) {
		xml += '<FraDato>' + wsOptions.startDate + '</FraDato>';
	}
	if (wsOptions.endDate !== undefined) {
		xml += '<TilDato>' + wsOptions.endDate + '</TilDato>';
	}
	if (wsOptions.maxQuantity !== undefined) {
		xml += '<MaxAntalHoldKvantitet>' + wsOptions.maxQuantity + '</MaxAntalHoldKvantitet>';
	}
	xml += '<SendAflysteHoldIndikator>' + !!wsOptions.includeCanceled + '</SendAflysteHoldIndikator>';
	xml += '</w005:ParameterList>';

	return requestEasyA({
		moduleNumber: 'W005',
		dsNumber: 999880,
		parameterList: xml,
		version: 7,
		password: wsOptions.password
	});
}

exports.requestEasyA = requestEasyA;
exports.requestW005 = requestW005;


/*
var allRelevantCourses = ["155","156","3398","40003","40066","40073","40086","40088","40089","40090","40091","40092","40093","40100","40103","40104","40106","40107","40108","40109","40111","40112","40113","40114","40137","40140","40141","40143","40160","40191","40326","40352","40353","40390","40391","40392","40494","40503","40504","40505","40567","40568","40569","40570","40573","40633","40743","40747","40855","40991","40996","41255","41355","41381","41979","41981","41982","41983","42002","42003","42004","42005","42006","42007","42008","42009","42010","42011","42012","42013","42014","42016","42017","42018","42019","42020","42021","42022","42023","42024","42025","42026","42029","42031","42032","42034","42035","42036","42037","42038","42039","42040","42041","42069","42070","42071","42072","42908","43171","43179","43572","43575","43697","43996","44019","44020","44022","44038","44039","44040","44042","44043","44044","44046","44051","44069","44070","44071","44072","44073","44074","44075","44076","44077","44080","44082","44083","44084","44085","44086","44087","44088","44089","44090","44093","44094","44095","44097","44098","44101","44102","44103","44104","44105","44106","44109","44110","44111","44112","44138","44151","44152","44153","44154","44162","44163","44164","44166","44168","44169","44170","44171","44172","44173","44177","44178","44179","44205","44206","44207","44208","44350","44371","44414","44451","44462","44465","44494","44499","44530","44555","44676","44714","44724","44725","44726","44729","44731","44732","44733","44737","44738","44741","44742","44743","44744","44745","44746","44748","44753","44754","44755","44756","44758","44760","44761","44763","44764","44765","44766","44772","44773","44774","44854","44856","44857","44860","44867","44939","44976","44977","44978","44979","44988","44990","44997","44998","45141","45261","45286","45350","45361","45362","45364","45365","45366","45367","45368","45369","45371","45375","45379","45380","45381","45473","45504","45508","45509","45563","45565","45566","45572","45573","45574","45622","45689","45702","45703","45704","45779","45806","45859","45882","45892","45897","45898","45903","45908","45910","45911","45917","45969","45972","45983","46266","46522","46525","46818","46982","46983","46984","46985","46987","46988","46989","46992","46993","46995","47052","47067","47101","47102","47134","47139","47140","47152","47242","47288","47306","47496","47498","47499","47501","47502","47504","47505","47506","47507","47508","47509","47517","47545","47546","47547","47549","47550","47557","47576","47592","47821","47824"];

require('async').eachLimit(allRelevantCourses, 3, function(courseId, done) {
	console.log(courseId + ': loading');
	requestW005({
		courses: [courseId],
		includeCanceled: true,
		includeAvailability: true,
		password: config.ws.easya.password
	}).on('end', function() {
		console.log(courseId + ': OK');
		done();
	}).pipe(require('fs').createWriteStream(__dirname + '/../../testdata/W005-' + courseId + '.xml'));
}, function() {
	console.log('All done.');
});
*/
