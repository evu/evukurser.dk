/** @module server/webservices/courses */
'use strict';

/*
Runs process of updating information in databse from webservices:

1. If at night and courses are not updated for more than one day, update them
2. For the course with the least recently updated classes, update the classes
*/

var updateCourses = require('./updateCourses');
var classes = require('./classes');
var config = require('../../config');
var easya = require('./easya');
var db = require('../../db');
// Logging 
var loggly = require('../../loggly');

var classesTimeToLive = 6 * 60 * 60; // 6 hours
var coursesTimeToLive = 18 * 60 * 60; // 18 hours

function findNumberOfCoursesWithOldClasses(callback) {
	db.courses.count({
		$or: [
			{ 'classesInfo.updated': { $lt: new Date(Date.now() - classesTimeToLive * 1000) } },
			{ 'classesInfo.updated': { $exists:false } }
		]
	}, callback);
}

function findCourseWithOldestClasses(callback) {
	db.courses.find({ 'overwrites.custom': { '$ne': true } }, { 'classesInfo.updated': 1 }).sort({ 'classesInfo.updated': 1 }).limit(1, function(error, docs) {
		if (error) return callback(error);
		if (!Array.isArray(docs)) return callback(null, null);

		var doc = docs[0] || {};
		callback(null, ('' + [doc._id]) || null);
	});
}

function updateClassesForCourse(courseId, callback) {
	var updateCourseIds = [courseId];

	easya.requestW005({
		courses: updateCourseIds,
		includeCanceled: true,
		includeAvailability: true,
		password: config.ws.easya.password
	})
		.pipe(classes.stream())
		.pipe(classes.geocoderStream())
		.pipe(classes.insertToDatabaseStream(db, updateCourseIds, callback));
}

function runClassUpdateForCourse(courseId) {
	updateClassesForCourse(courseId, function(error) {
		db.close();

		if (error) {
			console.error('Could not update classes for course:', error.stack || error);
			loggly.log('Could not update classes for course: ' +  (error.stack || error), ['evukurser.dk', 'fatal', 'node-webservice']);			
			process.exit(1);
		}
	});
}

function updateClassesForOneCourse(callback) {
	findCourseWithOldestClasses(function(error, courseId) {
		if (error) return callback(error);
		updateClassesForCourse(courseId, callback);
	});
}

function updateClassesUntilTime(count, endTime, errors, callback) {
	if (count <= 0) return callback();

	var now = Date.now();
	var timeLeft = endTime - now;
	if (timeLeft <= 0) return callback(errors);

	updateClassesForOneCourse(function(error) {
		if (error) {
			errors.push(error);
			console.error('Could not update classes for a course:', error.stack || error);
		}
		updateClassesUntilTime(count - 1, endTime, errors, callback);
	});
}

function updateClasses(maxCount, maxSeconds, callback) {
	updateClassesUntilTime(maxCount, Date.now() + maxSeconds * 1000, [], callback);
}

function runClassUpdater() {
	var hasErrors = false;

	findNumberOfCoursesWithOldClasses(function(error, count) {
		if (error) {
			hasErrors = true;
			console.error('Could not look up number of courses with old classes:', error.stack || error);
			loggly.log('Could not look up number of courses with old classes: ' +  (error.stack || error), ['evukurser.dk', 'fatal', 'node-webservice']);
			count = 5; // just update a few courses
		}

		if (!process.env.CRON) console.log('Number of couses that needs updating: %s', count);

		updateClasses(count, 55, function(errors) {
			if (errors && errors.length > 0) {
				hasErrors = true;
			}

			db.close();

			if (hasErrors) process.exit(1);
		});
	});
}

function findNumberOfOldCourses(callback) {
	db.courses.count({ 'src.id': { $exists: true } }, function(error, count) {
		if (error) {
			console.error('Could not check number of existing courses: ', error.stack || error);
			loggly.log('Could not check number of existing courses: ' +  (error.stack || error), ['evukurser.dk', 'fatal', 'node-webservice']);
		}

		if (!count) return callback(null, 1000); // report many old, since there are no existing

		db.courses.count({
			$or: [
				{ 'src.updated': { $lt: new Date(Date.now() - coursesTimeToLive * 1000) } },
				{ 'src.updated': { $exists:false } }
			],
			'src.id': { $exists: true }
		}, callback);
	});
}

function runCourseUpdater() {
	var hasErrors = false;

	findNumberOfOldCourses(function(error, count) {
		if (error) {
			hasErrors = true;
			console.error('Could not look up number of old courses:', error.stack || error);
			loggly.log('Could not look up number of old courses: ' +  (error.stack || error), ['evukurser.dk', 'fatal', 'node-webservice']);
			count = 1; // just update them
		}

		if (!process.env.DEBUG && count <= 0) {
			if (!process.env.CRON) console.log('Courses already up-to-date.');
			db.close();
			return;
		}

		updateCourses(db, function(error) {
			if (error) {
				hasErrors = true;
				console.error('Could not update courses:', error.stack || error);
				loggly.log('Could not update courses: ' +  (error.stack || error), ['evukurser.dk', 'fatal', 'node-webservice']);

			}

			db.updateCourseTypes(function(error) {
				if (error) {
					hasErrors = true;
					console.error('Could not update course types:', error.stack || error);
					loggly.log('Could not update course types:', (error.stack || error), ['evukurser.dk', 'fatal', 'node-webservice']);
				}

				db.close();

				if (hasErrors) process.exit(1);
			});
		});
	});
}

function main() {
	var command = process.argv[2];
	var arg = process.argv[3];
	var courseId = arg | 0;

	var logTags = ['evukurser.dk', 'info', 'start', 'node-webservice'];

	if (command === 'update-class' && courseId) {
		loggly.log('Webservice was started', logTags);
		logTags.push('update-class');
		runClassUpdateForCourse('' + courseId);
	} else if (command === 'update-classes') {
		logTags.push('update-classes');
		loggly.log('Webservice was started', logTags);
		runClassUpdater();
	} else if (command === 'update-courses') {
		logTags.push('update-courses');
		loggly.log('Webservice was started', logTags);
		runCourseUpdater();
	} else {
		console.log('Usage: %s %s <update-classes | update-courses>', process.argv[0], process.argv[1]);
		process.exit(1);
	}
}

main();
