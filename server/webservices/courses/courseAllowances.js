/** @module server/webservices/courses/courseAllowances */
'use strict';

var xml2json = require('./xml2json');
var normalize = require('./normalize');

function courseAllowancesStream() {
	return xml2json({
		path: ['Allowance_Courses', 'Allowance_Course', 'Allowance_Course'],
		transform: function(courseAllowance) {
			return {
				course: {
					_id: normalize.text(courseAllowance.SubjectCode)
				},
				institution: {
					_id: normalize.text(courseAllowance.InstitutionNumber)
				},
				level: normalize.text(courseAllowance.Level),
				start: normalize.date(courseAllowance.StartDate),
				end: normalize.date(courseAllowance.EndDate),
				allowance: normalize.text(courseAllowance.AllowanceType),
				granted_by: normalize.text(courseAllowance.GrantedBy) || null
			};
		}
	});
}

exports.stream = courseAllowancesStream;
