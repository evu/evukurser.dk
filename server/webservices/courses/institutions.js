/** @module server/webservices/courses/institutions */
'use strict';

var xml2json = require('./xml2json');
var normalize = require('./normalize');

function institutionsStream(competencesByInstitutionId) {
	return xml2json({
		path: ['Institutions', 'Institution', 'Institution'],
		transform: function(institution) {
			institution = {
				_id: normalize.text(institution.InstitutionNumber),
				legal_name: normalize.text(institution.LegalUnitName),
				competences: []
			};
			if (competencesByInstitutionId) {
				institution.competences = competencesByInstitutionId[institution._id];
			}
			return institution;
		}
	});
}

exports.stream = institutionsStream;
