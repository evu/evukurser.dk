/** @module server/webservices/courses/md2text */
'use strict';

var marked = require('marked');

function md2text(markdown) {
	return marked('' + [markdown]).replace(/<.*?>/g, '\n\n').replace(/\n{3,}/g, '\n\n').trim();
}

module.exports = md2text;
