/** @module server/webservices/courses/rtf2md */
'use strict';

var unrtf = require('unrtf');
var toMarkdown = require('to-markdown').toMarkdown;

function rtf2md(doc, callback) {
	if (!doc) return callback(null, '');

	unrtf(doc, function(error, result) {
		if (error) return callback(error);
		var md = toMarkdown(result.html);

		callback(null, md);
	});
}

module.exports = rtf2md;
