/** @module server/webservices/courses/geocoder */
'use strict';

var geocoder = require('geocoder');
var common = require('common');
var LRU = require('lru-cache');
var db = require('../../db');

var dbMaxAge = 7 * 24 * 60 * 60 * 1000;
var memoryMaxAge = 5 * 60 * 1000;
var requestsPerSecond = 9;

var cache = LRU({
	max: 1024 * 1024,
	length: function(obj) { return JSON.stringify(obj).length; },
	maxAge: memoryMaxAge
});

var requests = {};
var activeRequests = [];
var latestRequest = 0;
var requestWaitingTime = Math.round(1000 / requestsPerSecond);
var active = false;

function geocodeRealRequest(addressSuggestion, callback) {
	geocoder.geocode(addressSuggestion + ', Denmark', function(error, geo) {
		if (error) return callback(error);

		var result = geo && geo.results && geo.results[0];
		if (!result) return callback(null, {});

		callback(null, {
			type: 'Feature',
			geometry: {
				type: 'Point',
				coordinates: [
					result.geometry.location.lat,
					result.geometry.location.lng
				]
			},
			properties: {
				address: {
					formatted: result.formatted_address.replace(/, Denmark$/, '')
				}
			}
		});
	});
}

function geocodeRunRequests() {
	if (active) return;
	active = true;

	var now = Date.now();
	var remainingTime = Math.max(0, requestWaitingTime - (now - latestRequest));
	latestRequest = now;

	setTimeout(function(data) {
		var address = data[0];
		var callback = data[1];
		geocodeRealRequest(address, function(error, answer) {
			active = false;
			if (activeRequests.length > 0) {
				geocodeRunRequests();
			}
			callback(error, answer);
		});
	}, remainingTime, activeRequests.shift());
}

function geocodeCachedRequest(addressSuggestion, callback) {
	common.step([

		function(next) {
			db.geocode.findOne({
				address: addressSuggestion,
				updated: { $gt: new Date(Date.now() - dbMaxAge) }
			}, next);
		},

		function(answer, next) {
			if (answer) {
				callback(null, answer);
			} else {
				activeRequests.push([addressSuggestion, next]);
				geocodeRunRequests();
			}
		},

		function(answer) {
			callback(null, answer);
		}

	], function(error) {
		callback(error);
	});
}

function geocodeOne(addressSuggestion, callback) {
	addressSuggestion = '' + addressSuggestion;

	var answer = cache.get(addressSuggestion);
	if (answer) return callback(null, answer);

	var request = requests[addressSuggestion];
	if (!request) {
		request = requests[addressSuggestion] = common.future();
		request.get(function(error, answer) {
			if (!error && answer) {
				cache.set(addressSuggestion, answer);

				var query = {
					query: { address: addressSuggestion },
					update: {
						$set: {
							updated: Date.now(),
							answer: answer
						}
					},
					upsert: true
				};

				db.geocode.findAndModify(query, function(error) {
					if (error) {
						console.error('Could not cache geocoded address in DB:', error);
					}
				});
			}
		});
		geocodeCachedRequest(addressSuggestion, request.put);
	}

	request.get(callback);
}

function geocode(addressSuggestions, callback) {
	if (!Array.isArray(addressSuggestions)) {
		addressSuggestions = [addressSuggestions];
	}

	if (addressSuggestions.length === 0) return callback(null, {});

	var firstSuggestion = addressSuggestions.shift();
	geocodeOne(firstSuggestion, function(error, answer) {
		if (!error && answer && answer.geometry) {
			return callback(null, answer);
		}

		if (addressSuggestions.length > 0) {
			return geocode(addressSuggestions, callback);
		}

		if (error) {
			return callback(error);
		}

		callback(null, {});
	});
}

module.exports = geocode;
