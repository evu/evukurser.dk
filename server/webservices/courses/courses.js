/** @module server/webservices/courses/courses */
'use strict';

var normalize = require('./normalize');
var xml2json = require('./xml2json');
var through2 = require('through2');
var md2text = require('./md2text');
var rtf2md = require('./rtf2md');
var common = require('common');
var util = require('util');

function coursesStream() {
	return xml2json({
		path: ['CoursesExtented', 'CourseExtented', 'CourseExtented'],
		transform: function(course) {
			var id = normalize.text(course.SubjectCode);

			return {
				_id: id,
				level: normalize.text(course.Level),
				committee: {
					_id: normalize.text(course.CommitteeNumber)
				},
				title: {
					text: normalize.text(course.SubjectName),
					short: normalize.text(course.ShortSubjectName)
				},
				start: normalize.date(course.StartDate),
				end: normalize.date(course.EndDate),
				duration: {
					days: normalize.integer(course.Duration),
					text: normalize.text(course.VidarDuration)
				},
				certificate: normalize.boolean(course.Certificate),
				vidar: {
					purpose: normalize.text(course.VidarPurpose),
					admission: normalize.text(course.VidarAdmission),
					content: normalize.text(course.VidarContent),
					examination: normalize.text(course.VidarExamination)
				},
				purpose: normalize.text(course.PurposeOfCourse),
				judgmentRules: normalize.text(course.RulesRegardingJudgment),
				questions: normalize.text(course.QuestionsFor_systemfaelles_redskaber),
				certificateRules: normalize.text(course.RulesRegardingCertificates),
				errors: []
			};
		}
	});
}

function coursesRtfConversion() {
	return through2.obj(function(obj, encoding, callback) {
		var self = this;

		if (!obj) {
			self.push(null);
			callback();
			return;
		}

		common.step([

			function(next) {
				var purposeDone = next.parallel();
				rtf2md(obj.purpose, function(error, md) {
					if (error) {
						obj.errors.push('Could not convert RTF for .purpose: ' + (error.message || error));
					}
					obj.purpose = md || null;
					purposeDone();
				});

				var judgmentRulesDone = next.parallel();
				rtf2md(obj.judgmentRules, function(error, md) {
					if (error) {
						obj.errors.push('Could not convert RTF for .judgmentRules: ' + (error.message || error));
					}
					obj.judgmentRules = md || null;
					judgmentRulesDone();
				});

				var questionsDone = next.parallel();
				rtf2md(obj.questions, function(error, md) {
					if (error) {
						obj.errors.push('Could not convert RTF for .questions: ' + (error.message || error));
					}
					obj.questions = md || null;
					questionsDone();
				});

				var certificateRulesDone = next.parallel();
				rtf2md(obj.certificateRules, function(error, md) {
					if (error) {
						obj.errors.push('Could not convert RTF for .certificateRules: ' + (error.message || error));
					}
					obj.certificateRules = md || null;
					certificateRulesDone();
				});
			},

			function() {
				self.push(obj);
				callback();
			}

		]);
	});
}

function formatDuration(duration) {
	var days = duration.days;
	var weeks = days / 7 | 0;
	days -= weeks * 7;

	var text = [];
	var d = {};
	if (weeks > 0) {
		d.weeks = weeks;
		text.push(weeks + ' uge' + (weeks > 1 ? 'r' : ''));
	}
	if (days > 0) {
		d.days = days;
		text.push(days + ' dag' + (days > 1 ? 'e' : ''));
	}
	d.text = text.join(', ');

	return d;
}

function makeId(title) {
	return ('' + [title]).toLowerCase().replace(/[^a-z0-9]+/g, '-').replace(/^-|-$/g, '');
}

function guessTheme(tariff, competences) {
	var themes = [];

	if (tariff) {
		themes.push({
			id: makeId(tariff.family.description),
			text: tariff.family.description
		});
	}

	var masterCompetence = competences[0];
	if (masterCompetence) {
		themes.push({
			id: makeId(masterCompetence.title.text),
			text: masterCompetence.title.text
		});
	}

	return {
		main: themes[0] || null,
		sub: themes[1] || null,
		text: themes.map(function(t) { return t.text; }).join(' / ') || 'Ingen pakke'
	};
}

function markdownBlock(markdown) {
	if (!markdown) {
		return null;
	}

	var text = md2text(markdown);
	if (text === markdown) {
		return { text: text };
	}

	return {
		markdown: markdown,
		text: text
	};
}

function textBlock(text) {
	return text ? { text: text } : null;
}

function coursesNormalizerStream(index) {
	var competencesByCourseId = index.competencesByCourseId;
	var allowancesByCourseId = index.allowancesByCourseId;
	var tariffsByCourseId = index.tariffsByCourseId;
	// var relatedCousesById = index.relatedCousesById;
	var institutionsById = index.institutionsById;
	var typesByCourseId = index.typesByCourseId;
	var competencesById = index.competencesById;

	return through2.obj(function(doc, encoding, done) {
		var tariff = tariffsByCourseId[doc._id];

		var competences = (competencesByCourseId[doc._id] || []).filter(function(competence) {
			return competencesById[competence.competence && competence.competence._id];
		}).sort(function(a, b) {
			var cmp = b.master - a.master;
			if (cmp) return cmp;
			cmp = a.start < b.start ? 1 : a.start > b.start ? -1 : 0;
			return cmp;
		}).map(function(competence) {
			var id = competence.competence._id;
			competence = util._extend(competence, competencesById[id]);
			delete competence.competence;
			delete competence.courses;
			delete competence.institutions;
			return competence;
		});

		var schools = (allowancesByCourseId[doc._id] || []).filter(function(allowance) {
			return institutionsById[allowance.institution._id];
		}).sort(function(a, b) {
			var cmp = a.end < b.end ? 1 : a.end > b.end ? -1 : 0;
			if (cmp) return cmp;
			cmp = a.start < b.start ? 1 : a.start > b.start ? -1 : 0;
			return cmp;
		}).map(function(allowance) {
			var institution = institutionsById[allowance.institution._id];
			return institution._id;
		});

		var types = (typesByCourseId[doc._id] || []).map(function(id) {
			return id;
		});

		var competencePrimary = competences.filter(function(c) { return c.master; }).map(function(c) { return c._id; })[0];

		if (doc._id === '44039') {
			console.log('doc', doc);
		}

		this.push({
			id: doc._id,
			title: doc.title.text,
			description: markdownBlock(doc.purpose),
			duration: formatDuration(doc.duration),
			admission: textBlock(doc.vidar.admission),
			defaultTypes: types,
			defaultTheme: guessTheme(tariff, competences),
			certificate: !!doc.certificate,
			schools: schools,
			competences: competences.map(function(c) { return c._id; }),
			competencePrimary: competencePrimary,
			isCommon: (competencePrimary === '2270' || competencePrimary === '2735')
			// relatedCourses: relatedCousesById[doc._id].map(function(c) { return c._id; })
		});
		done();
	}, function(done) {
		done();
	});
}

function coursesInsertToDatabaseStream(db, callback) {
	var count = 0;
	var errors = [];
	var startTime = new Date();

	return through2.obj(function(course, encoding, done) {
		course = util._extend({}, course);

		// var defaultTypes = course.defaultTypes;
		// var defaultTheme = course.defaultTheme;
		delete course.defaultTypes;
		delete course.defaultTheme;

		course.updated = new Date();

		common.step([

			function(next) {
				var query = {
					query: { _id: course.id },
					update: { $set: { src: course } },
					upsert: true,
					new: true
				};
				if (process.env.DEBUG) {
					console.log('db.courses.findAndModify(\n' + util.inspect(query, { colors: true, depth: 4 }) + ');');
					setTimeout(function(next) { next(null, query.update.$set); }, 100, next);
				} else {
					db.courses.findAndModify(query, next);
				}
			},

			function(dbDoc, next) {
				if (!dbDoc) return next(new Error('Course not found!'));

				if (dbDoc.overwrites) return next();

				var query = {
					query: { _id: course.id },
					update: {
						$set: {
							'overwrites': {
								types: [],
								typesCount: 0
							}
						}
					}
				};

				if (process.env.DEBUG) {
					console.log('db.courses.findAndModify(\n' + util.inspect(query, { colors: true, depth: 4 }) + ');');
				} else {
					db.courses.findAndModify(query, function(error) {
						if (error) {
							console.error('Could not set default types/theme for course:', error);
						}
						next();
					});
				}
			},

			function() {
				if (!process.env.CRON) console.log('#%s. %s: %s', ++count, course.id, course.title);
				done();
			}

		], function(error) {
			errors.push(error);
			console.error('Could not insert course into database [' + course.id + ': ' + course.title + ']:', error.stack || error);
			done();
		});
	}, function(done) {
		var query = {
		    $or: [{
		        'src.updated': {
		            $lt: startTime
		        }
		    }, {
		        'src.updated': {
		            $exists: false
		        }
		    }],
		    'overwrites.custom': {
		        '$ne': true
		    }
		};

		var update = {
			$set: {
				'src': {}
			}
		};
		var options = {
			multi: true
		};

		if (process.env.DEBUG) {
			console.log('db.courses.update(\n' + util.inspect(query, { colors: true, depth: 4 }) + ', ' + util.inspect(update, { colors: true, depth: 4 }) + ', ' + util.inspect(options, { colors: true, depth: 4 }) + ');');
		} else {
			db.courses.update(query, update, options, function(error) {
				if (error) {
					errors.push(error);
					console.error('Could not remove old courses after updating all the courses:', error.stack || error);
				}

				if (errors.length > 0) {
					callback(new Error('There occurred ' + errors.length + ' errors while saving courses in database.'));
				} else {
					callback();
				}
				done();
			});
		}
	});
}

exports.stream = coursesStream;
exports.rtfConversion = coursesRtfConversion;
exports.normalizerStream = coursesNormalizerStream;
exports.insertToDatabaseStream = coursesInsertToDatabaseStream;
