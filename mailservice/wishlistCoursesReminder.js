// Modules
var common = require('common');
var util = require('util');
var config = require('./config');
var db = require('../server/db');
var mailProvider = require('../server/email/provider');
var moment = require('moment');

// Logging
var log4js = require('log4js');
var logger = log4js.getLogger('Wishlist course reminder');

start();

function start() {
	var result = {};

	common.step([
		// Get array of users with wishlist reminders enabled
		function(next) {
			getUsersWithWishlistRemindersEnabled(next);
		},
		// Get wishlist courses that has not been notified for users
		function(usersWithWishlistRemindersEnabled, next) {
			if (usersWithWishlistRemindersEnabled) { 
				usersWithWishlistRemindersEnabled = usersWithWishlistRemindersEnabled.filter(function(n) { return n != null; });
			}

			if (!usersWithWishlistRemindersEnabled || !usersWithWishlistRemindersEnabled.length || usersWithWishlistRemindersEnabled.length === 0) {
				logger.info('No users with certificate reminders enabled found, exiting');
				return process.exit(code=0);
			}

			logger.info('Found ' + usersWithWishlistRemindersEnabled.length + " users with wishlist reminders and enabled with completed classes");

			usersWithWishlistRemindersEnabled.forEach(function(user) {
				getWishesThatHasNotBeenNotifiedFor(user, next.parallel());
			});
		},
		// Iteratate through wishlist courses and see if there are open classes
		function(wishesThatHasNotBeenNotifiedFor, next) {
			if (wishesThatHasNotBeenNotifiedFor) {
				wishesThatHasNotBeenNotifiedFor = wishesThatHasNotBeenNotifiedFor.filter(function(n) { return n != null; }); 
			}

			if (!wishesThatHasNotBeenNotifiedFor || !wishesThatHasNotBeenNotifiedFor.length || wishesThatHasNotBeenNotifiedFor.length === 0) {
				logger.info('No wishesThatHasNotBeenNotifiedFor found, exiting');
				process.exit(code=0);
				return;
			}

			logger.info('Found ' + wishesThatHasNotBeenNotifiedFor.length + ' wishes that has not been notified for');

			wishesThatHasNotBeenNotifiedFor.forEach(function (wishesForUser) {
				wishesForUser.forEach(function(wish) {
					getCoursesWithOpenClasses(wish, next.parallel());
				});
			});
		},
		// Set has notified for wishlist for users
		function (wishesWithOpenClasses, next) {

			logger.info('Found ' + wishesWithOpenClasses.length + " wishes with open classes");

			wishesWithOpenClasses.forEach(function(wish) {
				setHasNotifiedForUser(wish, next.parallel());
			});
		},
		// Send email to users
		function (wishes, next) {
			wishes.forEach(function(wish) {
				sendEmailToUser(wish, next.parallel());
			});
		},
		function (mailSendingResult, next) {
			logger.info('Done sending emails');
			process.exit(code=1);
		},
		], function(error) {
			logger.fatal('An error occoured, exiting');
			logger.fatal(error);
			process.exit(code=1);
		});
}

function setHasNotifiedForUser(wish, callback) {
	if (!wish || !wish.courseID || !wish.userID) {
		callback(null, null);
		return;
	}

	if (config.isDevelopment === true) {
		logger.info('Not marking as already notified in development mode');
		callback(null, wish);
		return;
	}

	var query = { };
	query['wishlist.' + wish.courseID] = { 'hasNotifiedUser': true, date: new Date() };

	db.users.findAndModify({
		query: { 'email': wish.email },
		update: { '$set': query }
	}, function(err, doc, lastErrorObject) {
		callback(err, wish);
	}); 
}

function getCoursesWithOpenClasses(wish, callback) {
	var query = {};
	query['src.id'] = wish.courseID;
	query['classesInfo.duration.begin'] = { $gte: moment().format('YYYY-MM-DD') };
	query['classesInfo.participants.remaining'] = {$not:{$eq:0}};
	query.classesInfo = { $exists: true };

	db.courses.findOne(query, function(err, doc) {
		if (!doc) {
			callback(null, null);
			return;
		}

		wish.course = doc;
		callback(err, wish);
	});
}

function getWishesThatHasNotBeenNotifiedFor(user, callback) {
	var wishes = [];

	if (!user || !user.wishlist) {
		callback(null, null);
		return;
	}

	for (var key in user.wishlist) {
		var wish = user.wishlist[key];

		if (!wish.hasNotifiedUser) {
			wishes.push({ userID: user.id, email: user.email, name: user.name, courseID: key });
		}
	}

	callback(null, wishes);
}

function getUsersWithWishlistRemindersEnabled(callback) {
	var usersWithWishlistRemindersEnabled = [];

	db.users.find({ 'notifications.wishlistCourses': true }).forEach(function(err, doc) {
		if (err) {
			callback(err, null);
			return;
		}
		
		if (!doc) {
			callback(err, usersWithWishlistRemindersEnabled);
			return;
		}

		var result = { id: doc._id, name: doc.name, email: doc.email, wishlist: doc.wishlist };
		usersWithWishlistRemindersEnabled.push(result);
	});
}

function getCourseTitle(id, callback) {
	db.courses.findOne({ '_id': id }, function(err, doc) {
		if (err) {
			callback(error, doc);
			return;
		}

		if (!doc) {
			callback(err, null);
			return;
		}

		if (!doc.src || !doc.src.title) {
			callback(null, null);
			return;
		}

		callback(null, doc.src.title);
	});
}

function sendEmailToUser(wish, callback) {
	if (!wish) {
		callback(null, null);
		return;
	}

	logger.info('Sending email to user ' + wish.name + " - " + wish.email);

	getCourseTitle(wish.courseID, function(error, courseTitle) {
		if (!courseTitle) {
			callback(new Error('Course title could not be found for course id ' + wish.courseID));
		}

		console.log(util.inspect(wish));
		var courseLink = 'https://evukurser.dk/kursus/' + wish.courseID;

		if (config.isDevelopment !== true) {

			return mailProvider.sendEmail({
				name: wish.name,
				email: wish.email,
				template: 'wishlistOpenClasses'
			}, {
				subject: 'Nye hold på AMU-kursus: ' + courseTitle,
				name: wish.name,
				courseTitle: courseTitle,
				courseLink: courseLink
			}, callback);
		} else {
			logger.info('Not sending email in development mode');
			/*console.log(util.inspect({
				name: wish.name,
				email: wish.email,
				courseTitle: courseTitle,
				courseLink: courseLink,
				template: 'wishlistOpenClasses'
			}));*/
			/*console.log(util.inspect({
				subject: 'Nye hold på AMU-kursus: ' + courseTitle,
				name: wish.name
			}));*/
			callback(null, null);
			return;
		}

		callback(null, null);
	});
}

module.exports.start = start;
module.exports.sendEmailToUser = sendEmailToUser;
