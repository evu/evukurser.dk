// Modules
var config = require('./config');
var util = require('util');
var common = require('common');
var db = require('../server/db');
var mailProvider = require('../server/email/provider');
var moment = require('moment');


// Logging
var log4js = require('log4js');
var logger = log4js.getLogger('ExpiredCertificateReminder');

start();

function start() {
	var result = {};

	common.step([ 
		// Get array of users with certificate reminders enabled
		function(next) {
			logger.info('Expired Certificate reminder service started');
			getUsersWithCertificateRemindersEnabled(next);
		},
		// Get expired certificates for users
		function(usersWithCertificateRemindersEnabled, next) { 
			usersWithCertificateRemindersEnabled = usersWithCertificateRemindersEnabled.filter(function(n) { return n != null; });

			if (!usersWithCertificateRemindersEnabled || !usersWithCertificateRemindersEnabled.length || usersWithCertificateRemindersEnabled.length === 0) {
				logger.info('No users with certificate reminders enabled found and complated classes data, exiting');
				return process.exit(code=0);
			}

			logger.info('Found ' + usersWithCertificateRemindersEnabled.length + ' users with certificate reminders enabled');

			usersWithCertificateRemindersEnabled.forEach(function(user) {
				getExpiredCertificate(user, next.parallel()); 
			});
		},
		// Get course titles from certificates
		function(expiredCertificates, next) {
			expiredCertificates = expiredCertificates.filter(function(n) { return n != null; }); 

			if (!expiredCertificates || !expiredCertificates.length || expiredCertificates.length === 0) {
				logger.info('No expired certificates found, exiting');
				process.exit(code=0);
				return;
			}

			logger.info('Found ' + expiredCertificates.length + ' expired certificates');
			result.totalNumberOfCertificates = expiredCertificates.length;

			expiredCertificates.forEach(function (certificate) {
				getCourseTitlesFromCertificate(certificate, next.parallel());
			});
		},
		// Set has notified for users
		function (expiredCertificates, next) {
			expiredCertificates.forEach(function (certificate) {
				hasSentNotification(certificate, next.parallel()); 				
			});
		},
		// Send email to users
		function (expiredCertificates, next) {
			logger.info('Have not notified about ' + (result.totalNumberOfCertificates - expiredCertificates.length) + '/' + result.totalNumberOfCertificates + ' certificates');

			expiredCertificates.forEach(function (certificate) {
				if (!certificate.doc) {
					logger.info('Sending mail to ' + certificate.user.email);
					sendEmailToUser(certificate, next.parallel());
				}
			});
		},
		function (mailSendingResult, next) {
			logger.info('Done sending emails, setting has notified: ' + mailSendingResult);
			process.exit(code=1);
		},
		], function(error) {
			logger.fatal('An error occoured, exiting');
			logger.fatal(error);
			process.exit(code=1);
		});
}

function getUsersWithCertificateRemindersEnabled(callback) {
	var usersWithCertificateRemindersEnabled = [];

	db.users.find({ 'notifications.certificateExpiration': true }).forEach(function(err, doc) {
		if (err) {
			callback(err, null);
			logger.error(err);
			return;
		}
		
		if (!doc) {
			callback(err, usersWithCertificateRemindersEnabled);
			return;
		}

		if (doc.cpr && doc.cpr.completedClasses && doc.cpr.completedClasses.items) {
			var completedClasses = doc.cpr.completedClasses.items;

			completedClasses.forEach(function(schoolClass) {
				if (schoolClass && schoolClass.courses) {
					schoolClass.courses.forEach(function(course) {
						var user = {
							userID: doc._id,
							name: doc.name,
							email: doc.email,
							completedDate: course.duration[0].end,
							schoolClassID: schoolClass.id,
							courseID: course.id
						};

						usersWithCertificateRemindersEnabled.push(user);
					});
				}
			});
		}
	});
}

function getExpiredCertificate(user, callback) {
	if (!user.name || !user.email || !user.completedDate) {
		callback(new Error('Could not get expired certificates: missing one or more parameters'));
		return;
	}

	var name = user.name;
	var email = user.email;
	var completedDate = moment(user.completedDate);

	return getCourseWithCertificates(user.courseID, function(error, course) {
		if (error) {
			callback(error);
			return;
		}

		if (course) {
			var monthsBeforeCertificateExpire = null;
			var daysBeforeToNotify = null;
			var newCourseID = null;
			var oldCourseID = null;

			if (course.duration && course.duration.months) { 
				monthsBeforeCertificateExpire = course.duration.months;
			}

			if (course.notification && course.notification.days) {
				daysBeforeToNotify = course.notification.days;
			}

			if (course.newCourse && course.newCourse.id) {
				newCourseID = course.newCourse.id;
			}

			if (course.oldCourse && course.oldCourse.id) {
				oldCourseID = course.oldCourse.id;
			}

			var expireDate = completedDate.add(monthsBeforeCertificateExpire, 'M');
			var now = moment();
			var duration = moment.duration(now.diff(expireDate));
			var days = duration.asDays();

			if (days - daysBeforeToNotify <= 0) {
				var expiredCertificate = {
					user: user,
					course: course
				};

				callback(null, expiredCertificate);
				return;
			}
		}

		callback(null, null);
	});
}

function getCourseWithCertificates(courseID, callback) {
	db.courses.findOne({ '_id': courseID, 'overwrites.certificates': { $exists: true } }, function(err, doc) {
		if (err) {
			callback(error, doc);
			return;
		}
		
		if (!doc) {
			callback(err, null);
			return;
		}

		var certificates = doc.overwrites.certificates;
		certificates.oldCourse = { id: doc._id };
		callback(null, certificates);
	});
}

function sendEmailToUser(certificate, callback) {
	console.log(util.inspect(certificate));

	if (!certificate) {
		callback(new Error('Could not send email, certificate parameter was null'));
		return;
	}

	var user = certificate.user;

	if (!user) {
		callback(new Error('Could not send email, user parameter was null'));
		return;
	}

	var name = user.name;
	var email = user.email;
	var expireMonth = getExpireMonth(user.completedDate);
	var expireYear = getExpireYear(user.completedDate);

	var course = certificate.course;

	if (!course) {
		callback(new Error('Could not send email, course was null'));
		return;
	}

	var oldCourse = course.oldCourse;

	if (!oldCourse) {
		callback(new Error('Could not send email, oldCourse was null'));
		return;
	}

	var oldCourseTitle = oldCourse.title;

	var newCourse = course.newCourse;

	if (!newCourse) {
		callback(new Error('Could not send email, newCourse was null'));
		return;
	}

	var newCourseTitle = newCourse.title;
	var newCourseID = newCourse.id;

	if (!newCourseID) {
		callback(new Error('Could not send email, missing newCourseID'));
		return;
	}

	var newCourseURL = "https://evukurser.dk/kursus/" + newCourseID;

	if (!name || !email || !oldCourseTitle || !newCourseTitle || !newCourseID  || !expireMonth || !expireYear || !newCourseURL) {
		callback(new Error('Could not send email: missing one or more parameters'));
		return;
	}

	if (config.isDevelopment !== true) {
		return setHasNotifiedForCourse(certificate, function(error, value) {
			return mailProvider.sendEmail({
				name: name,
				email: email,
				template: 'certificateExpired'
			}, {
				subject: 'Certifikat er ved at udløbe på EVUKurser.dk',
				name: name,
				oldCourseTitle: oldCourseTitle,
				expireMonth: expireMonth,
				expireYear: expireYear,
				newCourseID: newCourseID,
				newCourseTitle: newCourseTitle,
				newCourseURL: newCourseURL
			}, callback); 
		});
	}



	logger.info('Not sending mail in development mode');
	/*
	logger.info(util.inspect({
				name: name,
				email: email,
				oldCourseTitle: oldCourseTitle,
				expireMonth: expireMonth,
				expireYear: expireYear,
				newCourseID: newCourseID,
				newCourseTitle: newCourseTitle,
				newCourseURL: newCourseURL,
				template: 'certificateExpired'
			}));
	logger.info(util.inspect({
				subject: 'Certifikat er ved at udløbe på EVUKurser.dk',
				name: name
			}));*/
}

function getCourseTitlesFromCertificate(certificate, callback) {
	if (!certificate || !certificate.course) {
		callback(null, null);
		return;
	}

	var oldCourse = certificate.course.oldCourse;
	var newCourse = certificate.course.newCourse;	 

	if (!oldCourse) {
		callback(new Error('Could not get oldCourse title, oldCourse was null'));
		return;
	}

	if (!oldCourse.id) {
		callback(new Error('Could not get oldCourse title, missing oldCourse id'));
		return;
	}

	if (!newCourse) {
		callback(new Error('Could not get newCourse title, newCourse was null'));
		return;
	}

	if (!newCourse.id) {
		callback(new Error('Could not get newCourse title, missing newCourse id'));
		return;
	}

	getCourseTitle(oldCourse, function(error, oldCourseWithTitle) {
		if (error) {
			callback(error);
			return;
		}

		if (oldCourseWithTitle) {
			certificate.oldCourse = oldCourseWithTitle;

			getCourseTitle(newCourse, function(error, newCourseWithTitle) {
				if (error) {
					callback(error);
					return;
				}

				if (newCourseWithTitle) {
					certificate.newCourse = newCourseWithTitle;
					callback(null, certificate);
				} else {
					callback(null, null);
				}
			});
		} else {
			callback(null, null);
		}
	});
}

function getExpireMonth(date) {
	var month = null;

	try {
		month = moment(date, 'YYYY-MM-DD', 'da').format("MMMM");
	} catch(err) {
		// Intentionally left blank
	}

	return month;
}

function getExpireYear(date) {
	var year = null;

	try {
		year = moment(date, 'YYYY-MM-DD').format('YYYY');
	} catch(err) {
		// Intentionally left blank
	}

	return year;
}

function getCourseTitle(course, callback) {
	if (!course) {
		callback(new Error('Could not get course title, course was null'));
		return;
	}

	if (!course.id) {
		callback(new Error('Could not get course title, missing course id'));
		return;
	}

	db.courses.findOne({ '_id': course.id }, function(err, doc) {
		if (err) {
			callback(error, doc);
			return;
		}

		if (!doc) {
			callback(err, null);
			return;
		}

		if (!doc.src || !doc.src.title) {
			callback(null, null);
			return;
		}

		course.title = doc.src.title;
		callback(null, course);
	});
}

function setHasNotifiedForCourse(certificate, callback) {
	db.certificateEmailReminders.insert({ 
		oldCourseID: certificate.oldCourse.id,
		newCourseID: certificate.newCourse.id,
		userID: certificate.user.userID, 
		email: certificate.user.email, 
		schoolClassID: certificate.user.schoolClassID,
		emailLastSent: new Date() 
	}, function(error, doc) {
		if (error || !doc) {
			return callback(new Error('Could not setHasNotifiedForCourse: ' + error));
		}

		return callback(null, certificate);
	});
}


function hasSentNotification(certificate, callback) {
	db.certificateEmailReminders.findOne({ 
		schoolClassID: certificate.user.schoolClassID, 
		userID: certificate.user.userID, 
	}, function(error, doc) {
		if (error) {
			callback(error, doc);
			return;
		}

		certificate.doc = doc;
		callback(error, certificate);
	});
}

exports.start = start;
exports.sendEmailToUser = sendEmailToUser;