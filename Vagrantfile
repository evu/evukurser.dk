# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  # Every Vagrant virtual environment requires a box to build off of.
  # Here we use Ubuntu 14.04 LTS 64-bit.
  config.vm.box = "puphpet/ubuntu1404-x64"

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine.
  config.vm.network "forwarded_port", guest: 3000, host: 5000

  # Use a non-login shell. Fixes a minor issue, see:
  # https://github.com/mitchellh/vagrant/issues/1673#issuecomment-28288042
  config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"

  # This is a development box, so setup hostname accordingly.
  config.vm.hostname = "dev.evukurser.dk"

  # Message to developer.
  config.vm.post_up_message = <<-msg
    Login to your box with `vagrant ssh`.
    The first time you need to setup the project:

      cd dev
      npm install

    And then run the server:

      npm start

    After that you can open http://localhost:5000/ on your host machine.
  msg

  # Setup a name for this box.
  config.vm.define "evukurser_dev"
  config.vm.provider :virtualbox do |vb|
    vb.name = "evukurser_dev_virtualbox"
  end

  # Install some Puppet modules.
  config.vm.provision "shell", :inline => "puppet module install puppetlabs-apt 2>/dev/null || true"

  # Enable provisioning with Puppet stand alone.  Puppet manifests
  # are contained in a directory path relative to this Vagrantfile.
  # You will need to create the manifests directory and a manifest in
  # the file default.pp in the manifests_path directory.
  config.vm.provision "puppet" do |puppet|
    # puppet.options        = "--verbose --debug"
    puppet.module_path    = "server/puppet/modules"
    puppet.manifests_path = "server/puppet/manifests"
    puppet.manifest_file  = "site.pp"
  end
end
