// Generated by CoffeeScript 1.7.1
(function() {
  var app;

  app = angular.module('ratings', []);

  app.directive("angularRatings", function() {
    return {
      restrict: 'E',
      scope: {
        model: '=ngModel',
        notifyId: '=notifyId',
        token: '=notifyToken',
        user_id: '=uid'
      },
      replace: true,
      transclude: true,
      template: '<div><ol class="angular-ratings">' + '<li ng-class="{active:model>0,over:over>0}">1</li>' + '<li ng-class="{active:model>1,over:over>1}">2</li>' + '<li ng-class="{active:model>2,over:over>2}">3</li>' + '<li ng-class="{active:model>3,over:over>3}">4</li>' + '<li ng-class="{active:model>4,over:over>4}">5</li>' + '</ol></div>',
      controller: [
      '$scope', '$attrs', '$http', function($scope, $attrs, $http) {
        $scope.over = 0;
        $scope.over = $scope.model;

        $scope.resetOver = function() {
          $scope.setOver($scope.model);
        };

        $scope.setRating = function(rating) {
          $scope.model = rating;
          $scope.$apply();
          if ($attrs.notifyUrl !== void 0 && $scope.notifyId && $scope.token) {
            return $http({
              method: 'PUT',
              'url': '/api/v1/courses/' + $scope.notifyId + '/rating/' + $scope.user_id,
              data: { rating: rating },
              headers: {'Authorization': 'Bearer ' + $scope.token }
            }).error(function(data) {
              return $scope.model = 0;
            }).success(function(data) {
              $scope.hasRatingSet = true;
              $scope.over = data.rating.stars;
              $scope.model = data.rating.stars;
            });
          }
        };

        return $scope.setOver = function(n) {
          $scope.over = n;
          return $scope.$apply();
        };
      }
      ],
      link: function(scope, iElem, iAttrs) {
        if (iAttrs.notifyUrl !== void 0) {
          return angular.forEach(iElem.children(), function(ol) {
            return angular.forEach(ol.children, function(li) {
              li.addEventListener('mouseover', function() {
                return scope.setOver(parseInt(li.innerHTML));
              });
              li.addEventListener('mouseout', function() {
                return scope.resetOver();
              });
              return li.addEventListener('click', function() {
                return scope.setRating(parseInt(li.innerHTML));
              });
            });
          });
        }
      }
    };
  });

}).call(this);