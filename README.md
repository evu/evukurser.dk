evukurser.dk      
============

## Om projektet (DK)
Dette projekt er udviklet af [Rise Digital](http://risedigital.dk) for [EVU](evu.dk), støttet af TUP 2013. Se projektets evaluering [her](http://www.emu.dk/sites/default/files/Slutrapport%20for%20evaluering%20af%20den%20Tv%C3%A6rga%CC%8Aende%20Udviklingspulje%202013.pdf)

Projektet er udviklet som open source. Derfor kan andre, herunder andre faglige udvalg eller skoler, frit kopiere siden (dog er grafisk identitet ejet af EVU). Nedenfor gives der instrukser til hvordan det kan gøres.

Ønskes mere information kan man kontakte evu@evu.dk, eller Navid fra Rise Digital.

Når projektet først er installeret på lokal server, kan [brugsvejledningen](http://evu.dk/sites/default/files/Vejledning%20til%20EVUKURSER.docx) være relevant.

## About (EN)
This project was developed by [Rise Digital](http://risedigital.dk) for [EVU](evu.dk).
Questions about the business case and functionality can be sent to [info@risedigital.dk](info@risedigital.dk).

## Requirements
To run this project, the following software needs to be installed:
- Node.js v 0.10
- gulp
- mongodb


## Setup

First time setup:

```bash
npm install
```

Run server (default on http://localhost:3000):

```bash
npm start
```

You can specify the server port to use (and whether to run in production or not):

```bash
PORT=8080 npm start --production
```

Build files using gulp (listen on files changes and continuesly build files):

```bash
gulp
```

You can also build all files once:

```
gulp build
```

If you want to clean up the build folder, then do:

```
gulp clean
gulp build
```
