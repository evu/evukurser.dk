/** @module app/directives/coursecard-type */

'use strict';

module.exports = function() {
	return { 
		link: function(scope, element, attrs) {
			var courseTypes = require('../enums/courseTypes');
			var cssClass;
			if (courseTypes[attrs.coursetype]) {
				cssClass = courseTypes[attrs.coursetype].cssClass;
			} else {
				cssClass = courseTypes.other.cssClass;
			}
			element.addClass(cssClass);
		}
	};
};