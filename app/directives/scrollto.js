/** @module app/directives/scrollTo */

'use strict';

module.exports = function () {
	var $ = require('jquery');

	return function (scope, element, attr) {
		element.click(function() {
			$('html, body').animate({
				scrollTop: $(attr.scrollto).offset().top
			}, 500);
		});
	};
};

