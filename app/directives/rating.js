/** @module app/directives/rating */

'use strict';

module.exports = ['AuthService', function(AuthService) {
	
	return {
		restrict: 'AE',
		replace: 'true',
		templateUrl: '/partials/rating.html',
		link: function(scope) {
			var auth = AuthService.getAuth();
			if (auth) {
				scope.access_token = auth.access_token;
				scope.uid = auth.user.id;
			}
		}
	};
}];


