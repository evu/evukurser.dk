/** @module app/directives/coursecard */

'use strict';

module.exports = ['WishlistService', 'AuthService', function(WishlistService, AuthService) {
    return {
        restrict: 'AE',
        replace: 'true',
        templateUrl: '/partials/coursecard.html',
        link: function(scope) {
            var auth = AuthService.getAuth();
            var moment = require('moment');

            if (scope.course && scope.course.description && scope.course.description.text) {
                scope.course.description.text = decodeLinks(scope.course.description.text);
            }

            scope.hasLinks = function() {
                return (scope.course.links && scope.course.links.length !== 0);
            };

            scope.isLoggedIn = function() {
                return (auth && auth.access_token);
            };

            scope.courseIsInWishlist = function() {
                return (scope.course.wishlist && scope.isLoggedIn() && scope.hasWishlistEnabled() && scope.course.wishlist);
            };

            scope.courseIsNotInWishlist = function() {
                return (!scope.course.wishlist && scope.isLoggedIn() && scope.hasWishlistEnabled() && !scope.course.wishlist);
            };

            scope.addToWishlist = function(courseID) {
                WishlistService.add(auth.user.id, courseID, auth.access_token, function( /* result */ ) {
                    scope.course.wishlist = true;
                    if (scope.refreshCourses) {
                        scope.refreshCourses();
                    }
                });
            };

            scope.removeFromWishlist = function(courseID) {
                WishlistService.delete(auth.user.id, courseID, auth.access_token, function( /* result */ ) {
                    scope.course.wishlist = null;
                    if (scope.refreshCourses) {
                        scope.refreshCourses();
                    }
                });
            };

            scope.hasCompendiumsEnabled = function() {
                if (scope.showCompendiums && scope.hasLinks()) {
                    return true;
                }
                return false;
            };

            scope.hasWishlistEnabled = function() {
                if (scope.showWishlist) {
                    return true;
                }
                return false;
            };



            scope.getLink = function() {
                var link = '/kursus/' + scope.course.id + scope.getFromDate() + scope.getToDate() + scope.getIsNotOpenForAttendance() + scope.getIsWarrantyCourse() + scope.getRegion() + scope.getSchool();
                return link;
            };


            scope.getFromDate = function() {
                var date = null;

                if (scope.searchParams && scope.searchParams['start-date']) {
                    date = scope.searchParams['start-date'];
                }

                if (date && moment(date).isValid()) {
                    return "/fra/" + date;
                }

                return "";
            };
            scope.getToDate = function() {
                var date = null;

                if (scope.searchParams && scope.searchParams['end-date']) {
                    date = scope.searchParams['end-date'];
                }

                if (date && moment(date).isValid()) {
                    return "/til/" + date;
                }

                return "";
            };

            scope.getIsNotOpenForAttendance = function() {
                if (scope.searchParams && scope.searchParams['signup-open'] === false) {
                    return "/vis-ikke-aaben-for-tilmelding";
                }

                return "";
            };

            scope.getIsWarrantyCourse = function() {
                if (scope.searchParams && scope.searchParams['guaranteed'] === true) {
                    return "/garantikursus";
                }

                return "";
            };

            scope.getSchool = function() {
                if (scope.searchParams && scope.searchParams['school'] && scope.searchParams['school'].length > 0) {
                    return "/skole/" + scope.searchParams['school'];
                }

                return "";
            };

            scope.getRegion = function() {
                if (scope.searchParams && scope.searchParams['region'] && scope.searchParams['region'].length > 0) {
                    return "/region/" + scope.searchParams['region'];
                }

                return "";
            };

            function decodeLinks(text) {
                var replacedText = null;
                var replacePattern1 = null;

                // URL's starting with http://, https://
                replacePattern1 = /(\b(https?):\/\/[-A-Z0-9+&amp;@#\/%?=~_|!:,.;]*[-A-Z0-9+&amp;@#\/%=~_|])/ig;
                replacedText = text.replace(replacePattern1, function(a, b) {
                    return decodeURIComponent(b);
                });

                return replacedText;
            }

        }
    };
}];
