'use strict';

module.exports = function() {
    var $ = require('jquery');

    return {
        restrict: 'A',
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {
            $(function(){
                element.datepicker({
                    dateFormat: 'dd/mm/yy',
                    dayNamesMin: [ "Sø", "Ma", "Ti", "On", "To", "Fr", "Lø" ],
                    monthNames: [ "Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December" ],
                    firstDay: 1,
                    weekHeader: "Uge",
                    onSelect: function (date) {
                        scope.$apply(function () {
                            ngModelCtrl.$setViewValue(date);
                        });
                    }
                });
            });
        }
    };
};