'use strict';

module.exports = function() {    
	return {
		require: 'ngModel',
		link: function(scope, element, attr) {          
			element.bind("change", function(/* event */) {
				readFile(element[0], function(result) {
					scope[attr.ngFileSelect] = result;
				});
			}); 

			function readFile(input, callback) {
				if (input.files && input.files[0]) {
					var FR = new FileReader();

					FR.onload = function(event) {
						callback(event.target.result);
					};
					FR.readAsText(input.files[0]);
				}
			}
		}
	};
};
