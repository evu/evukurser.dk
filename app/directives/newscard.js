/** @module app/directives/newscard */

'use strict';

module.exports = function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: '/partials/newscard.html'
  };
};