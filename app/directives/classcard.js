/** @module app/directives/classcard */

'use strict';

module.exports = function() {
	return {
		restrict: 'AE',
		replace: 'true',
		templateUrl: '/partials/classcard.html',
		link:  function ($scope) {
			var moment = require('moment');
			$scope.formatedDate = 'd. ' + moment($scope.schoolClass.duration.begin).lang("da").format('Do MMMM, YYYY');

			$scope.classHasPicture = function() {
				return false;
			};	
		}
	};
};