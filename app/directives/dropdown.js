/** @module app/directives/dropdown */

'use strict';

module.exports = function() {
	var $ = require('jquery');

	return {
		restrict: 'A',
		link: function(scope, element) {
			setTimeout(function() {
				$(element).dropdown();
			}, 10);				
		}
	};
};
