'use strict';

module.exports = function() {
  return {
    link: function(scope, element, attrs) {
      scope.$watch(attrs.focusinput, function(value) {
        if(value === true) { 
            element[0].focus();
            scope[attrs.focusinput] = false;
        }
      });
    }
  };
};