/** @module app/directives/completed-oursecard */

'use strict';

module.exports = function() {
	return {
		restrict: 'AE',
		replace: 'true',
		templateUrl: '/partials/completed-coursecard.html',
		link: function(scope) {
			var moment = require('moment');

			scope.hasLinks = function() {
				 return (scope.course.links && scope.course.links.length !== 0);
			};

			scope.formatDate = function(date) {
				return moment(date).lang("da").format('Do MMMM, YYYY');
			};

			scope.hasCompendiumsEnabled = function() {
				if (scope.showCompendiums && scope.hasLinks()) {
					return true;
				}
				return false;
			};
		}
	};
};