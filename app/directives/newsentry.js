/** @module app/directives/newsentry */

'use strict';

module.exports = function() {
	return {
		restrict: 'AE',
		replace: 'true',
		templateUrl: '/partials/newsentry.html',
		link:  function (scope) {
			var moment = require('moment');
			scope.formatedDate = 'd. ' + moment(scope.story.created).lang("da").format('Do MMMM, YYYY');	
		}
	};
};