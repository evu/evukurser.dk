'use strict';

var angular = require('angular');

module.exports = function() {
  return {
    restrict: 'E',
    templateUrl: '/partials/login.html',
    link: function(scope, element/*, attrs */) {
      scope.show = true;

      scope.$on('event:unauthorized', function(/* event */) {
        scope.show = true;
      });

      scope.$on('event:authenticated', function(/* event */) {
        scope.show = false;
      });

      var button = angular.element(element.find('button'));
      button.bind('click', function(){
        scope.$emit('event:authenticate', scope.username,
          scope.password);
      });
    }
  };
};