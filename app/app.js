/** @module app/app */
'use strict';

// Base setup
require('./dev'); // setup dev environment, automatically ignored in production
require('semantic-ui'); // load SemanticUI components
require('jquery-ui'); // load jQuery-ui components

// Angular setup
var angular = require('angular');
var app = angular.module('evu', ['ui.router', 'ratings']);

// Routes
app.config(require('./config/routes'));

// Interceptors
app.config(require('./interceptors/loginInterceptor'));

// Angular services
app.service("AuthService", require('./services/AuthService'));
app.service("CourseService", require('./services/CourseService'));
app.service("NewsService", require('./services/NewsService'));
app.service("ContactService", require('./services/ContactService'));
app.service("NewsAdminService", require('./services/NewsAdminService'));
app.service("FileUploadService", require('./services/FileUploadService'));
app.service("NemIDService", require('./services/NemIDService'));
app.service("WishlistService", require('./services/WishlistService'));
app.service("SearchService", require('./services/SearchService'));


// Angular controllers
app.controller("PageController", require('./controllers/PageController'));
app.controller("FrontPageController", require('./controllers/FrontPageController'));
app.controller("CourseOverviewController", require('./controllers/CourseOverviewController'));
app.controller("CourseController", require('./controllers/CourseController'));
app.controller("VerifyEmailController", require('./controllers/VerifyEmailController'));
app.controller("ContactController", require('./controllers/ContactController'));
app.controller("ProfileController", require('./controllers/ProfileController'));
app.controller("LoginController", require('./controllers/LoginController'));
app.controller("MyPageController", require('./controllers/MyPageController'));
app.controller("NewsOverviewController", require('./controllers/NewsOverviewController'));
app.controller("NewsController", require('./controllers/NewsController'));
app.controller("NewsAdminController", require('./controllers/NewsAdminController'));
app.controller("ForgottenPasswordController", require('./controllers/ForgottenPasswordController'));
app.controller("ResetPasswordController", require('./controllers/ResetPasswordController'));
app.controller("LogoutController", require('./controllers/LogoutController'));
app.controller("CompletedCoursesController", require('./controllers/CompletedCoursesController'));
app.controller("FileUploadController", require('./controllers/FileUploadController'));
app.controller("CompendiumController", require('./controllers/CompendiumController'));
app.controller("NewsLetterController", require('./controllers/NewsLetterController'));
app.controller("NemIdController", require('./controllers/NemIdController'));
app.controller("CoursePackageController", require('./controllers/CoursePackageController'));
app.controller("SubstituteController", require('./controllers/SubstituteController'));
app.controller("ReminderController", require('./controllers/ReminderController'));
app.controller("WishlistController", require('./controllers/WishlistController'));
app.controller("VejledningController", require('./controllers/VejledningController'));

// Angular directives
app.directive('coursecard', require('./directives/coursecard'));
app.directive('completedcoursecard', require('./directives/completed-coursecard'));
app.directive('coursetype', require('./directives/coursecard-type'));
app.directive('dropdown', require('./directives/dropdown'));
app.directive('datepicker', require('./directives/datepicker'));
app.directive('focusinput', require('./directives/focusinput'));
app.directive('newscard', require('./directives/newscard'));
app.directive('login', require('./directives/login'));
app.directive('ngenter', require('./directives/ngenter'));
app.directive('newsentry', require('./directives/newsentry'));
app.directive('classcard', require('./directives/classcard'));
app.directive('ngConfirmClick', require('./directives/ngConfirmClick'));
app.directive('rating', require('./directives/rating'));
app.directive('scrollto', require('./directives/scrollto'));
app.directive('ngFileSelect', require('./directives/ngFileSelect'));

module.exports = app;