'use strict';

module.exports = ['$rootScope', '$http', function(scope, $http) {
	var $ = require('jquery');
	var auth = null;

	scope.$on('event:refreshAuth', function(/* event, username, password */) {
		var refresh_token = localStorage.getItem('refresh_token');

		if (refresh_token !== null) {
			var payload = {
				grant_type: 'refresh_token',
				refresh_token: refresh_token,
				client_id: 'evukurser.dk'
			};

			authenticate(payload, setUser);
		}
	});

	scope.$on('event:authenticate', function(event, username, password) {
		var payload = {
			username: username,
			password: password,
			grant_type: 'password',
			client_id: 'evukurser.dk' 
		};

		localStorage.setItem("grant_type", payload.grant_type);
		authenticate(payload, setUser);
	});

	this.refreshAuth = function(callback) {
		var refresh_token = localStorage.getItem('refresh_token');

		if (refresh_token !== null) {
			var payload = {
				grant_type: 'refresh_token',
				refresh_token: refresh_token,
				client_id: 'evukurser.dk'
			};

			authenticate(payload, callback);
		} else {
			callback(null);
		}
	};

	this.login = function(username, password, callback) {
		var payload = {
			username: username,
			password: password,
			grant_type: 'password',
			client_id: 'evukurser.dk' 
		};

		try {
			localStorage.setItem("grant_type", payload.grant_type);
		}
		catch (e) {	}

		authenticate(payload, function(data) {
			setUser(data);
			callback(data);
		});
	};

	this.updateProfile = function(user, uid, access_token, callback) {
		$http({
			method: 'PUT',
			url: '/api/v1/users/' + uid,
			data: user,
			headers: {'Authorization': 'Bearer ' + access_token}
		}).success(function(data) {
			callback(data);
		}).error(function(data) {
			callback(data);
		});
	};

	this.forgottenPassword = function(email, callback) {
		$http({
			method: 'POST',
			url: '/api/v1/users/reset-password',
			data: $.param({'email': email }),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(data) {
			callback(data);
		}).error(function(data) {
			callback(data);
		});
	};

	this.resetPassword = function(password, id, callback) {
		$http({
			method: 'POST',
			url: '/api/v1/users/reset-password/' + id,
			data: $.param({'password': password }),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(data) {
			callback(data);
		}).error(function(data) {
			callback(data);
		});
	};

	this.logout = function(callback) {
		try {
			localStorage.removeItem("grant_type");
		} catch (e) {

		}

		try {
			localStorage.removeItem("refresh_token");
		} catch (e) {

		}

		auth = null;
		scope.$broadcast('event:loggedIn', false);
		callback(true);
	};

	function authenticate(payload, callback) {
		$http({
			method: 'POST',
			url: '/api/v1/auth/oauth2/token',
			data: $.param(payload),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(data) {
			try {
				localStorage.setItem('refresh_token', data.refresh_token);
			}
			catch (e) { }

			setUser(data);
			scope.$broadcast('event:loggedIn', true);
			callback(data);
		}).error(function(data) {
			scope.$emit('event:loggedIn', false);
			callback(data);
		});
	}

	this.createUser = function(user, callback) {
		$http({
			method: 'POST',
			url: '/api/v1/users',
			data: $.param(user),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(data) {
			scope.$emit('event:loggedIn', true);
			callback(data);
		}).error(function(data) {
			callback(data);
		});
	};

	function setUser(data) {
		auth = data;
	}

	this.getAuth = function() {
		return auth;
	};
}];
