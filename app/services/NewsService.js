'use strict';

module.exports = ['$http', '$sce', function ($http, $sce) {
    var markdown = require('marked');

    this.get = function(param, callback) {
        $http({
            method: 'get',
            url: '/api/v1/news',
            params: param
        }).success(function(stories) {
            for (var story in stories.items) {
                stories.items[story].article.html = $sce.trustAsHtml(markdown(stories.items[story].article.markdown));
            }

            callback(stories);
        }).error(function(/* stories */) {
            callback(null);
        });
    };

    this.getByID = function (id, callback) {
        var promise = $http.get("/api/v1/news/" + id, {
            method: 'GET'
        });

        promise.then(function(result) {
            var story = result.data;
            story.article.html = $sce.trustAsHtml(markdown(story.article.markdown));
            callback(story);
        }, function(/* reason */) {
            callback(null);
        });
    };
}];