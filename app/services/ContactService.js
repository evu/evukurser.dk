'use strict';

var $ = require('jquery');

module.exports = ['$rootScope', '$http', function(scope, $http) {
	this.put = function(name, email, message, callback) {
		$http({
			method: 'POST',
			url: '/api/v1/contact',
			data: $.param({'name': name, 'email': email, 'message': message }),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(data) {
			callback(data);
		}).error(function(data) {
			callback(data);
		});
	};
}];