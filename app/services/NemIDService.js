'use strict';

module.exports = ['$http', function ($http) {
    this.getApplet = function(userid, cpr, access_token, callback) {
        $http({
            method: 'POST',
            'url': '/api/v1/users/' + userid + '/cpr',
            data: { 'cpr': cpr },
            headers: {'Authorization': 'Bearer ' + access_token }
        }).success(function(data) {
            callback(data);
        }).error(function(data) {
            callback(data);
        });
    }; 

    this.put = function(userid, token, access_token, response, callback) {
       $http({
            method: 'POST',
            'url': '/api/v1/users/' + userid + '/cpr/' + token,
            data: response,
            headers: {'Authorization': 'Bearer ' + access_token }
        }).success(function(data) {
            callback(data);
        }).error(function(data) {
            callback(data);
        }); 
    };

    this.delete = function(userid, access_token, callback) {
       $http({
            method: 'DELETE',
            'url': '/api/v1/users/' + userid + '/cpr',
            headers: {'Authorization': 'Bearer ' + access_token }
        }).success(function(data) {
            callback(data);
        }).error(function(data) {
            callback(data);
        }); 
    };
}];