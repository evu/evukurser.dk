'use strict';

module.exports = ['$http', function ($http) {

    this.get = function(uid, access_token, callback) {
        $http({
            method: 'GET',
            url: '/api/v1/users/' + uid + '/wishlist',
            headers: {'Authorization': 'Bearer ' + access_token}
        }).success(function(data) {
            callback(data);
        }).error(function(data) {
            callback(data);
        });
    }; 

    this.add = function (uid, courseID, access_token, callback) {
        $http({
            method: 'POST',
            url: '/api/v1/users/' + uid + '/wishlist',
            data: { id: courseID },
            headers: {'Authorization': 'Bearer ' + access_token}
        }).success(function(data) {
            callback(data);
        }).error(function(data) {
            callback(data);
        });
    };

    this.delete = function (userID, courseID, access_token, callback) {
	    $http({
            method: 'DELETE',
            url: '/api/v1/users/' + userID + '/wishlist/' + courseID,
            data: {},
            headers: {'Authorization': 'Bearer ' + access_token}
        }).success(function(data) {
            callback(data);
        }).error(function(data) {
            callback(data);
        });
    };
}];