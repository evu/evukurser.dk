'use strict';

module.exports = ['$http', function($http) {
    this.get = function(param, access_token, callback) {
        var header = null;

        if (access_token) {
            header = {
                'Authorization': 'Bearer ' + access_token
            };
        }

        $http({
            method: 'get',
            url: '/api/v1/courses',
            headers: header,
            params: param
        }).success(function(result) {
            callback(result);
        }).error(function( /* result */ ) {
            callback(null);
        });
    };

    this.getByID = function(id, param, access_token, callback) {
        var header = null;

        if (access_token) {
            header = {
                'Authorization': 'Bearer ' + access_token
            };
        }

        $http({
            method: 'get',
            url: "/api/v1/courses/" + id,
            headers: header,
            params: param
        }).success(function(result) {
            callback(result);
        }).error(function( /* result */ ) {
            callback(null);
        });
    };

    this.getOptions = function(param, callback) {
        var promise = $http.get("/api/v1/options", {
            method: 'get',
            params: param
        });

        promise.then(function(result) {
            // Adding "Vis alle"-option to top of school dropdown
            if (result.data && result.data.school && result.data.school.items) {
                result.data.school.items.unshift({
                    id: null,
                    name: 'Vis alle'
                });
            }
            callback(result.data);
        }, function() {

        });
    };

    this.getCompletedCourses = function(userid, access_token, callback) {
        $http({
            method: 'GET',
            'url': '/api/v1/users/' + userid + '/classes/completed',
            headers: {
                'Authorization': 'Bearer ' + access_token
            }
        }).success(function(data) {
            callback(data);
        }).error(function( /* data */ ) {
            callback(null);
        });
    };
}];
