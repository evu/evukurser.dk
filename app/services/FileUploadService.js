'use strict';

module.exports = ['$http', function ($http) {
    this.put = function(fileName, content, access_token, callback) {
        $http({
            method: 'PUT',
            'url': '/api/v1/backend/data/' + fileName,
            data: content,
            headers: {'Authorization': 'Bearer ' + access_token,
                'Content-Type': 'text/csv; charset=UTF-8'
            }
        }).success(function(data) {
            callback(data);
        }).error(function(data) {
            callback(data);
        });
    }; 
}];