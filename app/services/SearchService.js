'use strict';

module.exports = function() {
	var searchParams = null;
	var searchOptions = null;
	var searchOptionParams = null;

	this.getSearchParams = function() {
		return searchParams;
	};

	this.setSearchParams = function(params) {
		searchParams = params;
		// searchParams.offset = 0;
	};

	this.getSearchOptions = function() {
		return searchOptions;
	};

	this.setSearchOptions = function(options) {
		searchOptions = options;
	};

	this.setSearchOptionParams = function(options) {
		searchOptionParams = options;
	};

	this.getSearchOptionParams = function() {
		return searchOptionParams;
	};
};