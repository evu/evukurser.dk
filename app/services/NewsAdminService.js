'use strict';

module.exports = ['$http', function ($http) {
	this.addOrUpdate = function(news, access_token, callback) {
		var method = 'POST';
		var id = '';
		if (news.hasOwnProperty('id')) {
			method = 'PUT';
			id = '/' + news.id;
		}

		$http({
			'method': method,
			url: '/api/v1/news' + id,
			data: news,
			headers: {'Authorization': 'Bearer ' + access_token}
		}).success(function(data) {
			callback(data);
		}).error(function(data) {
			callback(data);
		});
	};

	this.delete = function(id, access_token, callback) {
		$http({
			'method': 'DELETE',
			url: '/api/v1/news/' + id,
			headers: {'Authorization': 'Bearer ' + access_token}
		}).success(function(data) {
			callback(data);
		}).error(function(data) {
			callback(data);
		});
	};
}];