/** @module app/enums/loginErrors */
'use strict';

module.exports = 
{ 
	'invalid_request' : {
		showMessageFromServer: true
	},
	'not_found' : {
		message: 'Der opstod en fejl under kommunikation med serveren',
		showMessageFromServer: false
	},
	'server_error' : {
		message: 'Der opstod en fejl under kommunikation med serveren',
		showMessageFromServer: false
	},
	'invalid_grant' : {
		showMessageFromServer: true
	},
	'invalid_client' : {
		message: 'Der opstod en fejl under kommunikation med serveren',
		showMessageFromServer: false
	},
	'unsupported_grant_type' : {
		message: 'Der opstod en fejl under kommunikation med serveren',
		showMessageFromServer: false
	},
	'conflict' : {
		showMessageFromServer: true
	},
	'not_verified' : {
		showMessageFromServer: true
	},
	'email_failed' : {
		showMessageFromServer: true
	}
};







