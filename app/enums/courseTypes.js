/** @module app/enums/courseTypes */
'use strict';

module.exports = 
{ 
	'plumbing' : {
		'cssClass': 'course-card-green',
		'name': 'VVS',
		'id': 'plumbing'
	},
	'electrical' : {
		'cssClass': 'course-card-blue',
		'name': 'El',
		'id': 'electrical'
	},
	'chimney' : {
		'cssClass': 'course-card-yellow',
		'name': 'Skorstensfejer',
		'id': 'chimney'
	},
	'private_arrangements' : {
		'cssClass': 'course-card-black',
		'name': 'Private arrangementer',
		'id': 'private_arrangements'
	},
	'other' : {
		'cssClass': 'course-card-black',
		'name': 'Andre',
		'id': 'other'
	}
};