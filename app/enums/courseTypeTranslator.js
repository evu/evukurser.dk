/** @module app/enums/courseTypeTranslator */
'use strict';

module.exports = 
{ 
	'vvs' : 'plumbing',
	'el' : 'electrical',
	'skorstensfejer' : 'chimney',
	'private-arrangementer' : 'private_arrangements',
	'andre-udbydere' : 'other'
};