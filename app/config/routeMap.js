/** @module app/config/routeMap */
'use strict';

module.exports = 
{ 
	'frontpage' : {
		templateUrl: '../pages/frontpage.html',
		controller: 'FrontPageController',
		url: '/'
	},
	'kontakt' : {
		templateUrl: '../pages/contact.html',
		controller: 'ContactController',
		url: '/kontakt'
	},
	'kursusoversigt' : {
		templateUrl: '../pages/course_overview.html',
		controller: 'CourseOverviewController',
		url: '/kursusoversigt'
		//url: '/kursusoversigt{options:\/*.*?}'
	},
	'kursusoversigt-type' : {
		templateUrl: '../pages/course_overview.html',
		controller: 'CourseOverviewController',
		url: '/kursusoversigt/:type'
	},
	'tilskud' : {
		templateUrl: '../pages/substitute.html',
		controller: 'SubstituteController',
		url: '/kursus/:id/tilskud'
	},
	'tillaeg' : {
		templateUrl: '../pages/apprenticeships.html',
		url: '/kursus/:id/tillaeg'
	},
	'coursepackagelist' : {
		controller: 'CoursePackageController',
		templateUrl: '../pages/course_package_list.html',
		url: '/kursuspakke/:coursePackage'
	},
	'kursus-id' : {
		templateUrl: '../pages/course.html',
		controller: 'CourseController',
		url: '/kursus/:id{options:\/*.*?}'
	},
	'vejledning' : {
		templateUrl: '../pages/vejledning.html',
		controller: 'VejledningController',
		url: '/vejledning'
	},  
	'login' : {
		templateUrl: '../pages/login.html',
		controller: 'LoginController',
		url: '/login'
	},
	'logout' : {
		controller: 'LogoutController',
		url: '/log-ud'
	},
	'login.glemt-password' : {
		templateUrl: '../partials/login/forgotten_password.html',
		controller: 'ForgottenPasswordController',
		parent: 'login',
		url: '/glemt-kodeord'
	},
	'reset-password' : {
		templateUrl: '../partials/login/reset_password.html',
		controller: 'ResetPasswordController',
		url: '/nulstil-adgangskode/:id'
	},
	'min-side' : {
		templateUrl: '../pages/my_page.html',
		controller: 'MyPageController',
		url: '/min-side'
	},
	'min-side.completed-courses' : {
		templateUrl: '../partials/my-page/completed_courses.html',
		controller: 'CompletedCoursesController',
		parent: 'min-side',
		url: '/gennemfoerte-kurser'
	},
	'min-side.wishlist' : {
		templateUrl: '../partials/my-page/wishlist.html',
		controller: 'WishlistController',
		parent: 'min-side',
		url: '/oenskeliste'
	},
	'min-side.profile' : {
		templateUrl: '../partials/my-page/profile.html',
		controller: 'ProfileController',
		url: '/profil',	
		title: 'Profil',
		parent: 'min-side'
	},
	'min-side.paamindelser' : {
		templateUrl: '../partials/my-page/reminders.html',
		controller: 'ReminderController',
		url: '/paamindelser',	
		title: 'Påmindelser',
		parent: 'min-side'
	},
	'min-side.nemid' : {
		templateUrl: '../partials/my-page/nemid.html',
		controller: 'NemIdController',
		url: '/valider-nemid',	
		title: 'Valider NemID',
		parent: 'min-side'
	},
	'nyheder' : {
		templateUrl: '../pages/news-overview.html',
		controller: 'NewsOverviewController',
		url: '/nyheder'
	},
	'nyhed' : {
		templateUrl: '../pages/news.html',
		controller: 'NewsController',
		url: '/nyhed/:id'
	},
	'min-side.kompendier' : {
		templateUrl: '../partials/my-page/compendium.html',
		controller: 'CompendiumController',
		parent: 'min-side',
		url: '/kompendier/:id'
	},
	'min-side.newsletter' : {
		templateUrl: '../partials/my-page/newsletter.html',
		controller: 'NewsLetterController',
		parent: 'min-side',
		url: '/nyhedsbrev'
	},
	'min-side.skriv-nyhed' : {
		templateUrl: '../pages/backend/news_write.html',
		controller: 'NewsAdminController',
		parent: 'min-side',
		url: '/backend/skriv-nyhed'
	},
	'min-side.upload-csv' : {
		templateUrl: '../pages/backend/upload_csv.html',
		controller: 'FileUploadController',
		parent: 'min-side',
		url: '/backend/upload-csv'
	},
	'min-side.skriv-nyhed-id' : {
		templateUrl: '../pages/backend/news_write.html',
		controller: 'NewsAdminController',
		parent: 'min-side',
		url: '/backend/skriv-nyhed/:id'
	},
	'min-side.rediger-nyhed' : {
		templateUrl: '../pages/backend/news_edit.html',
		controller: 'NewsAdminController',
		parent: 'min-side',
		url: '/backend/rediger-nyhed'
	},
	'min-side.slet-nyhed' : {
		templateUrl: '../pages/backend/news_admin.html',
		controller: 'NewsAdminController',
		parent: 'min-side',
		url: '/backend/slet-nyhed'
	},
	'verificer-email' : {
		templateUrl: '../pages/verify-email.html',
		controller: 'VerifyEmailController',
		url: '/verificer-email/:type'
	}
};