/** @module app/config/routes */
'use strict';

var routesByPath = require('./routeMap');

/**
 * Route handling.
 * If a given request matches a URI, a templateUrl and controller will be served accordingly.
 * If there is no matches, the frontpage will be shown.
 * @param {AngularProvider} $routeProvider - Provider for Angular in module ngRoute, used for configuring routes.
 * @param {AngularProvider} $locationProvider - Provider for Angular in module ng, configures how application deep linking paths are stored.
 */
 module.exports = ['$stateProvider', '$locationProvider', '$urlRouterProvider', function($stateProvider, $locationProvider, $urlRouterProvider) {
   $locationProvider
   .html5Mode(true)
   .hashPrefix('!');

  $urlRouterProvider.otherwise('/');

  for (var key in routesByPath) { 
    var value = routesByPath[key];
    $stateProvider.state(key, value);
  }
}];
