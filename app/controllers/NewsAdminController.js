'use strict';

var $ = require('jquery');

module.exports = ['$scope', '$sce', '$state', 'AuthService', 'NewsAdminService', 'NewsService', function($scope, $sce, $state, AuthService, NewsAdminService, NewsService) { 
	$scope.newsArticle = {};
	$scope.errors = {};
	$scope.successes = {};
	var id = $state.params.id;

	if (id) {
		NewsService.getByID(id, function(story) {
			$scope.story = story;
			$scope.title = story.title;
			$scope.newsArticle.title = story.title;
			$scope.article = story.article.markdown;
			$scope.newsArticle['article-markdown'] = story.article.markdown;
			$scope.newsArticle.id = story.id;

			if (story.image) {
				$scope.image = story.image.normal;
			}
		});
	} else {
		var markdown = require('marked');

		NewsService.get({}, function(stories) {
			$scope.newsStories = stories.items;

			for (var x in stories.items) {
				var html = stories.items[x].article.markdown.substring(0, 100) + ' ...';

				stories.items[x].article.html = $sce.trustAsHtml(markdown(html));
			}
		});
	}

	$scope.$watch('title', function(newValue/*, oldValue */) {
		$scope.newsArticle['title'] = newValue;
	});

	$scope.$watch('article', function(newValue/*, oldValue */) {
		$scope.newsArticle['article-markdown'] = newValue;
	});

	$scope.postNews = function() {
		$scope.errors = {};
		$scope.successes = {};
		var auth = AuthService.getAuth();
		var access_token = auth.access_token;
		var name = auth.user.name;
		var email = auth.user.email;
		$scope.newsArticle.name = name;
		$scope.newsArticle.email = email;

		NewsAdminService.addOrUpdate($scope.newsArticle, access_token, function(response) {
			if (response.hasOwnProperty('error')) {
				$scope.errors.add = response.error_description;
			} else {
				if ($scope.newsArticle.hasOwnProperty('id')) { 
					$scope.successes.add = "Nyheden blev opdateret";
				} else {
					$scope.successes.add = "Nyheden blev tilføjet";
				}
			}
		});

	};

	$scope.deleteNews = function() {
		var auth = AuthService.getAuth();
		var access_token = auth.access_token;

		NewsAdminService.delete($scope.story.id, access_token, function(response) {
			if (response.hasOwnProperty('error')) {
				$scope.errors.add = response.error_description;
			} else {
				$scope.successes.add = "Nyheden blev slettet";
			}
		});
	};

	function readImage(input) {
		if ( input.files && input.files[0] ) {
			var FR= new FileReader();
			FR.onload = function(e) {
				$('#img').attr( "src", e.target.result );
				$scope.newsArticle.image = e.target.result;
			};       
			FR.readAsDataURL( input.files[0] );
		}
	}

	$("#asd").change(function(){
		readImage( this );
	});

	$scope.showNewsStory = function(id) {
		$state.go('min-side.skriv-nyhed-id', { 'id': id });
	};

}];