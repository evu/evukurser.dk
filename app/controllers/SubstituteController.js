/** @module app/controllers/SubstituteController */

'use strict';

module.exports = ["$scope", "$state", function($scope, $state) {
	$scope.$parent.combineMenuBar = false;
	$scope.id = $state.params.id;
}];