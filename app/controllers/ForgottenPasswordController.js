/** @module app/controllers/ForgottenPasswordController */

'use strict';

module.exports = ["$scope", "$state", "AuthService", function($scope, $state, AuthService) {
	$scope.sendVerification = function() {
		$scope.errorMessage = null;
		$scope.success = false;

		AuthService.forgottenPassword($scope.email, function(response) {
			if (response.hasOwnProperty('error')) {
					showError(response);
			} else {
				$scope.errorMessage = null;
				$scope.success = true;
			}
		});
	};

	function showError(data) {
		$scope.success = false;
		var loginErrors = require('../enums/loginErrors');
		var error = loginErrors[data.error];
		var message = '';
		if (error.showMessageFromServer === true) {
			message = data.error_description;
		} else {
			message = error.message;
		}	

		$scope.errorMessage = message;
	}
}];