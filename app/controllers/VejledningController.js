'use strict';

module.exports = ["$scope", "$state", '$location', function($scope, $state, $location) {
	$scope.$parent.combineMenuBar = false;

	$scope.goBack = function() {
		window.history.go(-1);
	};
}];