/** @module app/controllers/CoursePackageController */

'use strict';

module.exports = ["$scope", '$state', '$sce', "CourseService", function($scope, $state, $sce, CourseService) {
	$scope.course = {};
	var coursePackage = $state.params.coursePackage;
	$scope.coursePackage = coursePackage;

	$scope.courses = CourseService.get({ limit: 10 }, function(courses) {
		$scope.courses = courses.items;
	});

}];

