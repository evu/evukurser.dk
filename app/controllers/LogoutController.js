/** @module app/controllers/LogoutController */

'use strict';

module.exports = ["$scope", "$state", "AuthService", function($scope, $state, AuthService) {
	AuthService.logout(function() {
		window.location.href='/';
	});
}];