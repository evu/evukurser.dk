/** @module app/controllers/NewsLetterController */

'use strict';

module.exports = [function() {
	window.fnames = ['EMAIL', 'FNAME', 'LNAME'];
	window.ftypes = ['email', 'text', 'text'];
}];

