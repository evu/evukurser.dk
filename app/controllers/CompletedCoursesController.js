/** @module app/controllers/CompletedCoursesController */

'use strict';

module.exports = ["$scope", '$state', '$sce', "AuthService", "CourseService", function($scope, $state, $sce, AuthService, CourseService) {
	$scope.completedCourses = {};

	var auth = AuthService.getAuth();
	var userid = auth.user.id;
	var access_token = auth.access_token;

	CourseService.getCompletedCourses(userid, access_token, function(courses) {
		if ($scope.courses.count > 0) {
			$scope.completedCourses = courses.items;
		}
	});
}];