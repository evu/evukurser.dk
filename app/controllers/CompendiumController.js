/** @module app/controllers/CompendiumController */

'use strict';

module.exports = ["$scope", '$state', '$sce', "CourseService", function($scope, $state, $sce, CourseService) {
	$scope.course = {};
	var id = $state.params.id;

	CourseService.getByID(id, {}, null, function(course) {
		$scope.course = course.data;
	});

	$scope.hasLinks = function() {
		return ($scope.course.links && $scope.course.links.length !== 0);
	};
}];

