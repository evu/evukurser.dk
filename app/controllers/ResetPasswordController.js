/** @module app/controllers/LoginController */

'use strict';

module.exports = ["$scope", "$state", "AuthService", function($scope, $state, AuthService) {
	$scope.$watch("user.password", function(newValue, oldValue) {
		if (oldValue != newValue) {
			if (newValue.length >= 6) {
				$scope.user.passwordIsValid = true;
			} else {
				$scope.user.passwordIsValid = false;
			}
		}
	});

	$scope.$watch("user.passwordRepeated", function(newValue, oldValue) {
		if (oldValue != newValue) {
			if (newValue.length > 0 && newValue == $scope.user.password && $scope.user.passwordIsValid === true) {
				$scope.user.passwordRepeatedIsValid = true;
			} else {
				$scope.user.passwordRepeatedIsValid = false;
			}
		}
	});

	$scope.resetPassword = function() {
		var id = $state.params.id;
		$scope.errorMessage = null;
		$scope.success = false;

		if ($scope.user.passwordIsValid === true && $scope.user.passwordRepeatedIsValid === true) {

			AuthService.resetPassword($scope.user.password, id, function(result) {
				if (result.hasOwnProperty('error')) {
					showLoginError(result);
				} else {
					$scope.success = true;
				}
			});
		}
		//loginErrorMessage
	};

	function showLoginError(data) {
		var loginErrors = require('../enums/loginErrors');
		var error = loginErrors[data.error];
		var message = '';
		if (error.showMessageFromServer === true) {
			message = data.error_description;
		} else {
			message = error.message;
		}	

		$scope.errorMessage = message;
	}
}];