/** @module app/controllers/NewsController */

'use strict';

module.exports = ["$scope", "$state", "NewsService", function($scope, $state, NewsService) {
	$scope.$parent.combineMenuBar = false;
	var id = $state.params.id;
	var moment = require('moment');

	NewsService.getByID(id, function(story) {
		$scope.story = story;
		$scope.formatedDate = 'd. ' + moment($scope.story.created.time).lang("da").format('Do MMMM, YYYY');
	});
}];