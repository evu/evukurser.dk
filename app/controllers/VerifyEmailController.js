/** @module app/controllers/VerifyEmailController */

'use strict';

module.exports = ["$scope", "$state", function($scope, $state) {
	var type = $state.params.type;

	$scope.$on('$viewContentLoaded', function() {
		if (type == "ny-bruger") {
			$scope.message = "Din nye bruger er nu blevet aktiveret.";
		} else if (type == "ny-email") {
			$scope.message = 'Din nye e-mail er nu blevet aktiveret.';
		}
	});
}];
