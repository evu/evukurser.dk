/** @module app/controllers/ContactController */

'use strict';

module.exports = ["$scope", "ContactService", function($scope, ContactService) {
	$scope.$parent.combineMenuBar = true;

	$scope.send = function() {
		$scope.success = false;
		$scope.errorMessage = null;

		ContactService.put($scope.name, $scope.email, $scope.message, function(response) {
			if (response.hasOwnProperty('error')) {
					showError(response);
				} else {
					$scope.success = true;
				}
		});
	};

	function showError(data) {
		var loginErrors = require('../enums/loginErrors');
		var error = loginErrors[data.error];
		var message = '';
		if (error.showMessageFromServer === true) {
			message = data.error_description;
		} else {
			message = error.message;
		}	

		$scope.errorMessage = message;
	}
}];