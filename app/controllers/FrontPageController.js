/** @module app/controllers/FrontPageController */
'use strict';

module.exports = ["$scope", "$http", "CourseService", "NewsService", function($scope, $http, CourseService, NewsService) {
	$scope.$parent.combineMenuBar = true;
	$scope.showIE9Msg = false;

	var courseTypes = require('../enums/courseTypes');

	CourseService.get({ type: courseTypes.plumbing.id, limit: 3 }, null, function(courses) {
		$scope.highlightedVVS = courses.items;
	});

	CourseService.get({ type: courseTypes.chimney.id, limit: 3 }, null, function(courses) {
		$scope.highlightedSkorstensfejer = courses.items;
	});

	CourseService.get({ type: courseTypes.electrical.id, limit: 3 }, null, function(courses) {
		$scope.highlightedEl = courses.items;
	});

	$scope.newsStories = {};

	NewsService.get({ limit: 10 }, function(stories) {
		var i;

		if (stories.items.length >= 5) {
			$scope.newsStories.firstRow = [];

			for (i = 0; i < 5; i++) {
				$scope.newsStories.firstRow.push(stories.items[i]);
			}

			if (stories.items.length >= 10) {
				$scope.newsStories.secondRow = [];

				for (i = (stories.items.length - 1); i > 4; i--) {
					$scope.newsStories.secondRow.push(stories.items[i]);
				}
			}
		}		
	});

	$scope.getCourseTypeClass = function(courseType) {
		return courseTypes[courseType].cssClass;
	};







	//Message for those with a IE9 Browser
	$scope.getBrowserVersion = function() 
	{
	  if(navigator && navigator.userAgent)
	  {
	  	var myNav = navigator.userAgent.toLowerCase();

	  	if(parseInt(myNav.split('msie')[1]) === 9)
	  	{
	  		$scope.showIE9Msg = true;	
	  	}	  	
	  }
	}



	$scope.hideIE9Msg = function()
	{
		$scope.showIE9Msg = false;
	}

	$scope.getBrowserVersion();

	

}];
