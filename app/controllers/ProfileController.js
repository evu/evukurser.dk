/** @module app/controllers/ProfileController */

'use strict';

module.exports = ["$scope", "AuthService", function($scope, AuthService) {
	$scope.user = $scope.$parent.user;
	$scope.changedUser = {};
	$scope.cprVerified = false;
	var $ = require('jquery');
	$scope.isLoading = false;
	// id =>class

	$scope.$watch("changedUser.name", function(newValue, oldValue) {
		if (oldValue != newValue) {
			if (newValue.length >= 6) {
				$scope.changedUser.passwordIsValid = true;
			} else {
				$scope.changedUser.passwordIsValid = false;
			}
		}
	});

	$scope.$watch("changedUser.email", function(newValue, oldValue) {
		if (oldValue != newValue) {
			if (/^[A-Z-Z0-9._%+\-]+@[A-Z0-9.\-]+\.[A-Z]{2,20}$/i.test(newValue)) {
				$scope.changedUser.emailIsValid = true;
			} else {
				$scope.changedUser.emailIsValid = false;
			}
		}
	});


	$scope.$watch("changedUser.newPassword", function(newValue, oldValue) {
		if (oldValue != newValue) {
			if (newValue.length >= 6) {
				$scope.changedUser.passwordIsValid = true;
			} else {
				$scope.changedUser.passwordIsValid = false;
			}
		}
	});

	$scope.$watch("changedUser.newPasswordRepeated", function(newValue, oldValue) {
		if (oldValue != newValue) {
			if (newValue.length > 0 && newValue == $scope.changedUser.newPassword && $scope.changedUser.passwordIsValid === true) {
				$scope.changedUser.passwordRepeatedIsValid = true;
			} else {
				$scope.changedUser.passwordRepeatedIsValid = false;
			}
		}
	});

	$scope.updateProfile = function() {
		$scope.errorMessage = null;
		var uid = $scope.user.id;
		var access_token = AuthService.getAuth().access_token;
		readImage($('#asd'));
		$scope.isLoading = true;
		
		AuthService.updateProfile($scope.changedUser, uid, access_token, function(response) {
			$scope.isLoading = false;

			if (response.hasOwnProperty('error')) {
				showError(response);
			} else {
				$scope.$parent.user = response;
				$scope.user = response;
			}
		});
	};

	function showError(data) {
		var loginErrors = require('../enums/loginErrors');
		var error = loginErrors[data.error];
		var message = '';
		if (error.showMessageFromServer === true) {
			message = data.error_description;
		} else {
			message = error.message;
		}	

		$scope.errorMessage = message; 
	}

	$scope.$on('$viewContentLoaded', function() {
		$scope.changedUser.emailIsValid = true;
		$scope.changedUser.passwordIsValid = true;
		$scope.changedUser.passwordRepeatedIsValid = true;
	});

	function readImage(input) {
		if (input.files && input.files[0]) {
			var FR = new FileReader();
			FR.onload = function(e) {
				$('.profile-picture-selector-image').attr( "src", e.target.result );
				$scope.changedUser.picture = e.target.result;
			};       
			FR.readAsDataURL( input.files[0] );
		}
	}

	$(".profile-picture-selector").change(function(){
		readImage( this );
	});
}];