/** @module app/controllers/CourseController */

'use strict';

module.exports = ["$scope", '$state', '$sce', "AuthService", "CourseService", "WishlistService", function($scope, $state, $sce, AuthService, CourseService, WishlistService) {
    $scope.$parent.combineMenuBar = false;
    $scope.course = null;

    var auth = null;
    var access_token = null;
    var params = {};
    var optionsSplit = $state.params['options'].split('/');

    $scope.$on('$viewContentLoaded', function() {
        auth = AuthService.getAuth();

        if (auth && auth.access_token) {
            access_token = auth.access_token;
        }

        CourseService.getByID($state.params.id, params, access_token, function(course) {
            $scope.course = course;
            $scope.cssTypeClass = $scope.getCourseTypeColor();

            if ($scope.course && $scope.course.description && $scope.course.description.text) {
                var descriptionWithLinks = convertToLinks($scope.course.description.text);
                var descriptionWithNewlines = convertNewlinesToHTML(descriptionWithLinks);
                $scope.course.description.text = $sce.trustAsHtml(descriptionWithNewlines);
            }

        });
    });

    function convertNewlinesToHTML(text) {
        return text.replace(/[ \t]+/g, ' ').replace(/[ \t]+\n|\n[ \t]+/g, '<br>').replace(/\n+/g, '<br><br>');
    }

    function convertToLinks(text) {
        var replacedText = null;
        var replacePattern1 = null;

        // URL's starting with http://, https://
        replacePattern1 = /(\b(https?):\/\/[-A-Z0-9+&amp;@#\/%?=~_|!:,.;]*[-A-Z0-9+&amp;@#\/%=~_|])/ig;
        replacedText = text.replace(replacePattern1, function(a, b) {
        	return '<a href="' + b + '" target="_blank">' + decodeURIComponent(b) + '</a>';
    	});

        return replacedText;
    }

    if (optionsSplit && optionsSplit.length > 0) {
        var length = optionsSplit.length;

        for (var i = 0; i < length; i++) {
            var key = optionsSplit[i];

            if (key.length > 0) {
                if (length > (i + 1)) {
                    var value = optionsSplit[i + 1];
                    if (value.length > 0) {
                        if (key == "fra") {
                            params['start-date'] = value;
                        } else if (key == "til") {
                            params['end-date'] = value;
                        } else if (key == "region") {
                            params['region'] = value;
                        } else if (key == "skole") {
                            params['school'] = value;
                        }
                    }
                }

                if (key == "vis-ikke-aaben-for-tilmelding") {
                    params['signup-open'] = false;
                } else if (key == "garantikursus") {
                    params['guaranteed'] = true;
                }
            }
        }
    }

    $scope.courseHasGrant = function() {
        var courseTypes = require('../enums/courseTypes');
        return !!($scope.course && $scope.course.type && courseTypes[$scope.course.type.id] === courseTypes.electrical);
    };

    $scope.courseHasApprenticeship = function() {
        return !!($scope.course && $scope.course.apprenticeships);
    };

    $scope.courseHasGrantOrApprenticeships = function() {
        return ($scope.courseHasGrant() || $scope.courseHasApprenticeship());
    };

    $scope.cssTypeClass = "";

    $scope.getCourseTypeColor = function() {
        var courseTypes = require('../enums/courseTypes');
        var cssClass = "";

        if ($scope.course !== null && courseTypes[$scope.course.type.id]) {
            cssClass = courseTypes[$scope.course.type.id].cssClass + '-top-bar';
        }

        return cssClass;
    };

    $scope.goBack = function() {
        window.history.go(-1);
    };

    $scope.addToWishlist = function() {
        WishlistService.add(auth.user.id, $state.params.id, auth.access_token, function( /* result */ ) {
            $scope.course.wishlist = true;
        });
    };

    $scope.removeFromWishlist = function() {
        WishlistService.delete(auth.user.id, $state.params.id, auth.access_token, function( /* result */ ) {
            $scope.course.wishlist = null;
        });
    };

    $scope.isLoggedIn = function() {
        return !!(auth && !auth.hasOwnProperty('error') && auth.user);
    };

    $scope.hasNoClasses = function() {
        return ($scope.course && (!$scope.course.classes || $scope.course.classes.length == 0));
    }

    $scope.courseIsInWishlist = function() {
        return ($scope.hasNoClasses() && $scope.course && $scope.course.wishlist && $scope.isLoggedIn() && $scope.course.wishlist);
    };

    $scope.courseIsNotInWishlist = function() {
        return ($scope.hasNoClasses() && $scope.course && !$scope.course.wishlist && $scope.isLoggedIn() && !$scope.course.wishlist);
    };
}];
