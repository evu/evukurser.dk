/** @module app/controllers/FileUploadController */

'use strict';

module.exports = ["$scope", "AuthService", "FileUploadService", function($scope, AuthService, FileUploadService) {
	$scope.responses = {};

	$scope.uploadCompendium = function() {
		var fileName = 'links.csv';
		$scope.responses.compendium = {};
		
		uploadFile(fileName, $scope.compendiumContent, function(response) {
			if (response.hasOwnProperty("error")) {
				$scope.responses.compendium.error = response.error_description;
			} else {
				if (response.warnings === true) {
					$scope.responses.compendium.warning = true;
				}
				$scope.responses.compendium.warning = response.warnings;
				$scope.responses.compendium.success = "Filen blev uploaded";
				$scope.responses.compendium.successMessage = response.message;
			}
		});
	};

	$scope.uploadStarred = function() {
		var fileName = 'starred.csv';
		var content = $scope.starredContent;
		$scope.responses.starred = {};

		uploadFile(fileName, content, function(response) {
			if (response.hasOwnProperty("error")) {
				$scope.responses.starred.error = response.error_description;
			} else {
				$scope.responses.starred.warning = response.warnings;
				$scope.responses.starred.success = "Filen blev uploaded";
				$scope.responses.starred.successMessage = response.message;
			}
		});
	};

	$scope.uploadJobareasPackages = function() {
		var fileName = 'jobareas.csv';
		var content = $scope.jobareasContent;
		$scope.responses.jobareas = {};

		uploadFile(fileName, content, function(response) {
			if (response.hasOwnProperty("error")) {
				$scope.responses.jobareas.error = response.error_description;
			} else {
				$scope.responses.jobareas.warning = response.warnings;
				$scope.responses.jobareas.success = "Filen blev uploaded";
				$scope.responses.jobareas.successMessage = response.message;				
			}
		});	
	};

	$scope.uploadSimpleArrangements = function() {
		var fileName = 'courses.csv';
		var content = $scope.simpleArrangementsContent;
		$scope.responses.simpleArrangements = {};

		uploadFile(fileName, content, function(response) {
			if (response.hasOwnProperty("error")) {
				$scope.responses.simpleArrangements.error = response.error_description;
			} else {
				$scope.responses.simpleArrangements.warning = response.warnings;
				$scope.responses.simpleArrangements.success = "Filen blev uploaded";
				$scope.responses.simpleArrangements.successMessage = response.message;				
			}
		});		
	};

	$scope.uploadAddonToSvendebrev = function() {
		var fileName = 'apprenticeships.csv';
		var content = $scope.svendebrevContent;
		$scope.responses.svendebrev = {};

		uploadFile(fileName, content, function(response) {
			if (response.hasOwnProperty("error")) {
				$scope.responses.svendebrev.error = response.error_description;
			} else {
				$scope.responses.svendebrev.warning = response.warnings;
				$scope.responses.svendebrev.success = "Filen blev uploaded";
				$scope.responses.svendebrev.successMessage = response.message;				
			}
		});		
	};

	$scope.uploadCertificates = function() {
		var fileName = 'certificates.csv';
		var content = $scope.certificatesContent;
		$scope.responses.certificates = {};

		uploadFile(fileName, content, function(response) {
			if (response.hasOwnProperty("error")) {
				$scope.responses.certificates.error = response.error_description;
			} else {
				$scope.responses.certificates.warning = response.warnings;
				$scope.responses.certificates.success = "Filen blev uploaded";
				$scope.responses.certificates.successMessage = response.message;				
			}
		});		
	};

	$scope.uploadCampaigns = function() {
		var fileName = 'campaigns.csv';
		var content = $scope.campaignsContent;
		$scope.responses.campaigns = {};

		uploadFile(fileName, content, function(response) {
			if (response.hasOwnProperty("error")) {
				$scope.responses.campaigns.error = response.error_description;
			} else {
				$scope.responses.campaigns.warning = response.warnings;
				$scope.responses.campaigns.success = "Filen blev uploaded";
				$scope.responses.campaigns.successMessage = response.message;
			}
		});
	};

	$scope.uploadCompetences = function() {
		var fileName = 'competences.csv';
		var content = $scope.competencesContent;
		$scope.responses.competences = {};

		uploadFile(fileName, content, function(response) {
			if (response.hasOwnProperty("error")) {
				$scope.responses.competences.error = response.error_description;
			} else {
				$scope.responses.competences.warning = response.warnings;
				$scope.responses.competences.success = "Filen blev uploaded";
				$scope.responses.competences.successMessage = response.message;
			}
		});
	};

	$scope.uploadMainSchools = function() {
		var fileName = 'mainschools.csv';
		var content = $scope.mainSchoolsContent;
		$scope.responses.mainSchools = {};

		uploadFile(fileName, content, function(response) {
			if (response.hasOwnProperty("error")) {
				$scope.responses.mainSchools.error = response.error_description;
			} else {
				$scope.responses.mainSchools.warning = response.warnings;
				$scope.responses.mainSchools.success = "Filen blev uploaded";
				$scope.responses.mainSchools.successMessage = response.message;
			}
		});
	};

	$scope.uploadThemes = function() {
		var fileName = 'themes.csv';
		var content = $scope.themesContent;
		$scope.responses.themes = {};

		uploadFile(fileName, content, function(response) {
			if (response.hasOwnProperty("error")) {
				$scope.responses.themes.error = response.error_description;
			} else {
				$scope.responses.themes.warning = response.warnings;
				$scope.responses.themes.success = "Filen blev uploaded";
				$scope.responses.themes.successMessage = response.message;
			}
		});
	};

	$scope.uploadExtraAmu = function() {
		var fileName = 'extraamu.csv';
		var content = $scope.extraAmuContent;
		$scope.responses.extraAmu = {};

		uploadFile(fileName, content, function(response) {
			if (response.hasOwnProperty("error")) {
				$scope.responses.extraAmu.error = response.error_description;
			} else {
				$scope.responses.extraAmu.warning = response.warnings;
				$scope.responses.extraAmu.success = "Filen blev uploaded";
				$scope.responses.extraAmu.successMessage = response.message;
			}
		});
	};

	function uploadFile(fileName, content, callback) {
		var access_token = AuthService.getAuth().access_token;

		FileUploadService.put(fileName, content, access_token, function(response) {
			callback(response);
		});

		var $ = require('jquery');
		$('input').each(function() { 
			$(this).wrap('<form>').closest('form').get(0).reset();
			$(this).unwrap(); 
		});
	}
}];

