/** @module app/controllers/WishlistController */

'use strict';

module.exports = ["$scope", '$state', '$sce', "AuthService", "WishlistService", function($scope, $state, $sce, AuthService, WishlistService) {
	var auth = AuthService.getAuth(); 
	$scope.courses = null;
	$scope.showWishlist = true;
	$scope.showRating = true;
	$scope.showRatingInEditMode = false;

	$scope.refreshCourses = function() {
		WishlistService.get(auth.user.id, auth.access_token, function(courses) {
			$scope.courses = courses.items;
		});
	};

	$scope.hasCourses = function() {
		if ($scope.courses) {
			return true;
		}
		return false;
	};

	$scope.refreshCourses();
}];