/** @module app/controllers/NewsOverviewController */

'use strict';

module.exports = ["$scope", "$state",'$sce', "NewsService", function($scope, $state, $sce, NewsService) {
	$scope.$parent.combineMenuBar = true;
	var $ = require('jquery');
	var db = require('just-debounce');
	$scope.numberOfNews = 0;
	$scope.newsStories = {};

	var newsParams = {
		'limit': 10,
		'offset': 0
	};

	var isLoadingNews = false;

	var updateNews = function(shouldExtendNewsStories) {
		if (isLoadingNews === false) {
			isLoadingNews = true;

			NewsService.get(newsParams, function(stories) {
				var markdown = require('marked');
				$scope.numberOfNews = stories.count;

				for (var x in stories.items) { 
					var html = htmlSubstring(markdown(stories.items[x].article.markdown), 200) + '...';
					stories.items[x].article.html = $sce.trustAsHtml(html);
				}

				if (shouldExtendNewsStories === true) {
					Array.prototype.push.apply($scope.newsStories.items, stories.items);
				} else {
					$scope.newsStories = stories;
				}
				
				isLoadingNews = false;
			});
		}
	};

	updateNews();

	$scope.showMore = db(function() {
		showMore();
	}, 100);

	var showMore = function() {
		if ((newsParams.limit + newsParams.offset) < $scope.numberOfNews) {
			newsParams.offset += 10;
			updateNews(true);
		}
	};

	$(window).scroll(function() {
		if($(window).scrollTop() + $(window).height() + 500  >= $(document).height()) {
			$scope.showMore();
		}
	});

	$scope.showNewsStory = function(id) {
		$state.go('nyhed', { 'id': id });
	};

	function htmlSubstring(s, n) {
		var m, r = /<([^>\s]*)[^>]*>/g,
		stack = [],
		lasti = 0,
		result = '';

	    //for each tag, while we don't have enough characters
	    while ((m = r.exec(s)) && n) {
	        //get the text substring between the last tag and this one
	        var temp = s.substring(lasti, m.index).substr(0, n);

	        //append to the result and count the number of characters added
	        result += temp;
	        n -= temp.length;
	        lasti = r.lastIndex;

	        if (n) {
	        	result += m[0];
	        	if (m[1].indexOf('/') === 0) {
	                //if this is a closing tag, than pop the stack (does not account for bad html)
	                stack.pop();
	            } else if (m[1].lastIndexOf('/') !== m[1].length - 1) {
	                //if this is not a self closing tag than push it in the stack
	                stack.push(m[1]);
	            }
	        }
	    }

	    //add the remainder of the string, if needed (there are no more tags in here)
	    result += s.substr(lasti, n);

	    //fix the unclosed tags
	    while (stack.length) {
	    	result += '</' + stack.pop() + '>';
	    }

	    return result;
	}
}];