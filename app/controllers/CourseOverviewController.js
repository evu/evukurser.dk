/** @module app/controllers/CourseOverviewController */

'use strict';

var angular = require('angular');

module.exports = ["$scope", "$state", "$location", "CourseService", "AuthService", function($scope, $state, $location, CourseService, AuthService) {
	var moment = require('moment');
	var db = require('just-debounce');
	var $ = require('jquery');
	var auth = AuthService.getAuth(); 
	var numberOfCourses = 10;
	var isOnLoad = true;
	var courseTypes = require('../enums/courseTypes');
	var courseTypeTranslator = require('../enums/courseTypeTranslator');
	$scope.$parent.combineMenuBar = false;
	$scope.numberOfCoursesShown = null;
	$scope.numberOfCourses = 0;
	$scope.searchParams = {};
	$scope.searchOptionParams = {};
	$scope.showRating = true;
	$scope.showWishlist = true;
	$scope.chosenJobArea = null;
	$scope.chosenRegion = null;
	
	$scope.searchParams = {
		'type': getType(),
		'offset': 0,
		'limit': numberOfCourses
	};

	$scope.branches = [{
		'cssClass': 'course-card-yellow',
		'name': 'El',
		'id': 'electrical'
	},
	{
		'cssClass': 'course-card-green',
		'name': 'VVS',
		'id': 'plumbing'
	},
	{
		'cssClass': 'course-card-blue',
		'name': 'Skorstensfejer',
		'id': 'chimney'
	},
	{
		'cssClass': 'course-card-black',
		'name': 'Private arrangementer',
		'id': 'private_arrangements'
	}
	];

	$scope.showMore = db(function() {
		showMore();
	}, 100);

	var showMore = function() {
		if (($scope.searchParams.limit + $scope.searchParams.offset) < $scope.numberOfCourses) {
			$scope.searchParams.offset += numberOfCourses;
			updateSearchResult(true);
		}
	};

	$(window).scroll(function() {
		if ( $(window).scrollTop() + 500 >= ( $(document).height() - $(window).height() ) ) {
			$scope.showMore();
		}
	});

	$scope.getNumberOfCoursesShown = function() {
		if ($scope.numberOfCoursesShown >= $scope.numberOfCourses) {
			return $scope.numberOfCourses;
		}

		return $scope.numberOfCoursesShown;
	}; 

	$scope.isLoggedIn = function() {
		return !!(auth && !auth.hasOwnProperty('error') && auth.user);
	};

	$scope.$on('$viewContentLoaded', function() {
		var searchObject = $location.search();

		if (searchObject.garantikursus === true) {
			$scope.sidebarIsWarrentyCourse = searchObject.garantikursus;
		} else {
			$scope.sidebarIsWarrentyCourse = false;
		}

		if (searchObject.tilmelding === true) {
			$scope.sidebarIsOpenForAttendance = true;
		} else {
			$scope.sidebarIsOpenForAttendance = false;
		}

		if (searchObject.kampagnekursus === true) {
			$scope.sidebarIsCompaignCourse = searchObject.kampagnekursus;
		} else {
			$scope.sidebarIsCompaignCourse = false;
		}

		if (searchObject.faellesudbud === true) {
			$scope.sidebarIncludeCommonCourses = searchObject.faellesudbud;
		} else {
			$scope.sidebarIncludeCommonCourses = true;
		}
		
		$scope.searchParams.offset = 0;

		updateSearchOptions(function(options) {
			if (!$scope.sidebarRegions) {
				// Search caching, region
				if (searchObject.region) {
					$scope.sidebarRegions = searchObject.region;
				} else {
					$scope.sidebarRegions = options.region.items[options.region.items.length - 1].id;
				}
			}

			if (!$scope.sidebarJobAreas) {
				// Search caching, jobarea
				if (searchObject.jobomraade) {
					$scope.sidebarJobAreas = searchObject.jobomraade;
				} else {
					$scope.sidebarJobAreas = options.jobarea.items[options.jobarea.items.length - 1].id;
				}
			}

			if (!$scope.sidebarSubJobAreas) {
				// Search caching, subJobArea
				if (searchObject.temaomraade) {
					$scope.sidebarSubJobAreas = searchObject.temaomraade;
				} else {
					$scope.sidebarSubJobAreas = options.sub_jobarea.items[options.sub_jobarea.items.length - 1].id;
				}
			}

			if (!$scope.searchText) {
				// Search caching, search text
				if (searchObject.kviksoeg) {
					$scope.searchText = searchObject.kviksoeg;
				}
			}

			if (!$scope.sidebarSortOptions) {
				// Search caching, sorting
				if (searchObject.sortering) {
					$scope.sidebarSortOptions = searchObject.sortering;
				} else {
					$scope.sidebarSortOptions = options.sort.items[0].id;
				}
			}

			if (!$scope.sidebarSchools) {
				// Search caching, schools
				if (searchObject.skole) {
					$scope.sidebarSchools = searchObject.skole;
				} else {
					$scope.sidebarSchools = options.school.items[options.school.items.length - 1].id;
				}
			}

			// Search caching, from date
			if (searchObject.fra) {
				$scope.searchParams['start-date'] = searchObject.fra;
				$scope.sidebarStartDate = searchObject.fra;
				$('.sidebarStartDate').datepicker('setDate', moment(searchObject.fra).format('DD/MM/YYYY'));
			} 

			// Search caching, to date
			if (searchObject.til) {
				$scope.searchParams['end-date'] = searchObject.til;
				$scope.sidebarEndDate = searchObject.til;
				$('.sidebarEndDate').datepicker('setDate', moment(searchObject.til).format('DD/MM/YYYY'));
			}
			
			$scope.branches.push({ id: null, name: 'Vis alle' });
			$scope.sidebarBranches = getType();
			updateSearchResult(false);
		});
});

var lastSearchParams = {};

var updateSearchResult = db(function(shouldExtendSearchResult) {
	$scope.updateSearchResult(shouldExtendSearchResult);
}, 100);

$scope.isLoadingCourses = false;

$scope.updateSearchResult = function(shouldExtendSearchResult) {
	if ($scope.isLoadingCourses === false) {
		$scope.isLoadingCourses = true;
		angular.copy($scope.searchParams, lastSearchParams);

		$scope.searchParams.type = getType();
		var access_token = null;

		if (auth && auth.access_token) {
			access_token = auth.access_token;
		}

		CourseService.get($scope.searchParams, access_token, function(courses) {
			$scope.numberOfCoursesShown = courses.pagination.offset + courses.pagination.limit;
			$scope.numberOfCourses = courses.count;

			if (shouldExtendSearchResult === true) {
				Array.prototype.push.apply($scope.searchResult, courses.items);
			} else {
				$scope.searchResult = courses.items;
			}

			$scope.isLoadingCourses = false;
			//$('.ui.dropdown').dropdown();
		});
	} 
};

$scope.$watch("searchText", function(newValue, oldValue) {		
	if (oldValue != newValue) {
		$scope.searchParams['q'] = newValue;
		$location.search('kviksoeg', newValue);
		updateSearchResult();
	}
});

$scope.$watch("sidebarBranches", function(newValue, oldValue) {
	if (oldValue != newValue) {
		$scope.searchParams['type'] = newValue;
		$location.search('branche', newValue);
		updateSearchResult();
		updateSearchOptions();
	}
});

$scope.$watch("sidebarRegions", function(newValue, oldValue) {		
	setChosenRegion(newValue);

	if (oldValue != newValue) {
		$scope.searchParams['region'] = newValue;
		$location.search('region', newValue);
		updateSearchResult();
		getSchoolsForRegion();
	}
});

$scope.$watch("sidebarJobAreas", function(newValue, oldValue) {		
	setChosenJobArea(newValue);

	if (oldValue != newValue) {
		$scope.searchParams['jobarea'] = newValue;
		$location.search('jobomraade', $scope.searchParams.jobarea);
		updateSearchResult();
		getSubAreasForJobAreas();
		if (!isOnLoad) {
			$scope.sidebarSubJobAreas = $scope.searchChoices.sub_jobarea.items[$scope.searchChoices.sub_jobarea.items.length - 1].id;
			$location.search('temaomraade', $scope.searchChoices.sub_jobarea.items[$scope.searchChoices.sub_jobarea.items.length - 1].id);
		} else {
			isOnLoad = false;
		}
	}


});

$scope.$watch("sidebarSortOptions", function(newValue, oldValue) {		
	setChosenSorting(newValue);

	if (oldValue != newValue) {
		$scope.searchParams['sort'] = newValue;
		$location.search('sortering', newValue);
		updateSearchResult();
	}
});

$scope.$watch("sidebarSchools", function(newValue, oldValue) {		
	if (oldValue != newValue) {
		$scope.searchParams['school'] = newValue;
		$location.search('skole', newValue);
		updateSearchResult();
	}
});

$scope.$watch("sidebarSubJobAreas", function(newValue, oldValue) {
	if (oldValue != newValue) {
		$scope.searchParams['sub_jobarea'] = newValue;
		$location.search('temaomraade', newValue);
		updateSearchResult();
	}
});

$scope.$watch("sidebarStartDate", function(newValue, oldValue) {		
	if (oldValue != newValue) {
		$scope.searchParams['start-date'] = newValue;
		$location.search('fra', newValue);
		updateSearchResult();
	}
});

$scope.$watch("sidebarEndDate", function(newValue, oldValue) {		
	if (oldValue != newValue) {
		$scope.searchParams['end-date'] = newValue;
		$location.search('til', newValue);
		updateSearchResult();
	}
});

$scope.$watch("sidebarStartDatePicker", function(newValue, oldValue) {
	if (oldValue != newValue) {
		var date = null;

		if (newValue) {
			date = moment($('.sidebarStartDate').datepicker('getDate')).format('YYYY-MM-DD');
		}

		$scope.searchParams['start-date'] = date;
		$location.search('fra', date);
		updateSearchResult();
	}
});

$scope.$watch("sidebarEndDatePicker", function(newValue, oldValue) {		
	if (oldValue != newValue) {
		var date = null;

		if (newValue) {
			date = moment($('.sidebarEndDate').datepicker('getDate')).format('YYYY-MM-DD');
		}

		$scope.searchParams['end-date'] = date;
		$location.search('til', date);
		updateSearchResult();
	}
});

$scope.$watch("sidebarIsWarrentyCourse", function(newValue, oldValue) {		
	if (oldValue != newValue) {
		$scope.searchParams['guaranteed'] = newValue;
		$location.search('garantikursus', newValue);
		updateSearchResult();
	}
});

$scope.$watch("sidebarIsCompaignCourse", function(newValue, oldValue) {		
	if (oldValue != newValue) {
		$scope.searchParams['campaign'] = newValue;
		$location.search('kampagnekursus', newValue);
		updateSearchResult();
	}
});

$scope.$watch("sidebarIsOpenForAttendance", function(newValue, oldValue) {		
	$scope.searchParams['signup-open'] = newValue;
	$location.search('tilmelding', newValue);
	updateSearchResult();
});

$scope.$watch("sidebarIsStarMarked", function(newValue, oldValue) {		
	if (oldValue != newValue) {
		$scope.searchParams['starred'] = newValue;
		updateSearchResult();
	}
});

$scope.$watch("sidebarIncludeCommonCourses", function(newValue, oldValue) {		
	if (oldValue != newValue) {
		$scope.searchParams['common'] = newValue;
		$location.search('faellesudbud', newValue);
		updateSearchResult();
	}
});	

$scope.toggleAdvancedFiltersMobileSidebarMenu = false;

$scope.toggleAdvancedFiltersMobileSidebarMenu = function() {
	$scope.$parent.hideSidebar();
	$scope.showAdvancedFiltersMobileSideMenu = !$scope.showAdvancedFiltersMobileSideMenu;
};

$scope.$on('event:usermenu-sidebar-shown', function() {
	$scope.showAdvancedFiltersMobileSideMenu = false;
});

function getType() {
	var courseType = null;

	if ($scope.sidebarBranches && courseTypes[$scope.sidebarBranches]) {
		courseType = courseTypes[$scope.sidebarBranches].id;
	} else if ($state.params && $state.params.type) {
		courseType = courseTypeTranslator[$state.params.type];
	} else if (!$scope.sidebarBranches) {
		var searchObject = $location.search();

		if (searchObject.branche) {
			courseType = searchObject.branche;
		}		
	}

	return courseType;
}

function updateSearchOptions(callback) {
	$scope.searchOptionParams.type = getType();
	$scope.searchParams.offset = 0;

	CourseService.getOptions($scope.searchOptionParams, function(options) { 
		var showAllObject = { id: null, name: 'Vis alle' };

		for (var key in options) {
			if (key != 'sort') 
				options[key].items.push(showAllObject);
		}

		$scope.searchChoices = options;

		if (options && callback) {
			callback(options);
		}
	});
}

function setChosenRegion(regionID) {
	if ($scope.searchChoices && $scope.searchChoices.region && $scope.searchChoices.region.items) {
		for (var i = 0; i < $scope.searchChoices.region.items.length; i++) {
			var region = $scope.searchChoices.region.items[i];
			if (region.id === regionID) {
				$scope.chosenRegion = region;
				break;
			}
		}
	}
}

$scope.setRegion = function(region) {
	setChosenRegion(region);
	$scope.searchParams.region = region;
	getSchoolsForRegion();
	$location.search('region', region);
	updateSearchResult();
};

var getSchoolsForRegion = function() {
	$scope.searchOptionParams.region = $scope.searchParams.region;
	updateSearchOptions();
};

var getSubAreasForJobAreas = function() {
	$scope.searchOptionParams.jobarea = $scope.searchParams.jobarea;
	updateSearchOptions();
};

function setChosenJobArea(jobareaID) {
	if ($scope.searchChoices && $scope.searchChoices.jobarea && $scope.searchChoices.jobarea.items) {
		for (var i = 0; i < $scope.searchChoices.jobarea.items.length; i++) {
			var area = $scope.searchChoices.jobarea.items[i];

			if (area.id === jobareaID) {
				$scope.chosenJobArea = area;
				break;
			}
		}
	}
}

$scope.setJobArea = function(jobarea) {
	setChosenJobArea(jobarea);

	$scope.searchParams.jobarea = jobarea;
	$location.search('jobomraade', jobarea);
	updateSearchResult();
	getSubAreasForJobAreas();
	$scope.sidebarSubJobAreas = $scope.searchChoices.sub_jobarea.items[$scope.searchChoices.sub_jobarea.items.length - 1].id;
	$location.search('temaomraade', $scope.searchChoices.sub_jobarea.items[$scope.searchChoices.sub_jobarea.items.length - 1].id);
};

function setChosenSorting(sortID) {
	if ($scope.searchChoices && $scope.searchChoices.sort && $scope.searchChoices.sort.items) {
		for (var i = 0; i < $scope.searchChoices.sort.items.length; i++) {
			var sorting = $scope.searchChoices.sort.items[i];

			if (sorting.id === sortID) {
				$scope.chosenSorting = sorting;
				break;
			}
		}
	}
}

$scope.setSorting = function(sortType) {
	setChosenSorting(sortType);
	$scope.searchParams.sort = sortType;
	$location.search('sortering', sortType);
	updateSearchResult();
};

$scope.hasJobArea = function() {
	return !!($scope.searchParams['jobarea']);
};

$scope.isElectricalCourse = function() {
	var courseTypes = require('../enums/courseTypes');

	return (getType() == courseTypes.electrical.id);
};

$scope.getFromDate = function() {
	var date = $scope.searchParams['start-date'];

	if (date && moment(date).isValid()) {
		return "/fra/" + date;
	}

	return "";
};

$scope.getToDate = function() {
	var date = $scope.searchParams['end-date'];

	if (date && moment(date).isValid()) {
		return "/til/" + date;
	}

	return "";
};

$scope.getIsNotOpenForAttendance = function() {
	if ($scope.searchParams['signup-open'] === false) {
		return "/vis-ikke-aaben-for-tilmelding";
	}

	return "";
};

$scope.getIsWarrantyCourse = function() {
	if ($scope.searchParams['guaranteed'] === true) {
		return "/garantikursus";
	}

	return "";
};

$scope.getSchool = function() {
	if ($scope.searchParams['school'] && $scope.searchParams['school'].length > 0) {
		return "/skole/" + $scope.searchParams['school'];
	}

	return "";
};

$scope.getRegion = function() {
	if ($scope.searchParams['region'] && $scope.searchParams['region'].length > 0) {
		return "/region/" + $scope.searchParams['region'];
	}

	return "";
};

$scope.printAllCourses = function() {
	if ($scope.isLoadingCourses === false) {
		$scope.isLoadingCourses = true;
		$scope.searchParams.type = getType();
		$scope.searchParams.limit = 10000;

		CourseService.get($scope.searchParams, null, function(courses) {
			$scope.numberOfCoursesShown = courses.pagination.offset + courses.pagination.limit;
			$scope.numberOfCourses = courses.count;
			$scope.searchResult = courses.items;
			$scope.isLoadingCourses = false;
			$scope.showAdvancedFiltersMobileSideMenu = false;
			setTimeout(function() { window.print(); }, 5000);
		});
	}
};

$scope.breadCrumb = {};

$scope.breadCrumb.getBranch = function() {
	var branch = null;

	if ($scope.searchParams && $scope.searchParams.type) {
		branch = courseTypes[$scope.searchParams.type].name;
	} else {
		branch = "Alle brancher";
	}

	return branch + " > ";
};

$scope.breadCrumb.getRegion = function() {
	var region = null;

	if ($scope.searchParams && $scope.searchParams.region) {
		region = $scope.searchParams.region;
	} else {
		region = "Alle regioner";
	}

	return region + " > ";
};

$scope.breadCrumb.getJobArea = function() {
	var jobarea = "Alle jobområder";

	if ($scope.chosenJobArea && $scope.chosenJobArea.name && $scope.chosenJobArea.id !== null) {
		jobarea = $scope.chosenJobArea.name;
	}

	return jobarea + " > ";
};

$scope.breadCrumb.getSubJobarea = function() {
	var getSubJobArea = null;

	if ($scope.searchParams && $scope.searchParams.sub_jobarea) {
		var subJobareaID = $scope.sidebarSubJobAreas;
		
		if ($scope.searchChoices && $scope.searchChoices.sub_jobarea.items) {
			for (var i = 0; i < $scope.searchChoices.sub_jobarea.items.length; i++) {
				var element = $scope.searchChoices.sub_jobarea.items[i];

				if (element.id == subJobareaID) {
					subJobareaID = element.name;
				}
			}
		}
	}

	if (!subJobareaID) {
		return "";
	}

	return subJobareaID + " > ";
};

$scope.breadCrumb.getSorting = function() {
	var sort = null;

	if ($scope.searchParams && $scope.searchParams.sort) {
		if ($scope.searchParams.sort === "name") {
			sort = "Alfabetisk";
		} else {
			sort = "Hold start";
		}
	} else {
		sort = "Alfabetisk";
	}

	return sort;
};

$scope.breadCrumb.getSchool = function() {
	var chosenSchool = null;

	if ($scope.sidebarSchools && $scope.searchChoices.school && $scope.searchChoices.school.items) {
		for (var i = 0; i < $scope.searchChoices.school.items.length; i++) {
			var school = $scope.searchChoices.school.items[i];

			if (school.id === $scope.sidebarSchools) {
				chosenSchool = school.name;
			}
		}
	}

	if (!chosenSchool) {
		chosenSchool = "Alle skoler";
	}

	return chosenSchool + " > ";
};

$scope.breadCrumb.getIsWarrentyCourse = function() {
	var result = null;

	if ($scope.searchParams && $scope.searchParams.guaranteed) {
		result = "> Garantikurser";
	}

	return result;
};

	$scope.resetAdvancedFiltersSearch = function(isComputer) {
		if (isComputer) {
			window.location.href = [window.location.protocol, '//', window.location.host, window.location.pathname].join('');
			return;
		} 

		$scope.sidebarRegions = null;
		$scope.sidebarJobAreas = null;
		$scope.sidebarSortOptions = $scope.searchChoices.sort.items[0].id;

		$scope.sidebarSchools = null;
		$scope.sidebarSubJobAreas = null;
		// Computer only
		$scope.sidebarStartDatePicker = null;
		$scope.sidebarEndDatePicker = null;
		// Mobile and tablet only
		$scope.sidebarStartDate = null;
		$scope.sidebarEndDate = null;

		$scope.sidebarIsOpenForAttendance = false;
		$scope.sidebarIsWarrentyCourse = false;
		$scope.sidebarIncludeCommonCourses = true;
		$scope.sidebarIsCompaignCourse = false;
	};


}];