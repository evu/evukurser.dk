/** @module app/controllers/ReminderController */

'use strict';

module.exports = ["$scope", "$state", "AuthService", function($scope, $state, AuthService) {
	$scope.$parent.combineMenuBar = false;
	$scope.user = $scope.$parent.user;

	$scope.$watch("certificateExpired", function(newValue, oldValue) {		
		if (oldValue != newValue) {
			$scope.user.notifications.certificateExpiration = newValue;
		}
	});

	$scope.$watch("attendedCoursesReminders", function(newValue, oldValue) {		
		if (oldValue != newValue) {
			$scope.user.notifications.registeredCourses = newValue;
		}
	});

	$scope.$watch("wishlistCoursesReminders", function(newValue, oldValue) {		
		if (oldValue != newValue) {
			$scope.user.notifications.wishlistCourses = newValue;
		}
	});

	$scope.$on('$viewContentLoaded', function() {
		$scope.certificateExpired = $scope.user.notifications.certificateExpiration;
		$scope.attendedCoursesReminders = $scope.user.notifications.registeredCourses;
		$scope.wishlistCoursesReminders = $scope.user.notifications.wishlistCourses;
	});

	$scope.updateChanges = function() {
		$scope.success = false;
		$scope.errorMessage = null;	
		var access_token = AuthService.getAuth().access_token;

		AuthService.updateProfile($scope.user, $scope.user.id, access_token, function(response) {
			if (response.hasOwnProperty("error")) {
     				$scope.errorMessage = response.error_description;
   			} else {
   				$scope.success = true;
   			}
		});
	};
}];