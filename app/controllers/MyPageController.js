/** @module app/controllers/MyPageController */

'use strict';

module.exports = ["$scope", "$state", "$location", "CourseService", "AuthService", function($scope, $state, $location, CourseService, AuthService) {
  $scope.$parent.combineMenuBar = false;
  $scope.showMyPageSideBarMenu = false;
  $scope.user = null;
  var auth = AuthService.getAuth();
  if (auth && !auth.hasOwnProperty('error') && auth.user) {
    $scope.user = auth.user;
  } else {
    $state.go('login');
  }
  $scope.showRatingInEditMode = true;
  $scope.showCompendiums = true;

  // temp for completed courses
  $scope.isLoadingCourses = false;

  function setCompletedCourses() {
    if (auth && auth.user && $scope.IsCPRVerified()) {
      $scope.isLoadingCourses = true;

      CourseService.getCompletedCourses(auth.user.id, auth.access_token, function(courses) {
        $scope.isLoadingCourses = false;

        if (courses && courses.count && courses.count > 0) {
          $scope.completedCourses = courses.items;
        } else {
          $scope.completedCourses = null;
        }
      });
    }
  }

  $scope.hasCompletedCourses = function() {
    return ($scope.completedCourses !== null && $scope.IsCPRVerified());
  };

  $scope.refreshAuth = function() {
    AuthService.refreshAuth(function(response) {
      auth = response;
      setCompletedCourses();
    });
  };

  $scope.$on('$viewContentLoaded', function() {
    var $ = require('jquery');
    $('.my-courses-menu').find('.item').each(function() {
     var href = $(this).attr('href');

     if (href == $location.path()) {
      $(this).addClass('active');
    } else {
      $(this).removeClass('active');
    } 
  });
      setCompletedCourses();
  });

  $scope.isAdmin = function() {
    return ($scope.user !== null && ($scope.user.roles.indexOf('admin') !== -1));   
  };

  $scope.IsCPRVerified = function() {
    return (auth && auth.user && auth.user.cprVerified === true);
  };
}];