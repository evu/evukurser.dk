/** @module app/controllers/PageController */

'use strict';

module.exports = ["$scope", '$location', '$window', 'AuthService', function($scope, $location, $window, AuthService) {
	var $ = require('jquery');
	$scope.combineMenuBar = false;
	$scope.showSidebar = false;
	$scope.hasReceivedLoginResponse = false;
	$scope.user = null;

	AuthService.refreshAuth(function(response) {
		if (!response || response.hasOwnProperty("error")) {
			$scope.isLoggedIn = false;
		} else {
			$scope.isLoggedIn = true;
			$scope.user = response;
		}

		$scope.hasReceivedLoginResponse = true;
	});

	$scope.toggleSidebar = function() {
		if ($scope.showSidebar === true) {
			$scope.showSidebar = false;
		} else {
			$scope.$broadcast('event:usermenu-sidebar-shown');
			$scope.showSidebar = true;
		}
	};

	$scope.hideSidebar = function() {
		$scope.showSidebar = false;
	};

	$scope.$on('event:loggedIn', function(event, result) { 
		if (result === true) {
			$scope.isLoggedIn = result;
			$scope.hasReceivedLoginResponse = true;
		}
	});

	$scope.$on('$viewContentLoaded', function() {
		$window.ga('send', 'pageview', { page: $location.path() });

		$('.user-menu').find('.item').each(function() {
			var href = $(this).attr('href');

			if (href == $location.path()) {
				var parentContainer = $(this).parent().parent();
				if (parentContainer.hasClass('dropdown')) {
					parentContainer.addClass('active');
				}
				$(this).addClass('active');
			} else {
				$(this).removeClass('active');
			}
		});
	});

}];