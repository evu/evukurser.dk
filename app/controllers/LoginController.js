/** @module app/controllers/LoginController */

'use strict';

module.exports = ["$scope", "$state", "AuthService", function($scope, $state, AuthService) {
	$scope.$parent.combineMenuBar = false;
	$scope.newUser = {};
	$scope.user = {};

	$scope.$on('event:loggedIn', function(event, result) { 
		if (result) {
			$state.go('min-side');
		}
	});

	$scope.$watch("newUser.name", function(newValue, oldValue) {
		if (oldValue != newValue) {
			newValue = newValue.replace(/\s+/g, ' ').trim(); 
			if (newValue.length > 0) {
				$scope.newUser.nameIsValid = true;
			} else {
				$scope.newUser.nameIsValid = false;
			}
		}
	});

	$scope.$watch("newUser.email", function(newValue, oldValue) {
		if (oldValue != newValue) {
			if (/^[A-Z0-9._%+\-]+@[A-Z0-9.\-]+\.[A-Z]{2,20}$/i.test(newValue)) {
				$scope.newUser.emailIsValid = true;
			} else {
				$scope.newUser.emailIsValid = false;
			}
		}
	});

	$scope.$watch("newUser.password", function(newValue, oldValue) {
		if (oldValue != newValue) {
			if (newValue.length >= 6) {
				$scope.newUser.passwordIsValid = true;
			} else {
				$scope.newUser.passwordIsValid = false;
			}
		}
	});

	$scope.$watch("newUser.passwordRepeated", function(newValue, oldValue) {
		if (oldValue != newValue) {
			if (newValue.length > 0 && newValue == $scope.newUser.password && $scope.newUser.passwordIsValid === true) {
				$scope.newUser.passwordRepeatedIsValid = true;
			} else {
				$scope.newUser.passwordRepeatedIsValid = false;
			}
		}
	});

	$scope.createUser = function() {
		if ($scope.newUser.nameIsValid === true && $scope.newUser.emailIsValid === true && $scope.newUser.passwordIsValid === true && $scope.newUser.passwordRepeatedIsValid === true) {
			$scope.createUserErrorMessage = null;
			$scope.createUserSuccessMessage = null;

			AuthService.createUser({ name: $scope.newUser.name, email: $scope.newUser.email, password: $scope.newUser.password }, function(result) {
				if (result.hasOwnProperty('error')) {
					showCreateUserError(result);
				} else {
					showCreateUserSuccess(result);
				}
			});
		}
	};

	function showCreateUserError(data) {
		var loginErrors = require('../enums/loginErrors');
		var error = loginErrors[data.error];
		var message = '';
		if (error.showMessageFromServer === true) {
			message = data.error_description;
		} else {
			message = error.message;
		}	

		$scope.createUserErrorMessage = message;
	}

	function showCreateUserSuccess(/* data */) {
		$scope.createUserSuccessMessage = "Din bruger er nu oprettet. For at logge på, skal du først aktivere din bruger ved at følge linket du har modtaget på din email-adresse.";
	}

	function showLoginError(data) {
		var loginErrors = require('../enums/loginErrors');
		var error = loginErrors[data.error];
		var message = '';
		if (error.showMessageFromServer === true) {
			message = data.error_description;
		} else {
			message = error.message;
		}	

		$scope.loginErrorMessage = message;
	}

	$scope.loginUser = function() {
		AuthService.login($scope.user.email, $scope.user.password, function(result) {
			if (result.hasOwnProperty('error')) {
					showLoginError(result);
			}
		});
	};
}];