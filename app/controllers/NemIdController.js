/** @module app/controllers/NemIDController */

'use strict';

module.exports = ["$scope", "$state", "$sce", "AuthService", "NemIDService", function($scope, $state, $sce, AuthService, NemIDService) {
	var auth = AuthService.getAuth();
	$scope.isCPRVerified = isCPRVerified();
	$scope.isLoading = false;

	var token = null;
	$scope.nemid = {};

	$scope.$on('$viewContentLoaded', function() {
		if (isCPRVerified()) {
			$scope.successMessage = "Du er allerede CPR-valideret.";
		}
	});

	$scope.startVerification = function() {
		$scope.$parent.refreshAuth();
		resetResponseMessages();

		var userid = auth.user.id;
		var access_token = auth.access_token;
		var cpr = $scope.cprNumber;
		$scope.isLoading = true;

		NemIDService.getApplet(userid, cpr, access_token, function(response) {			
			if (response.hasOwnProperty("error")) {
				$scope.isLoading = false;
				$scope.errorMessage = response.error_description;
			} else {
				token = response.postBack.token;
				$scope.nemid = response.nemid;	
			}
		});
	};

	$scope.deleteVerification = function() {
		$scope.isLoading = true;

		if (isCPRVerified) {
			NemIDService.delete(auth.user.id, auth.access_token, function(response) {
				$scope.isLoading = false;

				if (response.hasOwnProperty("error")) {
					resetResponseMessages();
					$scope.errorMessage = "Din verificering kunne ikke slettes. Prøv igen.";
				} else {
					resetResponseMessages();
					$scope.isCPRVerified = false;
				}
			});
		}
	};

	$scope.trustSrc = function(src) {
		return $sce.trustAsResourceUrl(src);
	};

	function isCPRVerified() {
		return (auth && auth.user && auth.user.cprVerified === true);
	}

	$scope.getCPR = function() {
		if ($scope.isCPRVerified()) {
			return auth.user.cpr;
		}
	};

	function resetResponseMessages() {
		$scope.errorMessage = "";
		$scope.successMessage = "";
	}

	function validateCPR(response) {
		if (token) {
			NemIDService.put(auth.user.id, token, auth.access_token, response, function(response) {
				if (response.hasOwnProperty("error")) {
					$scope.errorMessage = response.error_description;
				} else if (response.status == "ok") {
					$scope.successMessage = "Du er nu CPR-valideret og kan se dine tilmeldte og gennemførte kurser.";
					$scope.isCPRVerified = true;
					$scope.$parent.refreshAuth();
				}
			});
		}
	}

	function onNemIDMessage(e) {
		var event = e || event;

		var win = document.getElementById('nemid_iframe').contentWindow;
		var message = JSON.parse(event.data);
		$scope.isLoading = false;

		if (message.command === 'SendParameters') {
			win.postMessage(JSON.stringify({
				command: 'parameters',
				content: $scope.nemid.parameters
			}), $scope.nemid.applet.origin);
		}

		if (message.command === 'changeResponseAndSubmit') {
			var response = { response: message.content };
			validateCPR(response);
		}
	}

	if (window.addEventListener) {
		window.addEventListener("message", onNemIDMessage);
	} else if (window.attachEvent) {
		window.attachEvent("onmessage", onNemIDMessage);
	}


}];

