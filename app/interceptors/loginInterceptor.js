/** @module app/interceptors/loginInterceptor */
'use strict';

module.exports = function($httpProvider) {
	var interceptor = ['$rootScope', '$q', function(scope, $q) {

		function success( response ) {
			return response;
		}

		function error( response ) {
			if ( response.status == 401) {
				var deferred = $q.defer();
				scope.$broadcast('event:unauthorized');
				return deferred.promise;
			}
			return $q.reject( response );
		}

		return function( promise ) {
			return promise.then( success, error );
		};

	}];
	$httpProvider.responseInterceptors.push( interceptor );
};